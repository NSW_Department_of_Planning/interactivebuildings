<?php

/**
 * @file
 * NSW Planning constants.
 *
 */

define('NSW_SEARCH_BASE_URL', '/' . drupal_get_path('module', 'nsw_planning_search'));