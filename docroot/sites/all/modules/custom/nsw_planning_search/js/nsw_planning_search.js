(function($){
  Drupal.behaviors.nswpsearch = {
    attach: function(context, settings) {

      // Search add HTML5 attributes for search field
      $(".form-item-keyword input").attr("placeholder", "What do you want to build?");
      $(".form-item-keyword input").attr("autocapitalize", "off");
      $(".form-item-keyword input").attr("autocorrect", "off");
      $(".form-item-keyword input").attr("autocomplete", "off");
      $(".form-item-keyword input").attr("spellcheck", "false");

      // List of Development Types node view - dropdowns
      $(".group-wrapper h3").click(function () {
          $(this).toggleClass("active");
          $(this).next($(".group-wrapper .group-results")).toggle();
      });

      $('.view-model-ui-residential .view-content').hide();
      $('.view-model-ui-commercial .view-content').hide();
      $('.view-model-ui-industrial .view-content').hide();

      //Prevent reset button from redirecting
      $(document).delegate('.views-reset-button .form-submit', 'click', function (event) {
        event.preventDefault();
        $('form').each(function () {
            $('form select option').removeAttr('selected');
            $('form input[type=text]').attr('value', '');
            this.reset();
        });
        $('.views-submit-button .form-submit').click();
        return false;
      });

      var search = $("input#edit-keyword");
      var modelNav = $('.view-alteration-object .view-content');

      search.addClass('clearable');

      function tog(v) {
        return v ? 'addClass' : 'removeClass';
      }

      $(document).on('input', '.clearable', function () {
          $(this)[tog(this.value)]('x');
      }).on('mousemove', '.x', function (e) {
          $(this)[tog(this.offsetWidth - 25 < e.clientX - this.getBoundingClientRect().left)]('onX');
      }).on('click', '.onX', function () {
          $(this).removeClass('x onX').val('').change();
          // Trigger reset
          $('#edit-reset').click();
      });

      // ajaxComplete()
      jQuery(document).ajaxComplete(function (event, xhr, settings) {

          if (settings.hasOwnProperty('extraData') && settings.extraData.hasOwnProperty('view_name') && settings.extraData.view_name === 'model_ui_residential') {
              modelNav.hide();

            $(document).on("click", ".clearable", function () {
                modelNav.show();
                $('.view-model-ui-residential .view-content').hide();
                $('.view-model-ui-commercial .view-content').hide();
                $('.view-model-ui-industrial .view-content').hide();
            });

            $('.search-results-menu .moveLink').click(function (e) {
                e.preventDefault();
                $('.view-display-id-residential_block .moveLink[data-altobj="' + $(this).data("altobj") + '"]').click();
                $('.view-display-id-commercial_block .moveLink[data-altobj="' + $(this).data("altobj") + '"]').click();
                $('.view-display-id-industrial_block .moveLink[data-altobj="' + $(this).data("altobj") + '"]').click();
            });

            //If content panel is open, then close it
            if ($("#object_content_section").is(':visible')) {
                $("#object_content_section").animate({height: 'hide'}, 'fast');
            }

            //Open up the search-results-menu
            $(".object-menu-wrapper .menu-link").animate({left: "0"}, 'fast');
            $(".search-results-menu .view-content").animate({width: 'show'}, 'fast');
          }

      }); // End ajaxComplete()

    }
  };
}(jQuery));