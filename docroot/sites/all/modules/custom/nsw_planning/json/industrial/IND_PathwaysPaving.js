{

    "metadata" :
    {
        "formatVersion" : 3.1,
        "generatedBy"   : "Blender 2.65 Exporter",
        "vertices"      : 4,
        "faces"         : 1,
        "normals"       : 1,
        "colors"        : 0,
        "uvs"           : [4],
        "materials"     : 1,
        "morphTargets"  : 0,
        "bones"         : 0
    },

    "scale" : 1.000000,

    "materials" : [ {
        "DbgColor" : 15658734,
        "DbgIndex" : 0,
        "DbgName" : "body",
        "blending" : "NormalBlending",
        "colorAmbient" : [0.7520000092983246, 0.7920000194311143, 0.800000011920929],
        "colorDiffuse" : [0.7520000092983246, 0.7920000194311143, 0.800000011920929],
        "colorSpecular" : [0.5, 0.5, 0.5],
        "depthTest" : true,
        "depthWrite" : true,
        "shading" : "Lambert",
        "specularCoef" : 50,
        "transparency" : 1.0,
        "transparent" : true,
        "vertexColors" : false
    }],

    "vertices" : [-141.765,0.499981,158.882,-86.7812,0.499981,158.882,-86.7812,0.499984,131.233,-141.765,0.499984,131.233],

    "morphTargets" : [],

    "normals" : [0,1,0],

    "colors" : [],

    "uvs" : [[0,0,1,0,1,1,0,1]],

    "faces" : [43,0,1,2,3,0,0,1,2,3,0,0,0,0],

    "bones" : [],

    "skinIndices" : [],

    "skinWeights" : [],

    "animations" : []


}