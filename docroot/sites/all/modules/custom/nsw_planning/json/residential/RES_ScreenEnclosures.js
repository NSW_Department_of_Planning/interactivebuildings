{

	"metadata" :
	{
		"formatVersion" : 3.1,
		"generatedBy"   : "Blender 2.65 Exporter",
		"vertices"      : 12,
		"faces"         : 3,
		"normals"       : 3,
		"colors"        : 0,
		"uvs"           : [4],
		"materials"     : 1,
		"morphTargets"  : 0,
		"bones"         : 0
	},

	"scale" : 1.000000,

	"materials" : [	{
		"DbgColor" : 15658734,
		"DbgIndex" : 0,
		"DbgName" : "MatMeshFabric_Active",
		"blending" : "NormalBlending",
		"colorAmbient" : [0.0, 0.0, 0.0],
		"colorDiffuse" : [0.0, 0.607843, 0.894117],
		"colorSpecular" : [0.25, 0.25, 0.25],
		"depthTest" : true,
		"depthWrite" : true,
		"mapDiffuse" : "../../images/MatMeshFabric_Active.png",
		"mapDiffuseWrap" : ["repeat", "repeat"],
		"shading" : "Lambert",
		"specularCoef" : 49,
		"transparency" : 0.699999988079071,
		"transparent" : true,
		"vertexColors" : false
	}],

	"vertices" : [-10.7996,23.3006,26.7504,-10.7996,13.1294,26.7504,5.41946,13.1294,26.7504,5.41946,23.3006,26.7504,6,23.3006,26.1775,6,13.1294,26.1775,6,13.1294,13.2276,6,23.3006,13.2276,6,23.3006,-13.2483,6,13.1294,-13.2483,6,13.1294,-26.1787,6,23.3006,-26.1787],

	"morphTargets" : [],

	"normals" : [0,0,1,0.999969,0,0,1,0,0],

	"colors" : [],

	"uvs" : [[0,0,1,0,1,1,0,1]],

	"faces" : [43,0,1,2,3,0,0,1,2,3,0,0,0,0,43,4,5,6,7,0,0,1,2,3,1,2,2,2,43,8,9,10,11,0,0,1,2,3,2,2,2,2],

	"bones" : [],

	"skinIndices" : [],

	"skinWeights" : [],

	"animations" : []


}
