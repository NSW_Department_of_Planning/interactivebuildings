<?php

/**
 * @file
 * NSW Planning - Residential.
 *
 */
function nsw_planning_terms_conditions_page() {

  $tcs = node_load(variable_get('tcs_node', NULL));

  // Check for node and correct content type.
  if (is_object($tcs) && $tcs->type == 'terms_and_conditions') {

    $form = array();

    // Build our terms and conditions page.
    $form['terms-conditions']['tcs']['title'] = array(
      '#markup' => '<h2>' . $tcs->title . '</h2>',
    );

    $form['terms-conditions']['tcs']['body'] = array(
      '#prefix' => '<div class="tcs-body">',
      '#markup' => check_markup($tcs->body[LANGUAGE_NONE][0]['value'], 'filtered_html'),
      '#suffix' => '</div>',
    );

    $form['terms-conditions']['tcs']['fine'] = array(
      '#prefix' => '<div class="tcs-fine">',
      '#markup' => check_plain($tcs->field_t_and_c_fine_print[LANGUAGE_NONE][0]['value']),
      '#suffix' => '</div>',
    );

    $form['terms-conditions']['tcs']['submit'] = array(
      '#prefix' => '<div class="tcs-submit">',
      '#type' => 'button',
      '#value' => t('Agree'),
      '#attributes' => array(
        'onclick' => "jq1102.cookie('agreed','1', { expires: 30, path: '/' });
				parent.location.reload();ga('send', 'event', 'terms_and_conditions','agree_terms_and_conditions', 'terms_buttons');"
      ),
      '#suffix' => '</div>',
    );

    $form['terms-conditions']['tcs']['disagree'] = array(
      '#prefix' => '<div class="tcs-disagree">',
      '#markup' => '<a href="/">Disagree</a>',
      '#suffix' => '</div>',
    );

    return $form;
  }
  else {
    drupal_set_message(t('Terms and Conditions not provided.'), 'error');
    // We want to keep a record of any terms and condtions not set.
    watchdog('nsw_planning', 'Terms and conditions node %tcs not correctly set', array(
      '%tcs' => variable_get('tcs_node', NULL)), WATCHDOG_NOTICE, $link = NULL);
  }
}
