<?php
/**
 * @file
 * NSW Planning - Helper functions.
 */

function get_term_name($tid) {
	$term = taxonomy_term_load($tid);
	$term_name = $term->name;
	return $term_name;
}

/**
 * Returns a loaded taxonomy term for use
 *
 * @param $term_name Term name
 * @param $vocabulary_name Name of the vocabulary to search the term in
 *
 * @return Found term, or FALSE if term is not found
 */
function _get_term_from_name($term_name, $vocabulary_name) {
  if ($vocabulary = taxonomy_vocabulary_machine_name_load($vocabulary_name)) {
    $tree = taxonomy_get_tree($vocabulary->vid);
    foreach ($tree as $term) {
      if ($term->name == $term_name) {
        return taxonomy_term_load($term->tid);
      }
    }
  }

  return FALSE;
}
