<?php
/**
 * @file
 * NSW Planning - Residential.
 *
 */

/**
 * Page callback from hook_menu().
 */
function nsw_planning_residential_interactive_page() {

  // Add page specific JS.
  $js_path = drupal_get_path('module', 'nsw_planning') . '/js/';
  drupal_add_js($js_path . 'residential.js');
  return theme_render_template(drupal_get_path('module', 'nsw_planning') . '/includes/residential/nsw_planning_interactive_model.tpl.php', array());

}