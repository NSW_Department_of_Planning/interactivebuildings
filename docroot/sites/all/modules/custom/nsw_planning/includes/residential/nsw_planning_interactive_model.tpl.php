<?php if (module_exists('nsw_planning_search_property')) : ?>
  <?php $block = module_invoke('nsw_planning_search_property', 'block_view', 'property_search'); ?>
  <?php print render($block['content']); ?>
<?php endif; ?>
<!-- Interactive model template -->
<div class="middle_content">

  <div id="canvas_container">

    <div class="object-menu-wrapper">

      <div class="object-menu">
        <?php print views_embed_view('model_ui_residential', 'residential_search_block'); ?>
        <?php print views_embed_view('alteration_object', 'residential_block'); ?>
      </div>

      <div class="menu-link"><span class="bars-icon"></span>MENU</div>

    </div>

    <div class="reset-link">
      <a href="#" class="moveLink" data-attr="ResetView"><span
          class="resize-icon"></span><span
          class="reset-text"><?php echo t("RESET VIEW"); ?></span></a>
    </div>

  </div>
  <!-- <div id="object-wrapper" class="denis"></div> -->

  <div id="popup_label_scene">
    <div id="popup_box">
      <?php print views_embed_view('tooltips', 'residential_tooltips'); ?>
    </div>
  </div>

</div> <!-- End .middle_content -->
<!-- End Interactive model template -->
<div id="object-wrapper" class=""></div>