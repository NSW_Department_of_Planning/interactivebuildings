<?php
module_load_include('inc', 'nsw_planning', 'includes/_helpers');

function nsw_planning_reports_details_pdf_page() {

  $temp = drupal_get_form('nsw_planning_reports_details_pdf_form');
  $html = drupal_render($temp);
  $fileName = 'NSW Interactive Buildings Report ' . date('omd') . '-' . time();

  //return $html;
  return nsw_pdf_using_mpdf_api($html, $fileName);
}

function nsw_planning_reports_details_pdf_form($form, &$form_state) {

  $values = $_SESSION['nsw_planning']['selections'];
  $property_details = NULL;
  if (!empty($_SESSION['nsw_planning_search']['property']) && module_exists('nsw_planning_search_property')) {
    $property_details = $_SESSION['nsw_planning_search']['property'];
  }

  $items = new RecursiveIteratorIterator(new RecursiveArrayIterator($values));

  foreach ($items as $key => $value) {

    if (strstr($key, 'node_') && $value == TRUE) {
      $nids = trim($key, 'node_');
      $node[] = node_load($nids);
    }
  }

  $form = array(
    '#attributes' => array(
      'content' => 'text/html; charset=utf-8',
      'class' => array(
        'cover',
        'reports-pdf',
      ),
    ),
  );

  $form['cover'] = array(
    '#type' => 'fieldset',
    '#title' => 'Reports',
    '#attributes' => array(
      'class' => array(
        'cover',
        'reports-pdf',
      ),
    ),
  );

  $tcs = node_load(variable_get('tcs_node', NULL));

  if ($property_details) {
    $address_label = nsw_planning_search_property_form_property_label(NULL, NULL, $property_details);
    $zone_label = implode(',', array_values($property_details['SIZones'])) . " {$property_details['Model']}";
    $suburb_postcode = array(
      $property_details['AddressSummary']['SuburbName'],
      $property_details['AddressSummary']['PostCode']
    );
    $lgas = array();
    $councils = nsw_planning_search_property_get_council_data($suburb_postcode);
    if (sizeof($councils) > 0) {
      foreach ($councils as $council_name => $council_obj) {
        $lgas[] = "<a href='{$council_obj->website}' target='_blank'>{$council_obj->council_name}</a>";
      }
    }
    $lga_label = implode(', ', $lgas);
  }

  $form['cover']['page'] = array(
    '#prefix' => '<div class="print-cover-logo"><img src="/sites/all/themes/nsw/images/logo-print.png" /></div>',
    '#markup' => '<div class="print-cover-content">NSW Interactive Buildings Reports</div>
                  <div class="print-cover-footer">' . $address_label . '</div>
                  <div class="print-cover-footer">' . date("j F Y") . '</div>
  		            <div  class="print-cover-flower"><img src="/sites/default/files/flower-print.jpg" /></div><div>&nbsp;</div>',
  );

  if ($property_details) {

    $form['property_info'] = array(
      '#type' => 'fieldset',
      '#title' => '',
      '#attributes' => array(
        'class' => array(
          'reports-pdf',
          'summary-property',
        ),
      ),
    );

    $form['property_info']['title'] = array(
      '#prefix' => '<span class="icon"><img src="/sites/default/files/icon-' . strtolower($property_details['Model']) . '-print.png" /></span><div class="dev-type ' . strtolower($property_details['Model']) . '">',
      '#markup' => 'Property Information',
      '#suffix' => '</div>',
    );

    $form['property_info']['address'] = array(
      '#markup' => $address_label,
    );

    $form['property_info']['zone'] = array(
      '#markup' => ' (' . $zone_label . ')',
    );

    $form['property_info']['lga'] = array(
      '#type' => 'item',
      '#title' => 'Local Government Area: ',
      '#markup' => $lga_label,
    );
  }

  global $base_url;

  // Load the current property development model and processed data
  if (!empty($_SESSION['nsw_planning_search']['property']['Model'])) {
    $module_type = $_SESSION['nsw_planning_search']['property']['Model'];
    // Get processed data from session
    $processed_property = $_SESSION['nsw_planning_search']['property']['ProcessedData'][$module_type];
  }

  // Array declaration for bundle of information.
  $faq_array = array();
  $general_req = array();

  foreach ($node as $value) {

    // Loop through processed data and find title matching for current alteration object
    $node_title = $value->title;
    $is_allowed = -1;
    if (!empty($processed_property[$node_title])) {
      $is_allowed = $processed_property[$node_title]['IsAllowed'];
    }

    $form[$value->nid] = array(
      '#type' => 'fieldset',
      '#title' => '',
      '#attributes' => array(
        'class' => array(
          'reports-pdf',
        ),
      ),
    );

    $form[$value->nid]['type'] = array(
      //'#prefix' => '<span class="icon"><img src="/sites/default/files/icon-' . get_term_name($value->field_development_types[LANGUAGE_NONE][0]['tid']) . '-print.png" /></span><div class="dev-type ' . get_term_name($value->field_development_types[LANGUAGE_NONE][0]['tid']) . '">',
      '#prefix' => '<div><span class="icon"><img src="/sites/all/themes/nsw/images/sprint2/icon-' . strtolower(get_term_name($value->field_development_types[LANGUAGE_NONE][0]['tid'])) . '-alteration_object.png" /></span><div class="dev-type ' . get_term_name($value->field_development_types[LANGUAGE_NONE][0]['tid']) . '">',
      '#markup' => get_term_name($value->field_development_types[LANGUAGE_NONE][0]['tid']),
      '#suffix' => '</div></div>',
    );

    $form[$value->nid]['category'] = array(
      '#prefix' => '<div class="category">',
      '#markup' => get_term_name($value->field_alteration_object_category[LANGUAGE_NONE][0]['tid']),
      '#suffix' => '</div>',
    );

    $form[$value->nid]['title'] = array(
      '#prefix' => '<div class="dev-type-item">',
      '#markup' => '<h3>' . $value->title . '</h3>',
      '#suffix' => '</div>',
    );

    if (isset($value->field_alteration_image[LANGUAGE_NONE][0]['uri'])) {

      $uri = $value->field_alteration_image[LANGUAGE_NONE][0]['uri'];

      $file_path = str_replace('public:/', variable_get('file_public_path', 'sites/default/files'), $uri);
      $fullpath = $base_url . '/' . $file_path;

      $form[$value->nid]['image'] = array(
        '#prefix' => '<div class="alt-image">',
        '#markup' => '<img src="' . $fullpath . '" />',
        '#suffix' => '</div>',
      );
    }

    $value->is_report = TRUE;

    $temp = node_view($value);
    $form[$value->nid]['content'] = array(
      '#markup' => '<p>' . drupal_render($temp) . '</p>',
    );
    $form[$value->nid]['pagebreak'] = array(
      '#markup' => '<pagebreak />',
    );
    // Section: Group FAQs.
    // FAQs should be grouped together.
    switch ($is_allowed) {
      case '0':
      case '2':
        if (isset($value->field_alteration_faq[LANGUAGE_NONE][0]['nid'])) {
          foreach ($value->field_alteration_faq[LANGUAGE_NONE] as $key => $faq_field_alteration) {
            $faq_array[] = $faq_field_alteration['nid'];
          }
        }
        break;
      case '1':
        if (isset($value->field_alteration_object_rest_faq[LANGUAGE_NONE][0]['nid'])) {
          foreach ($value->field_alteration_object_rest_faq[LANGUAGE_NONE] as $key => $faq_field_rest_faq) {
            $faq_array[] = $faq_field_rest_faq['nid'];
          }
        }
        break;
      default:
        if (isset($value->field_alteration_faq[LANGUAGE_NONE][0]['nid'])) {
          foreach ($value->field_alteration_faq[LANGUAGE_NONE] as $key => $faq_field_alteration) {
            $faq_array[] = $faq_field_alteration['nid'];
          }
        }
        if (isset($value->field_alteration_object_rest_faq[LANGUAGE_NONE][0]['nid'])) {
          foreach ($value->field_alteration_object_rest_faq[LANGUAGE_NONE] as $key => $faq_field_rest_faq) {
            $faq_array[] = $faq_field_rest_faq['nid'];
          }
        }
        break;
    }

    // Section : General requirements.
    // BANSW0006-152: General requirement appears only once at the end of the PDF.
    if (isset($value->field_additional_requirements[LANGUAGE_NONE][0]) && $is_allowed != 1) {
      foreach ($value->field_additional_requirements[LANGUAGE_NONE] as $key => $entity) {
        $node = $entity['node'];
        $general_req[] = $node->nid;
      }
    }
  } // end foreach ($node as $value).

  // Remove duplications in faq array.
  $faq_array = array_unique($faq_array, SORT_STRING);

  // Printing the FAQ Block.
  if (count($faq_array) != 0) {

    // Adding the fieldset around the FAQs.
    $form['requirements_faq'] = array(
      '#type' => 'fieldset',
      '#attributes' => array(
        'class' => array(
          'reports-pdf',
          'tcs',
        ),
      ),
    );

    $form['requirements_faq']['title'] = array(
      '#prefix' => '<div class="faq-title">',
      '#markup' => 'Frequently Asked Questions',
      '#suffix' => '</div>',
    );

    // For each FAQ collected, output it with markup.
    foreach ($faq_array as $faq_nid) {
      $faq = node_load($faq_nid);

      $faq_title = $faq->title;
      $faq_body = $faq->body[LANGUAGE_NONE][0]['safe_value'];

      $form['requirements_faq'][$faq_nid] = array(
        '#prefix' => '<div class="requirements-faq">',
        '#markup' => '<h4>' . $faq_title . '</h4><p>' . $faq_body . '</p>',
        '#suffix' => '</div>',
      );
    }
  }

  // Printing the General Requirements Block.
  if (count($general_req) != 0) {
    // Adding a page break before the general requirement section.
    /* $form[$value->nid]['page_break'] = array(
      '#markup' => '<pagebreak />',
      ); */

    // Adding the fieldset around the general requirement section.
    $form['additional_requirement'] = array(
      '#type' => 'fieldset',
      '#attributes' => array(
        'class' => array(
          'reports-pdf',
          'tcs',
        ),
      ),
    );

    // Foreach general requirement collected, we load and display them.
    foreach ($general_req as $req) {
      $node = node_load($req);
      $form['additional_requirement'][$req] = array(
        '#prefix' => '<div class="tcs-fineprint">',
        '#markup' => '<p>' . $node->body[LANGUAGE_NONE][0]['safe_value'] . '</p>',
        '#suffix' => '</div>',
      );
    }
  }

  // Adding a page break before the disclamer section.
  $form['additional_requirement']['page_break'] = array(
    '#markup' => '<pagebreak />',
  );

  $form['fineprint'] = array(
    '#type' => 'fieldset',
    '#attributes' => array(
      'class' => array(
        'reports-pdf',
        'tcs',
      ),
    ),
  );

  $form['fineprint']['title'] = array(
    '#prefix' => '<div class="disclaimer-title">',
    '#markup' => 'Disclaimer',
    '#suffix' => '</div>',
  );

  $form['fineprint']['body'] = array(
    '#prefix' => '<div class="tcs-fineprint">',
    '#markup' => check_markup($tcs->field_t_and_c_fine_print[LANGUAGE_NONE][0]['value'], 'full_html'),
    '#suffix' => '</div>',
  );

  return $form;
}

/**
 * Generate the address to be displayed in the footer.
 *
 * @return String Address formated into HTML.
 */
function tokenAddressForPdfFooter() {
  // Creating adress for the footer of the PDF.
  // 1. Processing Unit.
  if ($_SESSION['nsw_planning_search']['property']['AddressSummary']['StreetNumberUnitNumber'] == '') {
    $footer_unit = '';
  }
  else {
    $footer_unit = 'Unit ' . $_SESSION['nsw_planning_search']['property']['AddressSummary']['StreetNumberUnitNumber'] . ', ';
  }
  // 2. Processing street number.
  if ($_SESSION['nsw_planning_search']['property']['AddressSummary']['StreetNumberTo'] == '' &&
          $_SESSION['nsw_planning_search']['property']['AddressSummary']['StreetNumberFrom'] == '') {
    // Case 1: street number FROM and TO are empty.
    $footer_street_number = '';
  }
  elseif ($_SESSION['nsw_planning_search']['property']['AddressSummary']['StreetNumberTo'] == '' &&
          $_SESSION['nsw_planning_search']['property']['AddressSummary']['StreetNumberFrom'] != '') {
    // Case 2: only street number FROM is full.
    $footer_street_number = $_SESSION['nsw_planning_search']['property']['AddressSummary']['StreetNumberFrom'] . ', ';
  }
  else {
    // Case 3: street number FROM and TO are full.
    $footer_street_number = $_SESSION['nsw_planning_search']['property']['AddressSummary']['StreetNumberFrom'] .
            '-' . $_SESSION['nsw_planning_search']['property']['AddressSummary']['StreetNumberTo'] . ', ';
  }
  // 3. Processing street name.
  if ($_SESSION['nsw_planning_search']['property']['AddressSummary']['StreetName'] == '') {
    $footer_street_name = '';
  }
  else {
    $footer_street_name = $_SESSION['nsw_planning_search']['property']['AddressSummary']['StreetName'] . ' ';
  }
  // 4. Processing street type.
  if ($_SESSION['nsw_planning_search']['property']['AddressSummary']['StreetNameType'] == '') {
    $footer_street_type = '';
  }
  else {
    $footer_street_type = $_SESSION['nsw_planning_search']['property']['AddressSummary']['StreetNameType'] . ', ';
  }
  // 5. Processing suburb.
  if ($_SESSION['nsw_planning_search']['property']['AddressSummary']['SuburbName'] == '') {
    $footer_suburb = '';
  }
  else {
    $footer_suburb = $_SESSION['nsw_planning_search']['property']['AddressSummary']['SuburbName'] . ', ';
  }
  // 6. Processing postcode.
  if ($_SESSION['nsw_planning_search']['property']['AddressSummary']['PostCode'] == '') {
    $footer_postcode = '';
  }
  else {
    $footer_postcode = $_SESSION['nsw_planning_search']['property']['AddressSummary']['PostCode'];
  }

  // Displays all this information in 1 line.
  return $footer_unit . $footer_street_number . $footer_street_name . $footer_street_type
          . $footer_suburb . $footer_postcode;
}
