<?php
/**
 * @file
 * NSW Planning - Reports.
 *
 */
module_load_include('inc', 'nsw_planning', 'includes/_helpers');

/**
 * Page callback from hook_menu().
 */
function nsw_planning_reports_page() {

  foreach ($_COOKIE as $key => $value) {
    if (strstr($key, "node_")) {
      $data[$key] = $value;
    }
  }

  $output['form'] = drupal_get_form('nsw_planning_reports_form', $data);

  return $output;
}

function nsw_planning_reports_form($form, &$form_state, $data) {

  $form = array();

  $form['intro'] = array(
    '#prefix' => '<div class="intro">',
    '#markup' => 'Please select the development types reports that you would like to view/email.',
    '#suffix' => '</div>',
  );

  // Load nodes
  foreach ($data as $value) {
    $nid = trim($value, '"');
    $alt_obj = node_load($nid);
    if (!empty($_SESSION['nsw_planning_search']['property']) && module_exists('nsw_planning_search_property')) {
      $property_details = $_SESSION['nsw_planning_search']['property'];
      if (get_term_name($alt_obj->field_development_types[LANGUAGE_NONE][0]['tid']) === $_SESSION['nsw_planning_search']['property']['Model']) {
        $nodes[] = $alt_obj;
      }
    }
    else {
      $nodes[] = $alt_obj;
    }
  }

  if (isset($nodes)) {

    $form['select-all'] = array(
      '#type' => 'checkbox',
      '#title' => 'Select all',
      '#default_value' => TRUE,
      '#required' => FALSE,
    );

    // Development types
    foreach ($nodes as $key => $alt_obj) {

      if (is_object($alt_obj)) {

        // Developments
        $form['dev_type_' . $alt_obj->field_development_types[LANGUAGE_NONE][0]['tid']] = array(
          '#type' => 'item',
          '#title' => '<div class="dev-type-title">' . get_term_name($alt_obj->field_development_types[LANGUAGE_NONE][0]['tid']) . '</div>',
        );
      }
    }

    // Categories
    foreach ($nodes as $key => $alt_obj) {

      if (is_object($alt_obj)) {

        // Categories
        $form['dev_type_' . $alt_obj->field_development_types[LANGUAGE_NONE][0]['tid']]['cat_' . $alt_obj->field_alteration_object_category[LANGUAGE_NONE][0]['tid']] = array(
          '#prefix' => '<div class="dev-type-category">',
          '#type' => 'item',
          '#title' => get_term_name($alt_obj->field_alteration_object_category[LANGUAGE_NONE][0]['tid']),
          '#suffix' => '</div>',
        );
      }
    }

    // Items
    foreach ($nodes as $key => $alt_obj) {

      if (is_object($alt_obj)) {

        // Items
        $form['dev_type_' . $alt_obj->field_development_types[LANGUAGE_NONE][0]['tid']]['cat_' . $alt_obj->field_alteration_object_category[LANGUAGE_NONE][0]['tid']]['node_' . $alt_obj->nid] = array(
          '#type' => 'checkbox',
          '#title' => $alt_obj->title,
          '#default_value' => TRUE,
          '#required' => FALSE,
        );
      }
    }

    $form['back'] = array(
      '#type' => 'button',
      '#value' => t('<< Back'),
      '#attributes' => array(
        'onclick' => 'window.history.back();return false;',
        'style' => 'margin-right: 10px;'
      ),
    );

    $form['button'] = array(
      '#type' => 'button',
      '#value' => t('Clear all'),
      '#attributes' => array(
        'class' => array(
          'btn-clear'),
      ),
    );

    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Generate Reports'),
      '#attributes' => array(
        'formtarget' => rand(),
        'onmousedown' => 'randomFormTarget()',
      ),
    );

    // function who allows the client to open every report in another browser tab.
    $form['js'] = array(
      '#type' => 'markup',
      '#markup' => '<script>'
      . 'function randomFormTarget() {'
      . ' var r = Math.floor((Math.random() * 100) + 1);'
      . ' if (document.getElementById(\'edit-submit\')) {'
      . '   document.getElementById(\'edit-submit\').formTarget = \'pdf\' + r ;'
      . ' }'
      . '}'
      . '</script>'
    );


    /* BEGIN : Email Reports section */
    $form['button_email'] = array(
      '#type' => 'markup',
      '#markup' => '<input class="btn btn-email-reports" style="margin-left:10px" id="edit-button-email2" name="op" value="Email Reports" type="button" '
      . 'onclick="var el=document.getElementById(\'edit-your-email-address-wrapper\');el.style.display = (el.style.display == \'block\' ? \'none\' : \'block\' );">',
    );
    $form['your_email_address_wrapper'] = array(
      '#type' => 'container',
      '#attributes' => array(
        'class' => array(
          'display-none',
          'grey-block',
        ),
        'id' => 'edit-your-email-address-wrapper',
      ),
    );

    $form['your_email_address_wrapper']['email'] = array(
      '#type' => 'textfield',
      '#title' => t('Your e-mail address'),
      '#size' => 60,
      '#default_value' => '',
      '#maxlength' => 60,
    );

    $form['your_email_address_wrapper']['note'] = array(
      '#markup' => t('<strong>Please note:</strong> Your email address will only be used to send the requested reports.'),
      '#prefix' => '<div class="please_note">',
      '#suffix' => '</div>',
    );

    $form['your_email_address_wrapper']['submit_email'] = array(
      '#type' => 'submit',
      '#value' => t('Submit'),
    );

    // display the email report wrapper after a invalid email adress
    if (count($form_state['input']) > 0) {
      $form['your_email_address_wrapper']['javascript'] = array(
      '#type' => 'markup',
      '#markup' => '<script>'
        . 'document.getElementById(\'edit-your-email-address-wrapper\').style.display=\'block\' ;'
        . '</script>',
      );
    }


    $form['#validate'] = array(
      'nsw_planning_reports_custom_form_validation');
    /* END : Email Reports section */
  }
  else {
    $form['empty-result'] = array(
      '#prefix' => '<div class="empty-result">',
      '#markup' => 'There are no recently viewed development types. Explore the interactive building modules in order to generate reports.',
      '#suffix' => '</div>',
    );

    $form['back'] = array(
      '#type' => 'button',
      '#value' => t('<< Back'),
      '#attributes' => array(
        'onclick' => 'window.history.back();return false;',
        'style' => 'margin-right: 10px;'
      ),
    );
  }

  return $form;
}

function nsw_planning_reports_form_submit($form, &$form_state) {
  $_SESSION['nsw_planning'] = array();
  $_SESSION['nsw_planning']['selections'] = $form_state['values'];
  // case : after click on "Generate Reports" button
  if ($form_state['clicked_button']['#id'] == "edit-submit") {

    $form_state['redirect'] = 'reports/details/pdf';
  }
  // case : after click on "Email Reports/Submit" button
  else
    if ($form_state['clicked_button']['#id'] == "edit-submit-email") {
    nsw_planning_reports_create_new_report_to_email($form, $form_state);

    drupal_set_message(t('Thank you for your request. The reports will be emailed to ' . $form_state['input']['email'] . ' once processed.'), 'status');
  }
}

/**
 * custom form validation function
 * @param type $form
 * @param type $form_state
 */
function nsw_planning_reports_custom_form_validation($form, &$form_state) {
  // case : after click on "Email Reports/Submit" button
  if ($form_state['clicked_button']['#id'] == "edit-submit-email") {
    $mail = $form_state['values']['email'];
    if (!valid_email_address($mail)) {
      form_set_error('email', t('Please Enter a valid email address.'));
    }
  }
}



/**
 * function nsw_planning_reports_create_new_report_to_email
 * Allow us to create the node used to generate the pdf then send the email with the report.
 * = Step0
 * @param type $form
 * @param type $form_state
 */
function nsw_planning_reports_create_new_report_to_email($form, &$form_state) {

  // node creation
  $node = new stdClass();
  $node->type = "report_by_email";
  $node->language = LANGUAGE_NONE; // define the language of the content
  // field body : we store the HTML and generate the PDF later > because the PDF library dies after generating the PDF file.
  module_load_include('inc', 'nsw_planning', 'includes/reports/details_pdf_page');
  $bodyHtml = drupal_render(drupal_get_form('nsw_planning_reports_details_pdf_form'));
  $node->body[$node->language][0]['value'] = $bodyHtml;
  $node->body[$node->language][0]['format'] = 'full_html'; // Format full HTML
  // other fields
  $node->field_creation_date[$node->language][0]['value'] = REQUEST_TIME;
  $node->field_update_date[$node->language][0]['value'] = REQUEST_TIME;
  $node->field_report_email[$node->language][0]['value'] = $form_state['input']['email'];
  $node->field_report_status[$node->language][0]['value'] = "1-New report";

  // Node save
  node_object_prepare($node); // add default values
  $node = node_submit($node); // set date + auteur values
  node_save($node); // store the node
  $node->title = 'NSW Interactive Buildings Report ' . date('omd') . '-' . time(); // set the PDF FileName as Title
  node_save($node); // store the node
}
