
jq1102(function ($) {

  window.onload = function ()
  {
    threeStart();
    window.addEventListener('resize', resize3dArea, false);
    resize3dArea();
  };


  var width;
  var marginLeft;   //marginLeft is the number that shows the distance from the Menu label to the left margin of canvas
  function resize3dArea()
  {
    camera.aspect = 16 / 9;
    camera.updateProjectionMatrix();
    width = $('#canvas_container').width();

    if (width > 960)
    {
      width = 960;
    }
    renderer.setSize(width, width / (16 / 9));

    marginLeft = 310; //parseInt($('.view-alteration-object .view-footer').css('marginLeft').replace('px', '')) + parseInt($('.view-alteration-object .view-content').css('width').replace('px', '')) + parseInt($('.view-alteration-object .view-content').css("left").replace('px', ''));

    //popup_label_scene is the section which overlays on the 3D canvas, and it has the same width and height as the canvas, but it is invisible
    $('#popup_label_scene').css({"width": width, "height": width / (16 / 9)});
    var popup_marginLeft = -(width / 2);
    $('#popup_label_scene').css({"margin-left": popup_marginLeft});
  }

  /*
   Collection of camera positions.
   Camera position names should match the ID on the equivalent navigation link.

   posX, posY, posZ = Position of the camera.
   lookX, lookY, lookZ = Where the camera looks at.
   group = Moving between camera positions of the same group will not trigger the flyover effect. The flyover is needed to avoid clipping through the house and other objects.
   */
  var currentCamLoc = {posX: -160, posY: 115, posZ: 205, lookX: -70, lookY: 65, lookZ: 70, group: 'A'};
  var webglCamLoc = {
    ResetView: {posX: -160, posY: 115, posZ: 205, lookX: -70, lookY: 65, lookZ: 70, group: 'A'},
    DefaultView: {posX: -160, posY: 115, posZ: 205, lookX: -70, lookY: 65, lookZ: 70, group: 'A'},
    COM_AerialsAntennaeRoof: {posX: -60, posY: 120, posZ: 100, lookX: 0, lookY: 66, lookZ: 40, group: 'A'},
    COM_ATM: {posX: -90, posY: 25, posZ: -100, lookX: 0, lookY: -4, lookZ: -60, group: 'A'},
    COM_AwningsBlindsCanopies: {posX: -100, posY: 50, posZ: 130, lookX: -10, lookY: 30, lookZ: 40, group: 'A'},
    COM_Blinds: {posX: -90, posY: 25, posZ: 50, lookX: -40, lookY: 15, lookZ: 10, group: 'A'},
    COM_BuildingIdentificationSigns: {posX: -100, posY: 90, posZ: 65, lookX: -20, lookY: 60, lookZ: 0, group: 'A'},
    COM_ChangeOfUse: {posX: -20, posY: 20, posZ: 100, lookX: 0, lookY: 15, lookZ: 65, group: 'A'},
    COM_CommunityNotice: {posX: -160, posY: 115, posZ: 205, lookX: -70, lookY: 65, lookZ: 70, group: 'A'},
    COM_Demolition: {posX: -160, posY: 115, posZ: 205, lookX: -70, lookY: 65, lookZ: 70, group: 'A'},
    COM_DoorsWindows: {posX: -135, posY: 95, posZ: 110, lookX: -40, lookY: 26, lookZ: 25, group: 'A'},
    COM_ElectionSigns: {posX: -60, posY: 18, posZ: -55, lookX: -10, lookY: 15, lookZ: -25, group: 'A'},
    COM_EmergencyWork: {posX: -160, posY: 115, posZ: 205, lookX: -70, lookY: 65, lookZ: 70, group: 'A'},
    COM_FasciaSigns: {posX: -70, posY: 30, posZ: 80, lookX: -40, lookY: 25, lookZ: 60, group: 'A'},
    COM_Letterboxes: {posX: -20, posY: 20, posZ: 100, lookX: 0, lookY: 10, lookZ: 65, group: 'A'},
    COM_MaintenanceBuildingsDraftHeritage: {posX: -160, posY: 115, posZ: 205, lookX: -70, lookY: 65, lookZ: 70, group: 'A'},
    COM_MinorExternalBuildingAlterations: {posX: -95, posY: 45, posZ: 120, lookX: -35, lookY: 20, lookZ: 60, group: 'A'},
    COM_RealEstateSigns: {posX: -75, posY: 25, posZ: 50, lookX: -35, lookY: 11, lookZ: 25, group: 'A'},
    COM_SecurityScreens: {posX: -85, posY: 25, posZ: 55, lookX: -35, lookY: 13, lookZ: 0, group: 'A'},
    COM_SolarEnergySystems: {posX: -50, posY: 110, posZ: 85, lookX: -10, lookY: 80, lookZ: 40, group: 'A'},
    COM_TopHamperSigns: {posX: -60, posY: 25, posZ: 70, lookX: -10, lookY: 20, lookZ: 35, group: 'A'},
    COM_UnderAwningSigns: {posX: -65, posY: 22, posZ: 80, lookX: -28, lookY: 22, lookZ: 52, group: 'A'},
    COM_WallSigns: {posX: -85, posY: 50, posZ: 54, lookX: -40, lookY: 47, lookZ: 30, group: 'A'},
    COM_WindowSigns: {posX: -80, posY: 25, posZ: 30, lookX: -40, lookY: 15, lookZ: 10, group: 'A'},
    COM_AccessRamps: {posX: -60, posY: 35, posZ: -170, lookX: -10, lookY: 1, lookZ: -110, group: 'B'},
    COM_AerialsAntennaeGround: {posX: -20, posY: 65, posZ: -205, lookX: 30, lookY: 14, lookZ: -133, group: 'B'},
    COM_AirConditioningUnitsWall: {posX: -45, posY: 50, posZ: -150, lookX: 45, lookY: 17, lookZ: -40, group: 'B'},
    COM_AirConditioningUnitsGround: {posX: -45, posY: 30, posZ: -160, lookX: 40, lookY: -36, lookZ: -25, group: 'B'},
    COM_Bollards: {posX: -40, posY: 30, posZ: -185, lookX: -18, lookY: 7, lookZ: -230, group: 'B'},
    COM_CharityBins: {posX: -85, posY: 50, posZ: -295, lookX: -40, lookY: 14, lookZ: -255, group: 'B'},
    COM_Driveways: {posX: -135, posY: 50, posZ: -270, lookX: 0, lookY: -35, lookZ: -182, group: 'B'},
    COM_FencesAdjacentRoad: {posX: -100, posY: 40, posZ: -220, lookX: 0, lookY: -14, lookZ: -145, group: 'B'},
    COM_FencesIndustrial: {posX: -100, posY: 50, posZ: -140, lookX: 35, lookY: -5, lookZ: -195, group: 'B'},
    COM_Hardstanding: {posX: -100, posY: 50, posZ: -220, lookX: 0, lookY: -13, lookZ: -155, group: 'B'},
    COM_HotWaterSystems: {posX: -30, posY: 25, posZ: -150, lookX: 15, lookY: 4, lookZ: -100, group: 'B'},
    COM_PrivacyScreens: {posX: -65, posY: 35, posZ: -195, lookX: 32, lookY: -23, lookZ: -80, group: 'B'},
    COM_PathwaysPaving: {posX: -70, posY: 60, posZ: -265, lookX: 0, lookY: 15, lookZ: -205, group: 'B'},
    COM_RainwaterTanks: {posX: -40, posY: 35, posZ: -150, lookX: 20, lookY: 2, lookZ: -100, group: 'B'},
    COM_Scaffolding: {posX: -65, posY: 35, posZ: -160, lookX: -30, lookY: 11, lookZ: -120, group: 'B'},
    COM_SiteFencesHoardings: {posX: -65, posY: 35, posZ: -160, lookX: -30, lookY: 11, lookZ: -120, group: 'B'},
    COM_TemporaryBuildersStructures: {posX: -25, posY: 30, posZ: -195, lookX: 25, lookY: 15, lookZ: -155, group: 'B'},
    COM_MIBA_Bathroom: {posX: 38, posY: 15, posZ: -40, lookX: 5, lookY: 1, lookZ: -85, group: 'F'},
    COM_MIBA_Kitchen: {posX: 20, posY: 17, posZ: -50, lookX: 55, lookY: 6, lookZ: -63, group: 'F'},
    COM_MIBA_Office: {posX: 20, posY: 25, posZ: 48, lookX: 0, lookY: 13, lookZ: 25, group: 'F'},
    COM_MIBA_Stairs: {posX: 35, posY: 15, posZ: -50, lookX: 0, lookY: 3, lookZ: -59, group: 'F'},
    COM_InternalSigns: {posX: -20, posY: 18, posZ: 15, lookX: -5, lookY: 16, lookZ: 0, group: 'F'},
    COM_MinorInternalBuildingAlterations: {posX: -20, posY: 18, posZ: 15, lookX: -5, lookY: 15, lookZ: 0, group: 'F'},
    BuildingAlterationsExternalMenu: {posX: -100, posY: 55, posZ: -165, lookX: -30, lookY: 25, lookZ: -100, group: 'B'},
    OutdoorItemsMenu: {posX: -135, posY: 65, posZ: -245, lookX: -40, lookY: 25, lookZ: -185, group: 'B'},
    FencesWallsMenu: {posX: -100, posY: 50, posZ: -140, lookX: 35, lookY: 5, lookZ: -195, group: 'B'},
    RoofMenu: {posX: -40, posY: 120, posZ: 105, lookX: 20, lookY: 40, lookZ: 0, group: 'A'},
    SignsMenu: {posX: -90, posY: 30, posZ: 80, lookX: -40, lookY: 32, lookZ: 50, group: 'A'},
    MaintenanceMenu: {posX: -100, posY: 55, posZ: -200, lookX: -40, lookY: 25, lookZ: -145, group: 'B'},
  };

  var canvasCamLoc = {
    ResetView: {posX: -160, posY: 115, posZ: 205, lookX: -70, lookY: 65, lookZ: 70, group: 'A'},
    DefaultView: {posX: -160, posY: 115, posZ: 205, lookX: -70, lookY: 65, lookZ: 70, group: 'A'},
    COM_AerialsAntennaeRoof: {posX: -20, posY: 115, posZ: 100, lookX: 15, lookY: 75, lookZ: 43, group: 'A'},
    COM_ATM: {posX: -80, posY: 16, posZ: -65, lookX: 0, lookY: -4, lookZ: -65, group: 'A'},
    COM_AwningsBlindsCanopies: {posX: -90, posY: 50, posZ: 120, lookX: -10, lookY: 30, lookZ: 40, group: 'A'},
    COM_Blinds: {posX: -124, posY: 26, posZ: 8, lookX: -35, lookY: 12, lookZ: 5, group: 'A'},
    COM_BuildingIdentificationSigns: {posX: -115, posY: 65, posZ: -10, lookX: -68, lookY: 65, lookZ: -10, group: 'A'},
    COM_ChangeOfUse: {posX: -20, posY: 20, posZ: 100, lookX: 0, lookY: 15, lookZ: 65, group: 'A'},
    COM_CommunityNotice: {posX: -160, posY: 115, posZ: 205, lookX: -70, lookY: 65, lookZ: 70, group: 'A'},
    COM_Demolition: {posX: -160, posY: 115, posZ: 205, lookX: -70, lookY: 65, lookZ: 70, group: 'A'},
    COM_DoorsWindows: {posX: -135, posY: 95, posZ: 110, lookX: -40, lookY: 26, lookZ: 25, group: 'A'},
    COM_ElectionSigns: {posX: -80, posY: 18, posZ: -35, lookX: -20, lookY: 15, lookZ: -35, group: 'A'},
    COM_EmergencyWork: {posX: -160, posY: 115, posZ: 205, lookX: -70, lookY: 65, lookZ: 70, group: 'A'},
    COM_FasciaSigns: {posX: -65, posY: 30, posZ: 90, lookX: -30, lookY: 28, lookZ: 55, group: 'A'},
    COM_Letterboxes: {posX: -10, posY: 20, posZ: 100, lookX: 0, lookY: 10, lookZ: 65, group: 'A'},
    COM_MaintenanceBuildingsDraftHeritage: {posX: -160, posY: 115, posZ: 205, lookX: -70, lookY: 65, lookZ: 70, group: 'A'},
    COM_MinorExternalBuildingAlterations: {posX: -95, posY: 29, posZ: 120, lookX: -35, lookY: 20, lookZ: 60, group: 'A'},
    COM_RealEstateSigns: {posX: -95, posY: 16, posZ: 20, lookX: -35, lookY: 11, lookZ: 20, group: 'A'},
    COM_SecurityScreens: {posX: -106, posY: 14, posZ: 2, lookX: -26, lookY: 10, lookZ: 2, group: 'A'},
    COM_SolarEnergySystems: {posX: 0, posY: 115, posZ: 85, lookX: 10, lookY: 80, lookZ: 40, group: 'A'},
    COM_TopHamperSigns: {posX: -40, posY: 23, posZ: 70, lookX: -15, lookY: 23, lookZ: 45, group: 'A'},
    COM_UnderAwningSigns: {posX: -56, posY: 22, posZ: 70, lookX: -28, lookY: 22, lookZ: 70, group: 'A'},
    COM_WallSigns: {posX: -90, posY: 45, posZ: 22, lookX: -40, lookY: 45, lookZ: 22, group: 'A'},
    COM_WindowSigns: {posX: -90, posY: 15, posZ: 5, lookX: -37, lookY: 15, lookZ: 5, group: 'A'},
    COM_AccessRamps: {posX: -30, posY: 35, posZ: -160, lookX: -10, lookY: 1, lookZ: -120, group: 'B'},
    COM_AerialsAntennaeGround: {posX: -20, posY: 65, posZ: -205, lookX: 30, lookY: 14, lookZ: -133, group: 'B'},
    COM_AirConditioningUnitsWall: {posX: -45, posY: 50, posZ: -150, lookX: 45, lookY: 17, lookZ: -40, group: 'B'},
    COM_AirConditioningUnitsGround: {posX: -45, posY: 30, posZ: -160, lookX: 40, lookY: -36, lookZ: -25, group: 'B'},
    COM_Bollards: {posX: -40, posY: 30, posZ: -185, lookX: -18, lookY: 7, lookZ: -230, group: 'B'},
    COM_CharityBins: {posX: -85, posY: 50, posZ: -295, lookX: -40, lookY: 14, lookZ: -255, group: 'B'},
    COM_Driveways: {posX: -135, posY: 50, posZ: -270, lookX: 0, lookY: -35, lookZ: -182, group: 'B'},
    COM_FencesAdjacentRoad: {posX: -100, posY: 40, posZ: -220, lookX: 0, lookY: -14, lookZ: -145, group: 'B'},
    COM_FencesIndustrial: {posX: -100, posY: 50, posZ: -140, lookX: 35, lookY: -5, lookZ: -195, group: 'B'},
    COM_Hardstanding: {posX: -100, posY: 50, posZ: -220, lookX: 0, lookY: -13, lookZ: -155, group: 'B'},
    COM_HotWaterSystems: {posX: 0, posY: 50, posZ: -163, lookX: 15, lookY: 4, lookZ: -105, group: 'B'},
    COM_PrivacyScreens: {posX: -65, posY: 35, posZ: -195, lookX: 32, lookY: -10, lookZ: -80, group: 'B'},
    COM_PathwaysPaving: {posX: -70, posY: 60, posZ: -265, lookX: 0, lookY: 15, lookZ: -205, group: 'B'},
    COM_RainwaterTanks: {posX: 16, posY: 40, posZ: -165, lookX: 22, lookY: 2, lookZ: -105, group: 'B'},
    COM_Scaffolding: {posX: -65, posY: 35, posZ: -160, lookX: -30, lookY: 11, lookZ: -120, group: 'B'},
    COM_SiteFencesHoardings: {posX: -65, posY: 35, posZ: -160, lookX: -30, lookY: 11, lookZ: -120, group: 'B'},
    COM_TemporaryBuildersStructures: {posX: -25, posY: 30, posZ: -195, lookX: 25, lookY: 15, lookZ: -155, group: 'B'},
    COM_MIBA_Bathroom: {posX: 17, posY: 14, posZ: -35, lookX: 12, lookY: 1, lookZ: -90, group: 'F'},
    COM_MIBA_Kitchen: {posX: 20, posY: 17, posZ: -55, lookX: 55, lookY: 6, lookZ: -63, group: 'F'},
    COM_MIBA_Office: {posX: 16, posY: 25, posZ: 45, lookX: 0, lookY: 15, lookZ: 29, group: 'F'},
    COM_MIBA_Stairs: {posX: 39, posY: 13, posZ: -54, lookX: 0, lookY: 3, lookZ: -58, group: 'F'},
    COM_InternalSigns: {posX: 16, posY: 15, posZ: 17, lookX: 18, lookY: 15, lookZ: -33, group: 'F'},
    COM_MinorInternalBuildingAlterations: {posX: 10, posY: 10, posZ: 30, lookX: 10, lookY: 10, lookZ: -40, group: 'F'},
    BuildingAlterationsExternalMenu: {posX: -100, posY: 55, posZ: -165, lookX: -30, lookY: 25, lookZ: -100, group: 'B'},
    OutdoorItemsMenu: {posX: -135, posY: 65, posZ: -245, lookX: -40, lookY: 25, lookZ: -185, group: 'B'},
    FencesWallsMenu: {posX: -100, posY: 50, posZ: -140, lookX: 35, lookY: 5, lookZ: -195, group: 'B'},
    RoofMenu: {posX: -40, posY: 120, posZ: 105, lookX: 20, lookY: 40, lookZ: 0, group: 'A'},
    SignsMenu: {posX: -90, posY: 30, posZ: 80, lookX: -40, lookY: 32, lookZ: 50, group: 'A'},
    MaintenanceMenu: {posX: -100, posY: 55, posZ: -200, lookX: -40, lookY: 25, lookZ: -145, group: 'B'},
  };

  var objInfo = {
    COM_AccessRamps: {menu: "BuildingAlterationsExternalMenu", ghost: true, highlightable: true},
    COM_AirConditioningUnitsWall: {menu: "BuildingAlterationsExternalMenu", ghost: true, highlightable: true},
    COM_ATM: {menu: "BuildingAlterationsExternalMenu", ghost: true, highlightable: true},
    COM_AwningsBlindsCanopies: {menu: "BuildingAlterationsExternalMenu", ghost: false, highlightable: true},
    COM_Blinds: {menu: "BuildingAlterationsExternalMenu", ghost: true, highlightable: true},
    COM_HotWaterSystems: {menu: "BuildingAlterationsExternalMenu", ghost: true, highlightable: true},
    COM_SecurityScreens: {menu: "BuildingAlterationsExternalMenu", ghost: true, highlightable: true},
    COM_Letterboxes: {menu: "BuildingAlterationsExternalMenu", ghost: true, highlightable: true},
    COM_DoorsWindows: {menu: "BuildingAlterationsExternalMenu", ghost: false, highlightable: true},
    COM_MinorExternalBuildingAlterations: {menu: "BuildingAlterationsExternalMenu", ghost: false, highlightable: true},
    COM_MinorInternalBuildingAlterations: {menu: "BuildingAlterationsInternalMenu", ghost: false, highlightable: true},
    COM_InternalSigns: {menu: "BuildingAlterationsInternalMenu", ghost: true, highlightable: true},
    COM_BathroomAlterations: {menu: "BuildingAlterationsInternalMenu", ghost: false, highlightable: true},
    COM_OfficeAlterations: {menu: "BuildingAlterationsInternalMenu", ghost: false, highlightable: true},
    COM_Kitchen: {menu: "BuildingAlterationsInternalMenu", ghost: false, highlightable: false},
    COM_KitchenAlterations: {menu: "BuildingAlterationsInternalMenu", ghost: false, highlightable: true},
    COM_Room: {menu: "BuildingAlterationsInternalMenu", ghost: false, highlightable: false},
    COM_Hall: {menu: "BuildingAlterationsInternalMenu", ghost: false, highlightable: false},
    COM_InternalDoor: {menu: "BuildingAlterationsInternalMenu", ghost: false, highlightable: false},
    COM_InternalShelving: {menu: "BuildingAlterationsInternalMenu", ghost: false, highlightable: false},
    COM_InternalStair: {menu: "BuildingAlterationsInternalMenu", ghost: false, highlightable: true},
    COM_ChangeOfUse: {menu: "BuildingAlterationsInternalMenu", ghost: false, highlightable: true},
    COM_FencesIndustrial: {menu: "FencesWallsMenu", ghost: true, highlightable: true},
    COM_FencesAdjacentRoad: {menu: "FencesWallsMenu", ghost: true, highlightable: true},
    COM_Bollards: {menu: "OutdoorItemsMenu", ghost: true, highlightable: true},
    COM_Driveways: {menu: "OutdoorItemsMenu", ghost: false, highlightable: true},
    COM_Hardstanding: {menu: "OutdoorItemsMenu", ghost: false, highlightable: true},
    COM_CharityBins: {menu: "OutdoorItemsMenu", ghost: true, highlightable: true},
    COM_RainwaterTanks: {menu: "OutdoorItemsMenu", ghost: true, highlightable: true},
    COM_PrivacyScreens: {menu: "OutdoorItemsMenu", ghost: true, highlightable: true},
    COM_AirConditioningUnitsGround: {menu: "OutdoorItemsMenu", ghost: true, highlightable: true},
    COM_PathwaysPaving: {menu: "OutdoorItemsMenu", ghost: true, highlightable: true},
    COM_AerialsAntennaeGround: {menu: "OutdoorItemsMenu", ghost: true, highlightable: true},
    COM_SolarEnergySystems: {menu: "RoofMenu", ghost: true, highlightable: true},
    COM_AerialsAntennaeRoof: {menu: "RoofMenu", ghost: true, highlightable: true},
    COM_BuildingIdentificationSigns: {menu: "SignsMenu", ghost: true, highlightable: true},
    COM_WallSigns: {menu: "SignsMenu", ghost: true, highlightable: true},
    COM_RealEstateSigns: {menu: "SignsMenu", ghost: true, highlightable: true},
    COM_ElectionSigns: {menu: "SignsMenu", ghost: true, highlightable: true},
    COM_FasciaSigns: {menu: "SignsMenu", ghost: true, highlightable: true},
    COM_UnderAwningSigns: {menu: "SignsMenu", ghost: true, highlightable: true},
    COM_TopHamperSigns: {menu: "SignsMenu", ghost: true, highlightable: true},
    COM_WindowSigns: {menu: "SignsMenu", ghost: true, highlightable: true},
    COM_TemporaryBuildersStructures: {menu: "MaintenanceMenu", ghost: true, highlightable: true},
    COM_Scaffolding: {menu: "MaintenanceMenu", ghost: true, highlightable: true},
    COM_SiteFencesHoardings: {menu: "MaintenanceMenu", ghost: true, highlightable: true},
    COM_ChangeOfUse: {menu: "ChangeOfUseMenu", ghost: true, highlightable: true},
  };


  var scene, renderer, camera, light1, light2, light3, loader, projector, raycaster;
  var mouse = new THREE.Vector2(), INTERSECTED;

  var obj, vector, intersects;
  var currentAnimation = "";
  var currentFocus = "default";
  var numObjects = 51;
  var loaded = 0;
  var objArray = [];
  var toggleableArray = [];
  var objectColour = {
    body: "0x99a1a3",
    outline: "0x4D4D4D",
    highlight: "0x009be4",
    hover: "0x4D4D4D",
    grass: "0x80BB80"
  };
  var webgl = false;
  var canvas = null;
  var offsetX, offsetY;
  var renderDepth = 0;
  var hotspotObj = null;
  var tabRequireOpen = true;
  var tabRestrictOpen = false;
  var houseInternal = false;


  function threeStart()
  {
    initScene();
    initCamera();
    initSky();
    initLight();
    initObjects();
    detectUserDevice();
    render();
  }


  function initScene()
  {
    scene = new THREE.Scene();
    canvas = document.createElement('canvas');
    canvas.id = "CanvasID";
    if (Detector.webgl)
    {
      renderer = new THREE.WebGLRenderer({canvas: canvas, antialias: true});
      webgl = true;
    } else
    {
      renderer = new THREE.CanvasRenderer({canvas: canvas});
      webgl = false;
    }
    renderer.setSize(window.innerWidth, window.innerHeight);
    renderer.setClearColor(0xFFFFFF, 1, 0);
    document.getElementById("canvas_container").appendChild(renderer.domElement);
    $("#canvas_container").css({"-webkit-tap-highlight-color": "rgba(0, 0, 0, 0)", "-webkit-touch-callout": "none"});
  }


  function initCamera()
  {
    camera = new THREE.PerspectiveCamera(55, window.innerWidth / window.innerHeight, 1, 1000);
    camera.position.set(currentCamLoc.posX, currentCamLoc.posY, currentCamLoc.posZ);
    camera.up.x = 0;
    camera.up.y = 1;
    camera.up.z = 0;

    camera.lookAt({x: currentCamLoc.lookX, y: currentCamLoc.lookY, z: currentCamLoc.lookZ});
    scene.add(camera);
  }


  function initSky()
  {
    if (webgl)
    {
      scene.fog = new THREE.Fog(0xffffff, 1, 5000);
      var vertexShader = "varying vec3 vWorldPosition; void main() { vec4 worldPosition = modelMatrix * vec4( position, 1.0 ); vWorldPosition = worldPosition.xyz; gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 ); }";
      var fragmentShader = "uniform vec3 topColor; uniform vec3 bottomColor; uniform float offset; uniform float exponent; varying vec3 vWorldPosition; void main() { float h = normalize( vWorldPosition + offset ).y; gl_FragColor = vec4( mix( bottomColor, topColor, max( pow( h, exponent ), 0.0 ) ), 1.0 );}";
      var uniforms = {
        topColor: {type: "c", value: new THREE.Color(0x0077ff)},
        bottomColor: {type: "c", value: new THREE.Color(0xffffff)},
        offset: {type: "f", value: 0},
        exponent: {type: "f", value: 0.6}
      };
      scene.fog.color.copy(uniforms.bottomColor.value);
      var skyGeo = new THREE.SphereGeometry(750, 32, 15);
      var skyMat = new THREE.ShaderMaterial({vertexShader: vertexShader, fragmentShader: fragmentShader, uniforms: uniforms, side: THREE.BackSide});
      var skybox = new THREE.Mesh(skyGeo, skyMat);


      var groundGeo = new THREE.PlaneGeometry(10000, 10000);
      var groundMat = new THREE.MeshPhongMaterial({ambient: 0xffffff, color: 0x80BB80, specular: 0x050505});
      var ground = new THREE.Mesh(groundGeo, groundMat);
      ground.rotation.x = -Math.PI / 2;
      ground.position.y = 0;
      scene.add(skybox);
      scene.add(ground);
    }
  }


  function initLight()
  {
    if (webgl)
    {
      light1 = new THREE.HemisphereLight(0xFFFFFF, 1.2, 1.1);
      light1.position.set(-70, 137, 162);
      light2 = new THREE.PointLight(0xFFFFFF, 0.7, 0);
      light2.position.set(70, 170, -310);
      light3 = new THREE.PointLight(0xFFFFFF, 0.5, 0);
      light3.position.set(-150, 32, -70);
      scene.add(light1);
      scene.add(light2);
      scene.add(light3);
    } else
    {
      light1 = new THREE.DirectionalLight(0xFFFFFF, 1, 0);
      light1.position.set(-60, 137, 142);
      light2 = new THREE.DirectionalLight(0xFFFFFF, 1, 0);
      light2.position.set(90, 71, -230);
      light3 = new THREE.DirectionalLight(0xFFFFFF, 1, 0);
      light3.position.set(-150, 32, -70);
      scene.add(light1);
      scene.add(light2);
      scene.add(light3);

    }
  }


  function initObjects()
  {
    if (webgl)
    {
      loadObjects("COM_AccessRamps", "none", "none", 0, 0.3, 0);
      loadObjects("COM_AirConditioningUnitsWall", "none", "none", 0, 0, 0);
      loadObjects("COM_AirConditioningUnitsGround", "none", "none", 0, 0, 0);
      loadObjects("COM_Atm", "none", "none", 0, 0, 0);
      loadObjects("COM_AwningsBlindsCanopies", "none", "none", 0, 0, 0);
      loadObjects("COM_Blinds", "none", "none", 0, 0, 0);
      loadObjects("COM_Bollards", "none", "none", 0, 0, 0);
      loadObjects("COM_CharityBins", "none", "none", 0, 0, 0);
      loadObjects("COM_CommercialWalls", "none", "BuildingAlterationsExternalMenu", 0, 0, 0);
      loadObjects("COM_DoorsWindows", "none", "BuildingAlterationsInternalMenu", 0, 0, 0);
      loadObjects("COM_CommercialGround", "none", "none", 0, 0, 0);
      loadObjects("COM_CommercialRoof", "none", "RoofMenu", 0, 0.1, 0);
      loadObjects("COM_Road", "none", "OutdoorItemsMenu", 0, 0.05, 0);
      loadObjects("COM_PathwaysPaving", "none", "none", 0, 0.1, 0);
      loadObjects("COM_HotWaterSystems", "none", "none", -40, 0, 0);
      loadObjects("COM_RainwaterTanks", "none", "none", -40, 0, 0);
      loadObjects("COM_SolarEnergySystems", "none", "none", 0, 0, 0);
      loadObjects("COM_PrivacyScreens", "none", "none", 0, 0, 0);
      loadObjects("COM_RealEstateSigns", "none", "signsMenu", 0, 0, 0);
      loadObjects("COM_ElectionSigns", "none", "none", 0, 0, 0);
      loadObjects("COM_Letterboxes", "none", "none", 0, 0, 0);
      loadObjects("COM_ChangeOfUse", "none", "none", 0, 0, 0);
      loadObjects("COM_FencesIndustrial", "none", "FencesWallsMenu", 0, 0, 0);
      loadObjects("COM_FencesAdjacentRoad", "none", "FencesWallsMenu", 0, 0, 0);
      loadObjects("COM_TemporaryBuildersStructures", "none", "none", 0, 0, 0);
      loadObjects("COM_AerialsAntennaeRoof", "none", "none", 0, 0, 0);
      loadObjects("COM_AerialsAntennaeGround", "none", "none", 0, 0, 0);
      loadObjects("COM_FasciaSigns", "none", "none", 0, 0, 0);
      loadObjects("COM_TopHamperSigns", "none", "none", 0, 2, 0);
      loadObjects("COM_UnderAwningSigns", "none", "none", 0, 0, 0);
      loadObjects("COM_WallSigns", "none", "none", 0, 0, 0);
      loadObjects("COM_WindowSigns", "none", "none", 0, 0, 0);
      loadObjects("COM_BuildingIdentificationSigns", "none", "none", 0, 0, 0);
      loadObjects("COM_Scaffolding", "none", "none", 0, 0, 0);
      loadObjects("COM_SiteFencesHoardings", "none", "none", 0, 0, 0);
      loadObjects("COM_SecurityScreens", "none", "none", 0, 0, 0);
      loadObjects("COM_BathroomAlterations", "COM_MIBA_Bathroom", "none", 0, 0.055, 0);
      loadObjects("COM_Hall", "COM_MIBA_Stairs", "none", 0, 0.055, 0);
      loadObjects("COM_InternalDoor", "COM_MIBA_Stairs", "none", 0, 0.055, 0);
      loadObjects("COM_InternalStair", "COM_MIBA_Stairs", "none", 0, 0.055, 0);
      loadObjects("COM_InternalShelving", "COM_MIBA_Stairs", "none", 0, 0.055, 0);
      loadObjects("COM_KitchenAlterations", "COM_MIBA_Kitchen", "none", 0, 0.055, 0);
      loadObjects("COM_OfficeAlterations", "COM_MIBA_Office", "none", 0, 0.055, 0);
      loadObjects("COM_Room", "none", "none", 0, 0.055, 0);
      loadObjects("COM_InternalSigns", "none", "none", 0, 0, 0);
      loadObjects("COM_MinorInternalBuildingAlterations", "none", "none", 0, 0, 0);
      loadObjects("COM_CommercialInterior", "COM_MIBA_Office", "none", 0, 0.055, 0);
      loadObjects("COM_MinorExternalBuildingAlterations", "none", "none", 0, 0, 0);
      loadObjects("COM_RoadNew", "none", "none", 0, 0, 0);
      loadObjects("COM_Driveways", "none", "none", 0, 0.15, 0);
      loadObjects("COM_Hardstanding", "none", "none", 0, 0.15, 0);
    } else
    {
      loadObjects("COM_AccessRamps", "none", "none", 0, 1, 0);
      loadObjects("COM_AirConditioningUnitsWall", "none", "none", 0, 0, -2);
      loadObjects("COM_AirConditioningUnitsGround", "none", "none", 0, 0, 0);
      loadObjects("COM_Atm", "none", "none", 0, 0, 0);
      loadObjects("COM_AwningsBlindsCanopies", "none", "none", 0, 0, 0);
      loadObjects("COM_Blinds", "none", "none", 0, 0, 0);
      loadObjects("COM_Bollards", "none", "none", 0, 0, 0);
      loadObjects("COM_CharityBins", "none", "none", 0, 0, 0);
      loadObjects("COM_CommercialWalls", "none", "BuildingAlterationsExternalMenu", 0, 0, 0);
      loadObjects("COM_DoorsWindows", "none", "BuildingAlterationsInternalMenu", 0, 0, 0);
      loadObjects("COM_Grass", "none", "none", 0, -10, 0);
      loadObjects("COM_Road", "none", "outdoorItemsMenu", 0, 0.05, 0);
      loadObjects("COM_PathwaysPaving", "none", "none", 0, 0.1, 0);
      loadObjects("COM_Driveways", "none", "none", 0, 0.15, 0);
      loadObjects("COM_Hardstanding", "none", "none", 0, 1, 0);
      loadObjects("COM_CommercialRoofCanvas", "none", "RoofMenu", 0, 0.1, 0);
      loadObjects("COM_HotWaterSystems", "none", "none", -40, 0, 0);
      loadObjects("COM_RainwaterTanks", "none", "none", -40, 0, 0);
      loadObjects("COM_SolarEnergySystems", "none", "none", 0, 0, 0);
      loadObjects("COM_PrivacyScreens", "none", "none", 0, 0, 0);
      loadObjects("COM_RealEstateSigns", "none", "SignsMenu", 0, 0, 0);
      loadObjects("COM_ElectionSigns", "none", "none", 0, 0, 0);
      loadObjects("COM_Letterboxes", "none", "none", 0, 0, -2);
      loadObjects("COM_ChangeOfUse", "none", "none", 0, 0, 0);
      loadObjects("COM_FencesIndustrial", "none", "FencesWallsMenu", 0, 0, 0);
      loadObjects("COM_FencesAdjacentRoad", "none", "FencesWallsMenu", 0, 0.5, 0);
      loadObjects("COM_TemporaryBuildersStructures", "none", "none", 0, 0, 0);
      loadObjects("COM_AerialsAntennaeRoof", "none", "none", 0, 0, 0);
      loadObjects("COM_AerialsAntennaeGround", "none", "none", 0, 0, 0);
      loadObjects("COM_FasciaSigns", "none", "none", 0, 0, 0);
      loadObjects("COM_TopHamperSigns", "none", "none", 0, 2, 0);
      loadObjects("COM_UnderAwningSigns", "none", "none", 0, 0, 0);
      loadObjects("COM_WallSigns", "none", "none", 0, 0, 0);
      loadObjects("COM_WindowSigns", "none", "none", 0, 0, 0);
      loadObjects("COM_BuildingIdentificationSigns", "none", "none", 0, 0, 0);
      loadObjects("COM_Scaffolding", "none", "none", 0, 0, 0);
      loadObjects("COM_SiteFencesHoardings", "none", "none", 0, 0, 0);
      loadObjects("COM_SecurityScreens", "none", "none", 0, 0, 0);
      loadObjects("COM_BathroomAlterations", "COM_MIBA_Bathroom", "none", 0, 0.055, 0);
      loadObjects("COM_Hall", "COM_MIBA_Stairs", "none", 0, 0.055, 0);
      loadObjects("COM_InternalDoor", "COM_MIBA_Stairs", "none", 0, 0.055, 0);
      loadObjects("COM_InternalStair", "COM_MIBA_Stairs", "none", 0, 0.055, 0);
      loadObjects("COM_InternalShelving", "COM_MIBA_Stairs", "none", 0, 0.055, 0);
      loadObjects("COM_KitchenAlterations", "COM_MIBA_Kitchen", "none", 0, 0.055, 0);
      loadObjects("COM_OfficeAlterations", "COM_MIBA_Office", "none", 0, 0.055, 0);
      loadObjects("COM_Room", "none", "none", 0, 0.055, 0);
      loadObjects("COM_InternalSigns", "none", "none", -5, 0, 0);
      loadObjects("COM_MinorInternalBuildingAlterations", "none", "none", 0, 0, 0);
      loadObjects("COM_MinorExternalBuildingAlterations", "none", "none", 0, 0, 0);
      loadObjects("COM_RoadNew", "none", "none", 0, 0, 0);
      loadObjects("COM_CommercialInterior", "COM_MIBA_Office", "none", 0, 0.055, 0);
    }
  }


  function loadObjects(objectName, objectGroup, hotspotGroup, x, y, z)
  {
    renderDepth += 5;
    var thisRenderDepth = renderDepth;
    loader = new THREE.JSONLoader();
    loader.load(base_path + "/json/commercial/" + objectName + ".js", function load_Obj(geometry, materials)
    {
      var material = new THREE.MeshFaceMaterial(materials);

      var obj = new THREE.Mesh(geometry, material);
      ;
      obj.name = objectName;
      obj.scale.set(1, 1, 1);
      obj.position.set(x, y, z);
      obj.group = (objectGroup == "none") ? objectName : objectGroup;
      obj.hotspot = hotspotGroup;
      obj.textureLock = false;
      obj.visible = false;
      obj.renderDepth = thisRenderDepth;


      for (var i = 0; i < obj.material.materials.length; i++) {
        if (obj.material.materials[i].name == "outline") {
          obj.material.materials[i].color.setHex(objectColour['outline']);
        } else if (obj.material.materials[i].name == "body") {
          obj.material.materials[i].color.setHex(objectColour['body']);
        } else if (obj.material.materials[i].name == "grass") {
          obj.material.materials[i].color.setHex(objectColour['grass']);
        }
        obj.material.materials[i].overdraw = 1;
        obj.material.materials[i].defaultOpacity = obj.material.materials[i].opacity;
      }

      $.each(objInfo, function (key, info) {
        if (key.toLowerCase() == objectName.toLowerCase()) {
          obj.menu = info.menu;
          obj.ghost = info.ghost;
          obj.highlightable = info.highlightable;
        }
      });

      if (obj.menu == undefined) {
        obj.menu = "none";
      }

      scene.add(obj);

      objArray.push(obj);
      loaded++;

      if (loaded == numObjects) {
        toggleableArray = objArray.slice(0);

        if (webgl)
        {
          setAlwaysVisible('COM_CommercialGround');
          setAlwaysVisible('COM_CommercialRoof');
        } else
        {
          setAlwaysVisible('COM_Grass');
          setAlwaysVisible('COM_CommercialRoofCanvas');
        }
        setAlwaysVisible('COM_Driveways');
        setAlwaysVisible('COM_RoadNew');
        setAlwaysVisible('COM_Hardstanding');
        setAlwaysVisible('COM_AwningsBlindsCanopies');
        setAlwaysVisible('COM_Bollards');
        setAlwaysVisible('COM_CharityBins');
        setAlwaysVisible('COM_DoorsWindows');
        setAlwaysVisible('COM_CommercialWalls');
        setAlwaysVisible('COM_Road');
        setAlwaysVisible('COM_Atm');
        setAlwaysVisible('COM_FencesIndustrial');
        setAlwaysVisible('COM_FencesAdjacentRoad');
        setAlwaysVisible('COM_Letterboxes');
        setAlwaysVisible('COM_RealEstateSigns');
        setAlwaysVisible('COM_Room');

        // BANSW0006-142: If "nid" field is present in query string then select object by node ID
        var selectedObj = getValueFromQueryString("nid");
        if (selectedObj != null && selectedObj != undefined) {
          $('.view-alteration-object').find('.moveLink[data-altobj="' + selectedObj + '"]').click().parent('li').parent('ul').addClass('active').prev('h3').addClass('active');
        }
      }

      if (obj.name == "COM_FencesIndustrial" || obj.name == "COM_CommercialInterior")
      {
        for (var i = 0; i < obj.material.materials.length; i++) {
          obj.material.materials[i].side = 2;
        }
      }
    });
  }

  projector = new THREE.Projector();
  raycaster = new THREE.Raycaster();


  function setAlwaysVisible(objectName)
  {
    toggleObject(objectName, true);
    for (var i = 0; i < toggleableArray.length; i++)
    {
      if (objectName.toLowerCase() == toggleableArray[i].name.toLowerCase())
      {
        toggleableArray.splice(i, 1);
      }
    }
  }


  function onDocumentMouseDown(event)
  {
    calcOffsets();

    mouse.x = ((event.clientX - offsetX) / width) * 2 - 1;
    mouse.y = -((event.clientY - offsetY) / (width / (16 / 9))) * 2 + 1;

    vector = new THREE.Vector3(mouse.x, mouse.y, 1);
    projector.unprojectVector(vector, camera);

    raycaster.set(camera.position, vector.sub(camera.position).normalize());

    intersects = raycaster.intersectObjects(scene.children);

    if (intersects.length > 0)
    {
      for (var i = 0; intersects.length > i; i++) {
        if (currentAnimation == "" && intersects[ i ].object.hotspot != "none" && intersects[i].object.hotspot != undefined) {

          //Functions triggered by hotspot start from here, open the menu and close the content panel
          $("#object_content_section").animate({height: 'hide'}, 'fast');
          $('.object-menu').animate({width: 'show'}, 'fast');
          $(".object-menu-wrapper .menu-link").animate({left: "0"}, 'fast');

          //When hotspot is clicked, the original menu should be active
          $('.view-filters').animate({width: 'show'}, 'fast');
          $('.object-menu .view-alteration-object .view-content').show();
          $('.object-menu .search-results-menu .view-content').hide();

          //If the camera angle is inside the menu, set the hotspot as "BuildingAlterationsInternalMenu"
          if (houseInternal == true)
          {
            hotspotObj = "BuildingAlterationsInternalMenu";
          } else {
            hotspotObj = intersects[ i ].object.hotspot;
            hotspotObj = hotspotObj.charAt(0).toUpperCase() + hotspotObj.slice(1);
          }

          //If correct menu is already open don't close it
          if (!$(".view-alteration-object h3" + ' #' + hotspotObj).parent().hasClass('active')) {
            $('ul').removeClass('active');
            $(".view-alteration-object h3" + ' #' + hotspotObj).parent().click();
          }

          //Google analytics tracking code
          ga("send", "event", hotspotObj, "hotspot_click", this.href);
          break;
        }
      }
    } else
    {
      INTERSECTED = null;
    }
  }


  function onDocumentMouseMove(event)
  {
    if (currentAnimation == "") {

      calcOffsets();

      mouse.x = ((event.clientX - offsetX) / width) * 2 - 1;
      mouse.y = -((event.clientY - offsetY) / (width / (16 / 9))) * 2 + 1;

      var mouseCanPosX = event.clientX - offsetX;      //Mouse position X in canvas
      var mouseCanPosY = event.clientY - offsetY;      //Mouse position Y in canvas

      vector = new THREE.Vector3(mouse.x, mouse.y, 1);
      projector.unprojectVector(vector, camera);

      raycaster.set(camera.position, vector.sub(camera.position).normalize());

      intersects = raycaster.intersectObjects(scene.children);

      if (intersects.length > 0)
      {
        for (var i = 0; intersects.length > i; i++) {
          resetHighlight();
          if (intersects[i].object.visible && intersects[i].object.hotspot != "none" && intersects[i].object.hotspot != undefined) {

            highlightHotspotGroup(intersects[i].object.hotspot, "hover");
            if (houseInternal == true)
            {
              displayHotspotLabel("BuildingAlterationsInternalMenu", mouseCanPosX, mouseCanPosY);
            } else
            {
              displayHotspotLabel(intersects[i].object.hotspot, mouseCanPosX, mouseCanPosY);
            }

            break;
          } else
          {
            $('#popup_label_scene').hide();
          }
        }
      } else
      {
        resetHighlight();
        INTERSECTED = null;
      }
    }
  }
  // hide labels when mouse leaves the 3D viewer or mouse enters one of the ui items
  $('#canvas_container').mouseleave(function () {
    $('#popup_label_scene').hide();
  });
  $(".menu-link, .object-menu, .reset-link, #object-wrapper").mouseover(function () {
    $('#popup_label_scene').hide();
  });


  function onDocumentTouchStart(event)
  {
    if (currentAnimation == "") {

      calcOffsets();

      mouse.x = ((event.touches[0].clientX - offsetX) / width) * 2 - 1;
      mouse.y = -((event.touches[0].clientY - offsetY) / (width / (16 / 9))) * 2 + 1;

      vector = new THREE.Vector3(mouse.x, mouse.y, 1);
      projector.unprojectVector(vector, camera);

      raycaster.set(camera.position, vector.sub(camera.position).normalize());

      intersects = raycaster.intersectObjects(scene.children);

      if (intersects.length > 0) {
        if (INTERSECTED != intersects[ 0 ].object)
        {
          for (var i = 0; intersects.length > i; i++) {
            resetHighlight();
            if (intersects[i].object.visible && intersects[i].object.hotspot != "none" && intersects[i].object.hotspot != undefined) {

              $("#object_content_section").animate({height: 'hide'}, 'fast');
              $('.object-menu').animate({width: 'show'}, 'fast');
              $(".object-menu-wrapper .menu-link").animate({left: "0"}, 'fast');

              //When hotspot is clicked, the original menu should be active
              $('.view-filters').animate({width: 'show'}, 'fast');
              $('.object-menu .view-alteration-object .view-content').show();
              $('.object-menu .search-results-menu .view-content').hide();

              $('ul').removeClass('active');
              hotspotObj = intersects[ i ].object.hotspot;
              hotspotObj = hotspotObj.charAt(0).toUpperCase() + hotspotObj.slice(1);

              //If correct menu is already open don't close it
              if (!$(".view-alteration-object h3" + ' #' + hotspotObj).parent().hasClass('active')) {
                $('ul').removeClass('active');
                $(".view-alteration-object h3" + ' #' + hotspotObj).parent().click();
              }

              $(".view-alteration-object h3").removeClass('active');
              $(".view-alteration-object h3" + ' #' + hotspotObj).parent().addClass('active');

              ga("send", "event", hotspotObj, "hotspot_click", this.href);
              break;
            } else
            {
              $('#popup_label_scene').hide();
            }
          }
        }
      } else
      {
        resetHighlight();
        INTERSECTED = null;
      }
    }
  }


  function detectUserDevice()
  {
    if ((Detector.webgl) || (navigator.appVersion.indexOf("MSIE 10") > 0) || (navigator.userAgent.indexOf("Safari") > 0))
    {
      if (navigator.userAgent.indexOf("iPad") < 0)
      {
        document.getElementById("CanvasID").addEventListener('mousemove', onDocumentMouseMove, false);
        document.getElementById("CanvasID").addEventListener('mousedown', onDocumentMouseDown, false);
      } else
      {
        document.getElementById("CanvasID").addEventListener('touchstart', onDocumentTouchStart, false);
      }
    } else
    {
      document.getElementById("CanvasID").addEventListener('touchstart', onDocumentTouchStart, false);
    }
  }


  var render = function () {
    requestAnimationFrame(render);
    TWEEN.update();
    renderer.render(scene, camera);
  };


  //Display the hotspot name in a popup box when mouse is hovering on the hotspot object
  function displayHotspotLabel(hotspotName, mouseCanPosX, mouseCanPosY)
  {
    var objects = findHotspotGroup(hotspotName);      //hotspotName is fencesWallsMenu, roofMenu

    for (var i = 0; i < objects.length; i++)
    {
      if (hotspotName.toLowerCase() == objects[i].hotspot.toLowerCase())
      {
        $('#popup_label_scene span').removeClass("active");
        $('#popup_label_scene').show();
        $('#popup_label_scene #popup_box').show();
        hotspotName = hotspotName.charAt(0).toUpperCase() + hotspotName.slice(1) + "_Label";
        $('#popup_label_scene #' + hotspotName).addClass("active");

        var popup_box_half_width = ($("#popup_label_scene span").width()) / 2;
        var popup_box_height = $("#popup_label_scene #popup_box").height() + 22;

        mouseCanPosX = mouseCanPosX - popup_box_half_width;
        mouseCanPosY = mouseCanPosY - popup_box_height;

        $('#popup_label_scene #popup_box').css({"top": mouseCanPosY, "left": mouseCanPosX});

      }
    }
  }


  //Highlight the object depending on the object group
  function highlightObjectGroup(objectName, highlight)
  {
    if (objectName == "COM_MIBA_Stairs") {
      findObject('COM_Room').visible = false;
    } else
    {
      findObject('COM_Room').visible = true;
    }
    var objects = findObjectGroup(objectName);
    for (var i = 0; i < objArray.length; i++)
    {
      for (var j = 0; j < objects.length; j++)
      {
        if (objArray[i].name.toLowerCase() == objects[j].name.toLowerCase() && objArray[i].highlightable == true)
        {
          if (!objects[j].textureLock)
          {
            setObjectMaterial(objects[j], "body", highlight);
          }
        }
      }
    }
  }


  //Highlight the hotspot depending on the hotspot group
  function highlightHotspotGroup(hotspotName, highlight)
  {
    var objects = findHotspotGroup(hotspotName);
    for (var i = 0; i < objects.length; i++)
    {
      if (!objects[i].textureLock)
      {
        setObjectMaterial(objects[i], "grass", highlight);
        setObjectMaterial(objects[i], "body", highlight);
      }
    }
  }


  function resetOpacity()
  {
    for (var i = 0; i < objArray.length; i++)
    {
      for (var j = 0; j < objArray[i].material.materials.length; j++)
      {
        objArray[i].material.materials[j].opacity = objArray[i].material.materials[j].defaultOpacity;
      }
    }
  }


  //Remove the texture from the objects
  function resetHighlight()
  {
    for (var i = 0; i < objArray.length; i++)
    {
      if (!objArray[i].textureLock)
      {
        setObjectMaterial(objArray[i], "grass", "grass");
        setObjectMaterial(objArray[i], "body", "body");
      }
    }
  }


  //Add a texture to the object
  function setObjectMaterial(object, material, toMaterial)
  {
    for (var i = 0; i < object.material.materials.length; i++)
    {
      if (object.material.materials[i].name == material)
      {
        object.material.materials[i].color.setHex(objectColour[toMaterial]);
      }
    }
  }


  //Hide all the objects
  function makeObjectsInvisible()
  {
    for (var i = 0; i < toggleableArray.length; i++)
    {
      toggleableArray[i].visible = false;
    }
  }


  //Remove all the popup content for showing object details
  function removePopupContent()
  {
    $('#object-wrapper h1#page-title').hide();
    $('#object_content_section').animate({height: 'hide'}, 'fast');
  }


  function closeMenu()
  {
    if ($(".object-menu").is(':visible'))
    {
      $(".object-menu .search-results-menu .view-content").hide();
      $(".object-menu .view-alteration-object .view-content").show();
      $('.object-menu #edit-keyword').val("");
      $('.object-menu #edit-keyword').removeClass("x");
    } else
    {
      $(".object-menu-wrapper .menu-link").animate({left: "0"}, 'fast');
      $(".object-menu").animate({width: 'show'}, 'fast');
    }
  }


  //Find a specific object depending on the object name
  function findObject(objectName) {
    var object;
    for (var i = 0; i < objArray.length; i++)
    {
      if (objectName.toLowerCase() == objArray[i].name.toLowerCase())
      {
        object = objArray[i];
      }
    }
    return object;
  }


  //Find the object group including the particular objects
  function findObjectGroup(objectGroup)
  {
    var objects = [];
    for (var i = 0; i < objArray.length; i++)
    {
      if (objectGroup.toLowerCase() == objArray[i].group.toLowerCase())
      {
        objects.push(objArray[i]);
      }
    }
    return objects;
  }


  //Find the hotspot group including the certain objects
  function findHotspotGroup(hotspotGroup)
  {
    var objects = [];
    for (var i = 0; i < objArray.length; i++) {
      if (hotspotGroup.toLowerCase() == objArray[i].hotspot.toLowerCase()) {
        objects.push(objArray[i]);
      }
    }
    return objects;
  }


  function toggleObjectGroup(name, visibilty)
  {
    if (name == "COM_SiteFencesHoardings" || name == "COM_Scaffolding") {
      findObject('COM_Scaffolding').visible = visibilty;
      findObject('COM_SiteFencesHoardings').visible = visibilty;
    }
    if (name == "COM_InternalSigns") {
      findObject('COM_CommercialInterior').visible = visibilty;
    }
    for (var i = 0; i < objArray.length; i++) {
      if (name.toLowerCase() == objArray[i].group.toLowerCase()) {
        objArray[i].visible = visibilty;
      }
    }
  }


  function toggleObject(name, visibilty)
  {
    for (var i = 0; i < objArray.length; i++) {
      if (name.toLowerCase() == objArray[i].name.toLowerCase()) {
        objArray[i].visible = visibilty;
      }
    }
  }


  function ghostMenuObjectGroup(menuName)
  {
    for (var i = 0; i < objArray.length; i++) {
      if (menuName.toLowerCase() == objArray[i].menu.toLowerCase())
      {
        objArray[i].visible = true;
        if (objArray[i].ghost)
        {
          for (var j = 0; j < objArray[i].material.materials.length; j++) {
            objArray[i].material.materials[j].opacity = 0.5;
          }
        }
      }
    }
  }


  function resetTextureLock()
  {
    for (var i = 0; i < objArray.length; i++)
    {
      objArray[i].textureLock = false;
    }
  }


  function toggleTextureLock(objectGroup, toggle)
  {
    var objects = findObjectGroup(objectGroup);
    for (var i = 0; i < objects.length; i++) {
      objects[i].textureLock = toggle;
    }
  }


  //Calculate the start position of the canvas
  function calcOffsets()
  {
    var rect = canvas.getBoundingClientRect();
    offsetX = rect.left;
    offsetY = rect.top;
  }


  //Find the URL that the user is looking at, which is used for Google Analytics
  function findTabUrl(tabName)
  {
    var objName = $('.moveLink.selected').text();
    objName = objName.replace(/[^a-zA-Z0-9]/g, '').toLowerCase();
    var tabUrl = "/planning-commercial/" + objName + "/" + tabName;
    return tabUrl;
  }


  //Find the URL (FAQ)  that the user is looking at, which is used for Google Analytics
  function findFaqUrl(faq)
  {
    var objName = $('.moveLink.selected').text();
    objName = objName.replace(/[^a-zA-Z0-9]/g, '').toLowerCase();
    faq = faq.replace(/[^a-zA-Z0-9]/g, '').toLowerCase();
    var faqUrl = "/planning-commercial/" + objName + "/faq/" + faq;
    return faqUrl;
  }


  function getValueFromQueryString(key)
  {
    var param = location.search.match(new RegExp(key + "=(.*?)($|\&)", "i"));
    if (param == undefined) {
      return undefined;
    } else {
      return param[1];
    }
  }


  //Check whether the current camera view is inside house or outside house
  function isInsideHouse(objId)
  {
    if ($('.moveLink[data-attr="' + objId + '"]').parent().parent().prev('h3').find('#BuildingAlterationsInternalMenu').length > 0 || objId == "COM_InternalSigns")
    {
      houseInternal = true;
    } else
    {
      houseInternal = false;
    }
  }


  // FAQ dropdown
  $("#object-wrapper").on('click', 'h3.node-title', function () {

    $(this).parent().next($(".field-name-body")).toggle();
    $(this).toggleClass("active");

    if ($(this).hasClass('active'))
    {
      ga("send", "pageview", {page: findFaqUrl($(this).text())});
    }
  });


  //Get the URL when user clicks the requirements tab
  $('#object-wrapper').on('click', '#requirements-tab', function () {

    if (($(this).hasClass('ui-tabs-active')) && (tabRequireOpen == false))
    {
      tabRestrictOpen = false;
      tabRequireOpen = true;
      ga("send", "pageview", {page: findTabUrl("requirements")});
    }
  });


  //Get the URL when user clicks the restrictions tab
  $('#object-wrapper').on('click', '#restrictions-tab', function () {

    if (($(this).hasClass('ui-tabs-active')) && (tabRestrictOpen == false))
    {
      tabRestrictOpen = true;
      tabRequireOpen = false;
      ga("send", "pageview", {page: findTabUrl("restrictions")});
    }
  });


  // Click on "MENU" tab
  $(".object-menu-wrapper .menu-link").click(function () {
    // UPDATE BY DENIS
    //$("#object_content_section").animate({height: 'hide'}, 'fast');

    if ($(".object-menu").is(':visible'))
    {
      $(".object-menu").animate({width: 'hide'}, 'fast');
      $(".object-menu-wrapper .menu-link").animate({left: "0"}, 'fast');
    } else
    {
      $(".object-menu").animate({width: 'show'}, 'fast');
      $(".object-menu-wrapper .menu-link").animate({left: "0"}, 'fast');
    }
  });


  //Click on "Content Details Panel"
  $("#object-wrapper").on('click', ' h1', function () {

    if ($("#object_content_section").is(':visible'))
    {
      $("#object_content_section").animate({height: 'hide'}, 'fast');
    } else
    {
      $("#tabs").removeClass('ui-corner-all');
      $("#object_content_section").animate({height: 'show'}, {duration: 100, complete: function () {

          $("#tabs").addClass('ui-corner-all');
        }
      });
    }

    // Give class of active so we can change chevron icon when down
    $(this).toggleClass("active");

    //If "MENU" tab not hidden, close it and slide the menu label to the left
    if ($(".object-menu").is(':visible'))
    {
      $(".object-menu").animate({width: 'hide'}, 'fast');
      $(".object-menu-wrapper .menu-link").animate({left: "0"}, 'fast');
    }
  });

// Checks to see if a matching camera exists and returns the default camera if a matching one is not found
// Arg1: camera ID
// Arg2: object that contains all the camera locations
  function returnApporiateCameraId(cameraId, camLocObj) {
    if (camLocObj[cameraId] == null) {
      cameraId = 'DefaultView';
    }

    return camLocObj[cameraId];
  }

  $(document).ready(function () {

    $('.moveLink').click(function (e) {
      e.preventDefault();
      var id = $(this).attr('data-attr');

      var destination;

      if (webgl)
      {
        destination = returnApporiateCameraId(id, webglCamLoc);
      } else
      {
        destination = returnApporiateCameraId(id, canvasCamLoc);
      }

      if (!$(this).hasClass('selected')) {
        if (destination !== undefined) {
          currentAnimation = id;

          makeObjectsInvisible();
          removePopupContent();
          closeMenu();
          $('#popup_label_scene').hide();
          resetTextureLock();
          resetOpacity();
          resetHighlight();
          toggleObjectGroup(id, true);

          $(this).parents().find('.selected').removeClass('selected');
          $(this).addClass('selected');
          $('.' + id + '_content');
          TWEEN.removeAll();

          if (destination.group !== currentCamLoc.group) {
            // hide things when you click on reset view bouton.
            $('#page-subtitle-object-alteration').hide(); // hide the title of the node
            $('#see_report_in_title_btn').hide(); // hide the button : see reports
            $('#container_related_development_types').hide(); // hide the related dev types section

            tween1 = new TWEEN.Tween(currentCamLoc)
                    .to({posX: 0, posY: 500, posZ: 100, lookX: 0, lookY: 0, lookZ: 0}, 500)
                    .easing(TWEEN.Easing.Circular.InOut)
                    .onUpdate(function () {
                      camera.position.set(currentCamLoc.posX, currentCamLoc.posY, currentCamLoc.posZ);
                      camera.lookAt({x: currentCamLoc.lookX, y: currentCamLoc.lookY, z: currentCamLoc.lookZ});
                    });
            tweenWindow = new TWEEN.Tween(currentCamLoc)
                    .to({posX: 35, posY: 18, posZ: 98.3, lookX: 35, lookY: 17.5, lookZ: 69.6}, 500)
                    .easing(TWEEN.Easing.Circular.InOut)
                    .onUpdate(function () {
                      camera.position.set(currentCamLoc.posX, currentCamLoc.posY, currentCamLoc.posZ);
                      camera.lookAt({x: currentCamLoc.lookX, y: currentCamLoc.lookY, z: currentCamLoc.lookZ});
                    });
          }

          tween2 = new TWEEN.Tween(currentCamLoc)
                  .to({posX: destination.posX, posY: destination.posY, posZ: destination.posZ, lookX: destination.lookX, lookY: destination.lookY, lookZ: destination.lookZ}, 750)
                  .easing(TWEEN.Easing.Circular.InOut)
                  .onUpdate(function () {
                    camera.position.set(currentCamLoc.posX, currentCamLoc.posY, currentCamLoc.posZ);
                    camera.lookAt({x: currentCamLoc.lookX, y: currentCamLoc.lookY, z: currentCamLoc.lookZ});
                  })
                  .onComplete(function () {
                    if (id == currentAnimation) {
                      if (id !== "ResetView") {
                        $('#object_content_section').show();
                        highlightObjectGroup(id, "highlight");
                        toggleTextureLock(id, true);
                        isInsideHouse(id);
                        ga("send", "pageview", {page: findTabUrl("requirements")});
                      } else
                      {

                        removePopupContent();
                        $('.view-alteration-object .view-content').find('.active').removeClass('active');
                        ga("send", "event", "resetview", "button_click", this.href);

                        if (id != null) {
                          houseInternal = false;
                        } else {
                          houseInternal = true;
                        }
                      }
                      currentAnimation = "";
                      currentFocus = id.charAt(0).toLowerCase() + id.slice(1);
                    }
                  });


          if (destination.group !== currentCamLoc.group)
          {
            if (destination.group == 'F')
            {
              if (currentCamLoc.group == 'A')
              {
                tweenWindow.chain(tween2);
                tweenWindow.start();
              } else
              {
                tween1.chain(tweenWindow);
                tweenWindow.chain(tween2);
                tween1.start();
              }
            } else if (currentCamLoc.group == 'F')
            {

              if (destination.group == 'A')
              {
                tweenWindow.chain(tween2);
                tweenWindow.start();
              } else
              {
                tweenWindow.chain(tween1);
                tween1.chain(tween2);
                tweenWindow.start();
              }
            } else
            {
              tween1.chain(tween2);
              tween1.start();
            }
          } else
          {
            tween2.start();
          }

          currentCamLoc.group = destination.group;
        }
      } else
      {
        closeMenu();
        $('.view-alteration-object .view-content').find('.active').removeClass('active');
      }
    });

    $('.view-content h3').click(function (e) {
      e.preventDefault();

      //Active class is for showing the sub menu
      $('ul').not($(this).next('ul')).removeClass('active');
      $(this).next('ul').toggleClass('active');

      var id = $(this).children().not('.arrow').attr('id');
      var destination;

      if (webgl)
      {
        destination = webglCamLoc[id];
      } else
      {
        destination = canvasCamLoc[id];
      }

      //If this is not selected and not active then animate
      if (!$(this).hasClass('selected') && $(this).hasClass('active')) {
        if (destination !== undefined) {
          currentAnimation = id;

          makeObjectsInvisible();
          removePopupContent();
          $('#popup_label_scene').hide();
          resetTextureLock();
          resetHighlight();
          resetOpacity();
          ghostMenuObjectGroup(id);

          //Selected class tells us that the camera is currently focusing on this item
          $(this).parents().find('.selected').removeClass('selected');
          $(this).addClass('selected');
          TWEEN.removeAll();

          if (destination.group !== currentCamLoc.group) {
            tween1 = new TWEEN.Tween(currentCamLoc)
                    .to({posX: 0, posY: 500, posZ: 100, lookX: 0, lookY: 0, lookZ: 0}, 500)
                    .easing(TWEEN.Easing.Circular.InOut)
                    .onUpdate(function () {
                      camera.position.set(currentCamLoc.posX, currentCamLoc.posY, currentCamLoc.posZ);
                      camera.lookAt({x: currentCamLoc.lookX, y: currentCamLoc.lookY, z: currentCamLoc.lookZ});
                    });
            tweenWindow = new TWEEN.Tween(currentCamLoc)
                    .to({posX: 180, posY: 15, posZ: -60, lookX: 0, lookY: 15, lookZ: -60}, 500)
                    .easing(TWEEN.Easing.Circular.InOut)
                    .onUpdate(function () {
                      camera.position.set(currentCamLoc.posX, currentCamLoc.posY, currentCamLoc.posZ);
                      camera.lookAt({x: currentCamLoc.lookX, y: currentCamLoc.lookY, z: currentCamLoc.lookZ});
                    });
          }

          tween2 = new TWEEN.Tween(currentCamLoc)
                  .to({posX: destination.posX, posY: destination.posY, posZ: destination.posZ, lookX: destination.lookX, lookY: destination.lookY, lookZ: destination.lookZ}, 750)
                  .easing(TWEEN.Easing.Circular.InOut)
                  .onUpdate(function () {
                    camera.position.set(currentCamLoc.posX, currentCamLoc.posY, currentCamLoc.posZ);
                    camera.lookAt({x: currentCamLoc.lookX, y: currentCamLoc.lookY, z: currentCamLoc.lookZ});
                  })
                  .onComplete(function () {
                    if (id == currentAnimation) {
                      highlightObjectGroup(id, "highlight");
                      toggleTextureLock(id, true);
                      ga("send", "pageview", {page: findTabUrl("requirements")});
                      currentAnimation = "";
                      currentFocus = id.charAt(0).toLowerCase() + id.slice(1);

                      if (id != null) {
                        houseInternal = false;
                      } else {
                        houseInternal = true;
                      }
                    }
                  });

          if (destination.group !== currentCamLoc.group)
          {
            if (destination.group == 'F')
            {
              if (currentCamLoc.group == 'A')
              {
                tweenWindow.chain(tween2);
                tweenWindow.start();
              } else
              {
                tween1.chain(tweenWindow);
                tweenWindow.chain(tween2);
                tween1.start();
              }
            } else if (currentCamLoc.group == 'F')
            {
              if (destination.group == 'A')
              {
                tweenWindow.chain(tween2);
                tweenWindow.start();
              } else
              {
                tweenWindow.chain(tween1);
                tween1.chain(tween2);
                tweenWindow.start();
              }
            } else
            {
              tween1.chain(tween2);
              tween1.start();
            }
          } else
          {
            tween2.start();
          }

          currentCamLoc.group = destination.group;
        }
      }
    });


    /********* DEBUG CODE START ***********/
    /*   $('.middle_content').append('<span id="cameraTest"><style>#cameraTest input{width:60px;} #cameraTest{display:block; text-align: center;position:absolute; margin-top: 200px;width:100%;}</style><span>posX<input type="text" name="posX" value="0">posY<input type="text" name="posY" value="0">posZ<input type="text" name="posZ" value="0">lookX<input type="text" name="lookX" value="0">lookY<input type="text" name="lookY" value="0">lookZ<input type="text" name="lookZ" value="0"><button id="moveCam">update</button></span></span>');
     $('#moveCam').click(function(e){
     e.preventDefault();
     updateCam(500);
     });
     var increment = 1;
     function updateCam(time){
     tweenMove = new TWEEN.Tween(currentCamLoc)
     .to({posX: parseInt($('input[name="posX"]').val()), posY: parseInt($('input[name="posY"]').val()), posZ: parseInt($('input[name="posZ"]').val()), lookX: parseInt($('input[name="lookX"]').val()), lookY: parseInt($('input[name="lookY"]').val()), lookZ: parseInt($('input[name="lookZ"]').val())}, time)
     .easing(TWEEN.Easing.Circular.InOut)
     .onUpdate(function(){
     camera.position.set(currentCamLoc.posX,currentCamLoc.posY,currentCamLoc.posZ);
     camera.lookAt( {x:currentCamLoc.lookX, y:currentCamLoc.lookY, z:currentCamLoc.lookZ } );
     })
     tweenMove.start();
     }
     $('#cameraTest input').keydown(function(e){
     if(e.which == 13){
     $('#moveCam').click();
     }else if(e.which == 38){
     $(this).val( parseInt($(this).val()) + increment ) ;
     updateCam(1);
     }else if(e.which == 40){
     $(this).val( parseInt($(this).val()) - increment ) ;
     updateCam(1);
     }
     $(window).keydown(function(e) {
     if(e.which == 16){
     increment = 10;
     }
     });
     $(window).keyup(function(e) {
     if(e.which == 16){
     increment = 1;
     }
     });
     });
     if(webgl)
     {
     $('input[name="posX"]').val(webglCamLoc['ResetView'].posX);
     $('input[name="posY"]').val(webglCamLoc['ResetView'].posY);
     $('input[name="posZ"]').val(webglCamLoc['ResetView'].posZ);
     $('input[name="lookX"]').val(webglCamLoc['ResetView'].lookX);
     $('input[name="lookY"]').val(webglCamLoc['ResetView'].lookY);
     $('input[name="lookZ"]').val(webglCamLoc['ResetView'].lookZ);
     }
     else
     {
     $('input[name="posX"]').val(canvasCamLoc['ResetView'].posX);
     $('input[name="posY"]').val(canvasCamLoc['ResetView'].posY);
     $('input[name="posZ"]').val(canvasCamLoc['ResetView'].posZ);
     $('input[name="lookX"]').val(canvasCamLoc['ResetView'].lookX);
     $('input[name="lookY"]').val(canvasCamLoc['ResetView'].lookY);
     $('input[name="lookZ"]').val(canvasCamLoc['ResetView'].lookZ);
     }
     $('.moveLink').click(function(e){
     e.preventDefault();
     var id = $(this).attr('data-attr');
     if(webgl)
     {
     $('input[name="posX"]').val(webglCamLoc[id].posX);
     $('input[name="posY"]').val(webglCamLoc[id].posY);
     $('input[name="posZ"]').val(webglCamLoc[id].posZ);
     $('input[name="lookX"]').val(webglCamLoc[id].lookX);
     $('input[name="lookY"]').val(webglCamLoc[id].lookY);
     $('input[name="lookZ"]').val(webglCamLoc[id].lookZ);
     }
     else
     {
     $('input[name="posX"]').val(canvasCamLoc[id].posX);
     $('input[name="posY"]').val(canvasCamLoc[id].posY);
     $('input[name="posZ"]').val(canvasCamLoc[id].posZ);
     $('input[name="lookX"]').val(canvasCamLoc[id].lookX);
     $('input[name="lookY"]').val(canvasCamLoc[id].lookY);
     $('input[name="lookZ"]').val(canvasCamLoc[id].lookZ);
     }
     });    */
    /********* DEBUG CODE END ***********/

  });


})(jQuery);
