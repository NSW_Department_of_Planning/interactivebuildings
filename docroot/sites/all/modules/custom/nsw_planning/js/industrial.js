
jq1102(function ($) {

  window.onload = function ()
  {
    threeStart();
    window.addEventListener('resize', resize3dArea, false);
    resize3dArea();
  };


  var width;
  var marginLeft;   //marginLeft is the number that shows the distance from the Menu label to the left margin of canvas
  function resize3dArea()
  {
    camera.aspect = 16 / 9;
    camera.updateProjectionMatrix();
    width = $('#canvas_container').width();

    if (width > 960)
    {
      width = 960;
    }
    renderer.setSize(width, width / (16 / 9));

    marginLeft = 310; //parseInt($('.view-alteration-object .view-footer').css('marginLeft').replace('px', '')) + parseInt($('.view-alteration-object .view-content').css('width').replace('px', '')) + parseInt($('.view-alteration-object .view-content').css("left").replace('px', ''));

    //popup_label_scene is the section which overlays on the 3D canvas, and it has the same width and height as the canvas, but it is invisible
    $('#popup_label_scene').css({"width": width, "height": width / (16 / 9)});
    var popup_marginLeft = -(width / 2);
    $('#popup_label_scene').css({"margin-left": popup_marginLeft});
  }

  /*
   Collection of camera positions.
   Camera position names should match the ID on the equivalent navigation link.

   posX, posY, posZ = Position of the camera.
   lookX, lookY, lookZ = Where the camera looks at.
   group = Moving between camera positions of the same group will not trigger the flyover effect. The flyover is needed to avoid clipping through the house and other objects.
   */
  var currentCamLoc = {posX: -300, posY: 190, posZ: 590, lookX: -80, lookY: 60, lookZ: 220, group: 'A'};
  var webglCamLoc = {
    ResetView: {posX: -300, posY: 190, posZ: 590, lookX: -80, lookY: 60, lookZ: 220, group: 'A'},
    DefaultView: {posX: -300, posY: 190, posZ: 590, lookX: -80, lookY: 60, lookZ: 220, group: 'A'},
    IND_AccessRamps: {posX: 40, posY: 25, posZ: 170, lookX: -18, lookY: 0, lookZ: 100, group: 'A'},
    IND_AerialsAntennaeRoof: {posX: -240, posY: 90, posZ: 190, lookX: -150, lookY: 50, lookZ: 70, group: 'A'},
    IND_Bollards: {posX: 45, posY: 25, posZ: 170, lookX: -15, lookY: 0, lookZ: 110, group: 'A'},
    IND_Blinds: {posX: -220, posY: 20, posZ: 170, lookX: -190, lookY: 15, lookZ: 135, group: 'A'},
    IND_BuildingIdentificationSigns: {posX: -40, posY: 70, posZ: 170, lookX: -5, lookY: 55, lookZ: 110, group: 'A'},
    IND_ChangeOfUse: {posX: -195, posY: 20, posZ: 165, lookX: -145, lookY: 11, lookZ: 85, group: 'A'},
    IND_CommunityNotice: {posX: -300, posY: 190, posZ: 590, lookX: -80, lookY: 60, lookZ: 220, group: 'A'},
    IND_Demolition: {posX: -300, posY: 190, posZ: 590, lookX: -80, lookY: 60, lookZ: 220, group: 'A'},
    IND_DoorsWindows: {posX: -300, posY: 190, posZ: 590, lookX: -80, lookY: 60, lookZ: 220, group: 'A'},
    IND_Driveways: {posX: -300, posY: 190, posZ: 590, lookX: -80, lookY: 60, lookZ: 220, group: 'A'},
    IND_FrontPaving: {posX: -227, posY: 68, posZ: 245, lookX: -150, lookY: 19, lookZ: 157, group: 'A'},
    IND_Hardstanding: {posX: -300, posY: 190, posZ: 590, lookX: -80, lookY: 60, lookZ: 220, group: 'A'},
    IND_ElectionSigns: {posX: -240, posY: 16, posZ: 360, lookX: -221, lookY: 5, lookZ: 320, group: 'A'},
    IND_EmergencyWork: {posX: -300, posY: 190, posZ: 590, lookX: -80, lookY: 60, lookZ: 220, group: 'A'},
    IND_FencesAdjacentRoad: {posX: -290, posY: 22, posZ: 455, lookX: -53, lookY: -75, lookZ: 138, group: 'A'},
    IND_FencesIndustrial: {posX: -400, posY: 200, posZ: 500, lookX: -31, lookY: -144, lookZ: 110, group: 'A'},
    IND_Letterboxes: {posX: -170, posY: 25, posZ: 165, lookX: -145, lookY: 8, lookZ: 120, group: 'A'},
    IND_MaintenanceBuildingsDraftHeritage: {posX: -300, posY: 190, posZ: 590, lookX: -80, lookY: 60, lookZ: 220, group: 'A'},
    IND_MinorExternalBuildingAlterations: {posX: -230, posY: 50, posZ: 195, lookX: -195, lookY: 30, lookZ: 160, group: 'A'},
    IND_PathwaysPaving: {posX: -225, posY: 65, posZ: 270, lookX: -155, lookY: 25, lookZ: 190, group: 'A'},
    IND_RealEstateSigns: {posX: -200, posY: 28, posZ: 380, lookX: -170, lookY: 10, lookZ: 326, group: 'A'},
    IND_ReplacementIdentificationSigns: {posX: -300, posY: 190, posZ: 590, lookX: -80, lookY: 60, lookZ: 220, group: 'A'},
    IND_Scaffolding: {posX: -250, posY: 25, posZ: 155, lookX: -205, lookY: 3, lookZ: 100, group: 'A'},
    IND_SecurityScreens: {posX: -220, posY: 20, posZ: 170, lookX: -190, lookY: 15, lookZ: 135, group: 'A'},
    IND_SiteFencesHoardings: {posX: -260, posY: 25, posZ: 150, lookX: -200, lookY: -10, lookZ: 80, group: 'A'},
    IND_SolarEnergySystems: {posX: 140, posY: 180, posZ: 180, lookX: 50, lookY: 90, lookZ: 90, group: 'A'},
    IND_TemporaryBuildersStructures: {posX: -180, posY: 30, posZ: 290, lookX: -240, lookY: 4, lookZ: 215, group: 'A'},
    IND_TemporaryEventSigns: {posX: -300, posY: 190, posZ: 590, lookX: -80, lookY: 60, lookZ: 220, group: 'A'},
    IND_WallSigns: {posX: -60, posY: 25, posZ: 130, lookX: -35, lookY: 15, lookZ: 90, group: 'A'},
    IND_AerialsAntennaeGround: {posX: 264, posY: 75, posZ: 156, lookX: 221, lookY: 16, lookZ: 85, group: 'B'},
    IND_AirConditioningUnitsWall: {posX: 255, posY: 25, posZ: 140, lookX: 210, lookY: 22, lookZ: 91, group: 'B'},
    IND_AirConditioningUnitsGround: {posX: 240, posY: 20, posZ: 135, lookX: 200, lookY: 5, lookZ: 90, group: 'B'},
    IND_HotWaterSystems: {posX: 260, posY: 20, posZ: 160, lookX: 190, lookY: 3, lookZ: 95, group: 'B'},
    IND_RainwaterTanks: {posX: 280, posY: 35, posZ: 170, lookX: 220, lookY: 10, lookZ: 110, group: 'B'},
    IND_MIBA_Bathroom: {posX: -110, posY: 15, posZ: 35, lookX: -130, lookY: 6, lookZ: 12, group: 'F'},
    IND_MIBA_Kitchen: {posX: -127, posY: 15, posZ: 35, lookX: -88, lookY: 6, lookZ: 18, group: 'F'},
    IND_MIBA_Office: {posX: -166, posY: 16, posZ: 103, lookX: -113, lookY: 5, lookZ: 85, group: 'F'},
    IND_MIBA_Stairs: {posX: -118, posY: 16, posZ: 30, lookX: -140, lookY: 8, lookZ: 25, group: 'F'},
    IND_InternalSigns: {posX: -160, posY: 15, posZ: 90, lookX: -90, lookY: 8, lookZ: -30, group: 'F'},
    IND_MinorInternalBuildingAlterations: {posX: -190, posY: 15, posZ: 105, lookX: -110, lookY: 8, lookZ: 10, group: 'F'},
    BuildingAlterationsExternalMenu: {posX: 230, posY: 75, posZ: 300, lookX: 70, lookY: 0, lookZ: 115, group: 'A'},
    OutdoorItemsMenu: {posX: 275, posY: 65, posZ: 275, lookX: 150, lookY: 0, lookZ: 105, group: 'A'},
    FencesWallsMenu: {posX: -400, posY: 200, posZ: 500, lookX: -31, lookY: -70, lookZ: 110, group: 'A'},
    RoofMenu: {posX: -90, posY: 180, posZ: 280, lookX: -55, lookY: 50, lookZ: 125, group: 'A'},
    SignsMenu: {posX: -230, posY: 40, posZ: 400, lookX: -210, lookY: 22, lookZ: 350, group: 'A'},
    MaintenanceMenu: {posX: -230, posY: 70, posZ: 330, lookX: -200, lookY: 0, lookZ: 110, group: 'A'},
  };

  var canvasCamLoc = {
    ResetView: {posX: -300, posY: 190, posZ: 590, lookX: -80, lookY: 60, lookZ: 220, group: 'A'},
    DefaultView: {posX: -300, posY: 190, posZ: 590, lookX: -80, lookY: 60, lookZ: 220, group: 'A'},
    IND_AccessRamps: {posX: -27, posY: 25, posZ: 160, lookX: -27, lookY: 0, lookZ: 100, group: 'A'},
    IND_AerialsAntennaeRoof: {posX: -220, posY: 90, posZ: 170, lookX: -150, lookY: 50, lookZ: 70, group: 'A'},
    IND_Blinds: {posX: -220, posY: 20, posZ: 180, lookX: -190, lookY: 15, lookZ: 135, group: 'A'},
    IND_Bollards: {posX: 25, posY: 35, posZ: 180, lookX: -15, lookY: 0, lookZ: 110, group: 'A'},
    IND_BuildingIdentificationSigns: {posX: 0, posY: 60, posZ: 180, lookX: 0, lookY: 55, lookZ: 110, group: 'A'},
    IND_ChangeOfUse: {posX: -166, posY: 16, posZ: 165, lookX: -166, lookY: 11, lookZ: 86, group: 'A'},
    IND_CommunityNotice: {posX: -300, posY: 190, posZ: 590, lookX: -80, lookY: 60, lookZ: 220, group: 'A'},
    IND_Demolition: {posX: -300, posY: 190, posZ: 590, lookX: -80, lookY: 60, lookZ: 220, group: 'A'},
    IND_DoorsWindows: {posX: -300, posY: 190, posZ: 590, lookX: -80, lookY: 60, lookZ: 220, group: 'A'},
    IND_Driveways: {posX: -300, posY: 190, posZ: 590, lookX: -80, lookY: 60, lookZ: 220, group: 'A'},
    IND_FrontPaving: {posX: -195, posY: 68, posZ: 245, lookX: -150, lookY: 17, lookZ: 157, group: 'A'},
    IND_Hardstanding: {posX: -300, posY: 190, posZ: 590, lookX: -80, lookY: 60, lookZ: 220, group: 'A'},
    IND_ElectionSigns: {posX: -220, posY: 16, posZ: 360, lookX: -220, lookY: 5, lookZ: 320, group: 'A'},
    IND_EmergencyWork: {posX: -300, posY: 190, posZ: 590, lookX: -80, lookY: 60, lookZ: 220, group: 'A'},
    IND_FencesAdjacentRoad: {posX: -290, posY: 22, posZ: 455, lookX: -53, lookY: -75, lookZ: 138, group: 'A'},
    IND_FencesIndustrial: {posX: -380, posY: 200, posZ: 480, lookX: -31, lookY: -144, lookZ: 110, group: 'A'},
    IND_Letterboxes: {posX: -145, posY: 15, posZ: 165, lookX: -145, lookY: 8, lookZ: 120, group: 'A'},
    IND_MaintenanceBuildingsDraftHeritage: {posX: -300, posY: 190, posZ: 590, lookX: -80, lookY: 60, lookZ: 220, group: 'A'},
    IND_MinorExternalBuildingAlterations: {posX: -165, posY: 18, posZ: 210, lookX: -160, lookY: 8, lookZ: 110, group: 'A'},
    IND_PathwaysPaving: {posX: -200, posY: 70, posZ: 265, lookX: -155, lookY: 25, lookZ: 190, group: 'A'},
    IND_RealEstateSigns: {posX: -170, posY: 12, posZ: 370, lookX: -170, lookY: 10, lookZ: 320, group: 'A'},
    IND_ReplacementIdentificationSigns: {posX: -300, posY: 190, posZ: 590, lookX: -80, lookY: 60, lookZ: 220, group: 'A'},
    IND_Scaffolding: {posX: -255, posY: 20, posZ: 150, lookX: -205, lookY: 3, lookZ: 100, group: 'A'},
    IND_SecurityScreens: {posX: -220, posY: 20, posZ: 170, lookX: -190, lookY: 15, lookZ: 135, group: 'A'},
    IND_SiteFencesHoardings: {posX: -275, posY: 25, posZ: 150, lookX: -195, lookY: -10, lookZ: 80, group: 'A'},
    IND_SolarEnergySystems: {posX: 50, posY: 235, posZ: 100, lookX: 30, lookY: 90, lookZ: 100, group: 'A'},
    IND_TemporaryBuildersStructures: {posX: -180, posY: 20, posZ: 200, lookX: -225, lookY: 15, lookZ: 200, group: 'A'},
    IND_TemporaryEventSigns: {posX: -300, posY: 190, posZ: 590, lookX: -80, lookY: 60, lookZ: 220, group: 'A'},
    IND_WallSigns: {posX: -40, posY: 15, posZ: 120, lookX: -38, lookY: 15, lookZ: 70, group: 'A'},
    IND_AerialsAntennaeGround: {posX: 264, posY: 75, posZ: 145, lookX: 221, lookY: 16, lookZ: 85, group: 'B'},
    IND_AirConditioningUnitsWall: {posX: 255, posY: 25, posZ: 140, lookX: 210, lookY: 22, lookZ: 91, group: 'B'},
    IND_AirConditioningUnitsGround: {posX: 240, posY: 20, posZ: 135, lookX: 200, lookY: 5, lookZ: 90, group: 'B'},
    IND_HotWaterSystems: {posX: 255, posY: 27, posZ: 151, lookX: 185, lookY: -8, lookZ: 92, group: 'B'},
    IND_RainwaterTanks: {posX: 240, posY: 40, posZ: 190, lookX: 220, lookY: 10, lookZ: 110, group: 'B'},
    IND_MIBA_Bathroom: {posX: -138, posY: 10, posZ: 43, lookX: -140, lookY: 6, lookZ: 10, group: 'F'},
    IND_MIBA_Kitchen: {posX: -127, posY: 15, posZ: 35, lookX: -88, lookY: 6, lookZ: 18, group: 'F'},
    IND_MIBA_Office: {posX: -142, posY: 29, posZ: 97, lookX: -127, lookY: 12, lookZ: 96, group: 'F'},
    IND_MIBA_Stairs: {posX: -114, posY: 16, posZ: 24, lookX: -145, lookY: 8, lookZ: 26, group: 'F'},
    IND_InternalSigns: {posX: -140, posY: 10, posZ: 88, lookX: -140, lookY: 8, lookZ: -10, group: 'F'},
    IND_MinorInternalBuildingAlterations: {posX: -140, posY: 10, posZ: 88, lookX: -140, lookY: 8, lookZ: -10, group: 'F'},
    BuildingAlterationsExternalMenu: {posX: 230, posY: 75, posZ: 300, lookX: 70, lookY: 0, lookZ: 115, group: 'A'},
    OutdoorItemsMenu: {posX: 275, posY: 65, posZ: 275, lookX: 150, lookY: 0, lookZ: 105, group: 'A'},
    FencesWallsMenu: {posX: -380, posY: 200, posZ: 480, lookX: -31, lookY: -70, lookZ: 110, group: 'A'},
    RoofMenu: {posX: -90, posY: 180, posZ: 280, lookX: -55, lookY: 50, lookZ: 125, group: 'A'},
    SignsMenu: {posX: -230, posY: 40, posZ: 400, lookX: -210, lookY: 22, lookZ: 350, group: 'A'},
    MaintenanceMenu: {posX: -230, posY: 70, posZ: 330, lookX: -200, lookY: 0, lookZ: 110, group: 'A'},
  };

  var objInfo = {
    IND_AccessRamps: {menu: "BuildingAlterationsExternalMenu", ghost: true, highlightable: true},
    IND_Blinds: {menu: "BuildingAlterationsExternalMenu", ghost: true, highlightable: true},
    IND_AirConditioningUnitsWall: {menu: "BuildingAlterationsExternalMenu", ghost: true, highlightable: true},
    IND_HotWaterSystems: {menu: "BuildingAlterationsExternalMenu", ghost: true, highlightable: true},
    IND_SecurityScreens: {menu: "BuildingAlterationsExternalMenu", ghost: true, highlightable: true},
    IND_MinorExternalBuildingAlterations: {menu: "BuildingAlterationsExternalMenu", ghost: false, highlightable: true},
    IND_MinorInternalBuildingAlterations: {menu: "BuildingAlterationsInternalMenu", ghost: false, highlightable: true},
    IND_InternalSigns: {menu: "BuildingAlterationsInternalMenu", ghost: false, highlightable: true},
    IND_BathroomAlterations: {menu: "BuildingAlterationsInternalMenu", ghost: false, highlightable: true},
    IND_OfficeAlterations: {menu: "BuildingAlterationsInternalMenu", ghost: false, highlightable: true},
    IND_Kitchen: {menu: "BuildingAlterationsInternalMenu", ghost: false, highlightable: false},
    IND_KitchenAlterations: {menu: "BuildingAlterationsInternalMenu", ghost: false, highlightable: true},
    IND_Room: {menu: "BuildingAlterationsInternalMenu", ghost: false, highlightable: false},
    IND_Hall: {menu: "BuildingAlterationsInternalMenu", ghost: false, highlightable: false},
    IND_InternalDoor: {menu: "BuildingAlterationsInternalMenu", ghost: false, highlightable: false},
    IND_InternalShelving: {menu: "BuildingAlterationsInternalMenu", ghost: false, highlightable: false},
    IND_InternalStair: {menu: "BuildingAlterationsInternalMenu", ghost: false, highlightable: true},
    IND_FencesIndustrial: {menu: "FencesWallsMenu", ghost: true, highlightable: true},
    IND_FencesAdjacentRoad: {menu: "FencesWallsMenu", ghost: true, highlightable: true},
    IND_AirConditioningUnitsGround: {menu: "OutdoorItemsMenu", ghost: true, highlightable: true},
    IND_Bollards: {menu: "OutdoorItemsMenu", ghost: true, highlightable: true},
    IND_RainwaterTanks: {menu: "OutdoorItemsMenu", ghost: true, highlightable: true},
    IND_AerialsAntennaeGround: {menu: "OutdoorItemsMenu", ghost: true, highlightable: true},
    IND_Letterboxes: {menu: "OutdoorItemsMenu", ghost: true, highlightable: true},
    IND_FrontPaving: {menu: "OutdoorItemsMenu", ghost: true, highlightable: true},
    IND_Driveways: {menu: "OutdoorItemsMenu", ghost: true, highlightable: true},
    IND_Hardstanding: {menu: "OutdoorItemsMenu", ghost: true, highlightable: true},
    IND_PathwaysPaving: {menu: "OutdoorItemsMenu", ghost: true, highlightable: true},
    IND_SolarEnergySystems: {menu: "RoofMenu", ghost: true, highlightable: true},
    IND_AerialsAntennaeRoof: {menu: "RoofMenu", ghost: true, highlightable: true},
    IND_BuildingIdentificationSigns: {menu: "SignsMenu", ghost: true, highlightable: true},
    IND_WallSigns: {menu: "SignsMenu", ghost: true, highlightable: true},
    IND_RealEstateSigns: {menu: "SignsMenu", ghost: true, highlightable: true},
    IND_ElectionSigns: {menu: "SignsMenu", ghost: true, highlightable: true},
    IND_InternalSigns: {menu: "SignsMenu", ghost: true, highlightable: true},
    IND_Scaffolding: {menu: "MaintenanceMenu", ghost: true, highlightable: true},
    IND_SiteFencesHoardings: {menu: "MaintenanceMenu", ghost: true, highlightable: true},
    IND_TemporaryBuildersStructures: {menu: "MaintenanceMenu", ghost: true, highlightable: true},
    IND_ChangeOfUse: {menu: "ChangeOfUseMenu", ghost: true, highlightable: true},
  };


  var scene, renderer, camera, light1, light2, light3, light4, loader, projector, raycaster;
  var mouse = new THREE.Vector2(), INTERSECTED;

  var obj, vector, intersects;
  var currentAnimation = "";
  var currentFocus = "default";
  var numObjects = 43;
  var loaded = 0;
  var objArray = [];
  var toggleableArray = [];
  var unToggleableArray = [];
  var objectColour = {
    body: "0x99a1a3",
    outline: "0x4D4D4D",
    highlight: "0x009be4",
    hover: "0x4D4D4D",
    grass: "0x80BB80"
  };
  var webgl = false;
  var canvas = null;
  var offsetX, offsetY;
  var renderDepth = 0;
  var hotspotObj = null;
  var tabRequireOpen = true;
  var tabRestrictOpen = false;
  var houseInternal = false;


  function threeStart()
  {
    initScene();
    initCamera();
    initSky();
    initLight();
    initObjects();
    detectUserDevice();
    render();
  }


  function initScene()
  {
    scene = new THREE.Scene();
    canvas = document.createElement('canvas');
    canvas.id = "CanvasID";
    if (Detector.webgl)
    {
      renderer = new THREE.WebGLRenderer({canvas: canvas, antialias: true});
      webgl = true;
    } else
    {
      renderer = new THREE.CanvasRenderer({canvas: canvas});
      webgl = false;
    }
    renderer.setSize(window.innerWidth, window.innerHeight);
    renderer.setClearColor(0xFFFFFF, 1, 0);
    document.getElementById("canvas_container").appendChild(renderer.domElement);
    $("#canvas_container").css({"-webkit-tap-highlight-color": "rgba(0, 0, 0, 0)", "-webkit-touch-callout": "none"});
  }


  function initCamera()
  {
    camera = new THREE.PerspectiveCamera(55, window.innerWidth / window.innerHeight, 1, 4500);
    camera.position.set(currentCamLoc.posX, currentCamLoc.posY, currentCamLoc.posZ);
    camera.up.x = 0;
    camera.up.y = 1;
    camera.up.z = 0;

    camera.lookAt({x: currentCamLoc.lookX, y: currentCamLoc.lookY, z: currentCamLoc.lookZ});
    scene.add(camera);
  }


  function initSky()
  {
    if (webgl)
    {
      scene.fog = new THREE.Fog(0xffffff, 1, 5000);
      var vertexShader = "varying vec3 vWorldPosition; void main() { vec4 worldPosition = modelMatrix * vec4( position, 1.0 ); vWorldPosition = worldPosition.xyz; gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 ); }";
      var fragmentShader = "uniform vec3 topColor; uniform vec3 bottomColor; uniform float offset; uniform float exponent; varying vec3 vWorldPosition; void main() { float h = normalize( vWorldPosition + offset ).y; gl_FragColor = vec4( mix( bottomColor, topColor, max( pow( h, exponent ), 0.0 ) ), 1.0 );}";
      var uniforms = {
        topColor: {type: "c", value: new THREE.Color(0x0077ff)},
        bottomColor: {type: "c", value: new THREE.Color(0xffffff)},
        offset: {type: "f", value: 1},
        exponent: {type: "f", value: 0.6}
      };
      scene.fog.color.copy(uniforms.bottomColor.value);
      var skyGeo = new THREE.SphereGeometry(1500, 32, 15);
      var skyMat = new THREE.ShaderMaterial({vertexShader: vertexShader, fragmentShader: fragmentShader, uniforms: uniforms, side: THREE.BackSide});
      var skybox = new THREE.Mesh(skyGeo, skyMat);


      var groundGeo = new THREE.PlaneGeometry(10000, 10000);
      var groundMat = new THREE.MeshPhongMaterial({ambient: 0xffffff, color: 0x80BB80, specular: 0x050505});
      var ground = new THREE.Mesh(groundGeo, groundMat);
      ground.rotation.x = -Math.PI / 2;
      ground.position.y = -1;
      scene.add(skybox);
      scene.add(ground);
    }
  }


  function initLight()
  {
    if (webgl)
    {
      light1 = new THREE.HemisphereLight(0xFFFFFF, 1, 1.4);
      light1.position.set(-343, 250, 458.76);
      light2 = new THREE.PointLight(0xFFFFFF, 0.7, 0);
      light2.position.set(633, 0, -39);
      scene.add(light1);
      scene.add(light2);
    } else
    {
      light1 = new THREE.DirectionalLight(0xFFFFFF, 1, 0);
      light1.position.set(330, 248, 110);
      light2 = new THREE.DirectionalLight(0xFFFFFF, 0.8, 0);
      light2.position.set(-232, 200, 539);
      light3 = new THREE.DirectionalLight(0xFFFFFF, 0.3, 0);
      light3.position.set(-450, 214, 132);
      scene.add(light1);
      scene.add(light2);
      scene.add(light3);
    }
  }


  function initObjects()
  {
    if (webgl)
    {
      loadObjects("IND_IndustrialRoof", "none", "RoofMenu", 0, 0, 0);
      loadObjects("IND_IndustrialWalls", "none", "BuildingAlterationsExternalMenu", 0, -0.1, 0);
      loadObjects("IND_IndustrialPaving", "none", "OutdoorItemsMenu", 0, 0, 0);
      loadObjects("IND_IndustrialDoors", "none", "BuildingAlterationsInternalMenu", 0, 0, 0);
      loadObjects("IND_AccessRamps", "none", "none", 0, 0.5, 0);
      loadObjects("IND_AerialsAntennaeRoof", "none", "none", 0, 0, 0);
      loadObjects("IND_AerialsAntennaeGround", "none", "none", 0, 0, 0);
      loadObjects("IND_AirConditioningUnitsWall", "none", "none", 0, 0, 0);
      loadObjects("IND_AirConditioningUnitsGround", "none", "none", 0, 0, 0);
      loadObjects("IND_Bollards", "none", "none", 0, 0, 0);
      loadObjects("IND_FrontPaving", "none", "none", 0, 0, 0);
      loadObjects("IND_PathwaysPaving", "none", "none", 0, 0, 0);
      loadObjects("IND_Blinds", "none", "none", 0, 0, 0);
      loadObjects("IND_ElectionSigns", "none", "none", 0, 0, 0);
      loadObjects("IND_HotWaterSystems", "none", "none", 0, 0, 0);
      loadObjects("IND_RainwaterTanks", "none", "none", 0, 0, 0);
      loadObjects("IND_RealEstateSigns", "none", "SignsMenu", 0, 0, 0);
      loadObjects("IND_SolarEnergySystems", "none", "none", 0, 0, 60);
      loadObjects("IND_TemporaryBuildersStructures", "none", "none", 0, 0, 0);
      loadObjects("IND_WallSigns", "none", "none", 0, 0, 0);
      loadObjects("IND_ChangeOfUse", "none", "none", 0, 0, 0);
      loadObjects("IND_Scaffolding", "none", "none", 0, 0, 0);
      loadObjects("IND_SiteFencesHoardings", "none", "none", 0, 0, 0);
      loadObjects("IND_ChangeOfUse", "none", "none", 0, 0, 0);
      loadObjects("IND_SecurityScreens", "none", "none", 0, 0, 0);
      loadObjects("IND_Letterboxes", "none", "none", 0, 0, 0);
      loadObjects("IND_FencesIndustrial", "none", "FencesWallsMenu", 0, 0, 0);
      loadObjects("IND_FencesAdjacentRoad", "none", "FencesWallsMenu", 0, 0, 0);
      loadObjects("IND_BuildingIdentificationSigns", "none", "none", 0, 0, 0);
      loadObjects("IND_MinorInternalBuildingAlterations", "none", "none", 0, 0, 0);
      loadObjects("IND_MinorExternalBuildingAlterations", "none", "none", 0, 0, 0);
      loadObjects("IND_InternalSigns", "none", "none", 0, 0, 0);
      loadObjects("IND_BathroomAlterations", "IND_MIBA_Bathroom", "none", 0, 0, 0);
      loadObjects("IND_Hall", "IND_MIBA_Stairs", "none", 0, 0, 0);
      loadObjects("IND_InternalDoor", "IND_MIBA_Stairs", "none", 0, 0, 0);
      loadObjects("IND_InternalShelving", "IND_MIBA_Stairs", "none", 0, 0, 0);
      loadObjects("IND_InternalStair", "IND_MIBA_Stairs", "none", 0, 0, 0);
      loadObjects("IND_KitchenAlterations", "IND_MIBA_Kitchen", "none", 0, 0, 0);
      loadObjects("IND_OfficeAlterations", "IND_MIBA_Office", "none", 0, 0, 0);
      loadObjects("IND_Room", "none", "none", 0, 0, 0);
      loadObjects("IND_IndustrialInterior", "IND_MIBA_Office", "none", 0, 0, 0);
      loadObjects("IND_IndustrialGroundGrass", "none", "none", 0, 0, 0);
      loadObjects("IND_IndustrialGroundRoad", "none", "none", 0, 0, 0);
    } else
    {
      loadObjects("IND_IndustrialRoofCanvas", "none", "RoofMenu", 0, 0, 0);
      loadObjects("IND_IndustrialWallsCanvas", "none", "BuildingAlterationsExternalMenu", 0, 0, 0);
      loadObjects("IND_IndustrialPaving", "none", "OutdoorItemsMenu", 0, 0, 0);
      loadObjects("IND_IndustrialDoors", "none", "BuildingAlterationsInternalMenu", 0, 0, 0);
      loadObjects("IND_AccessRamps", "none", "none", 0, 0.5, 0);
      loadObjects("IND_AerialsAntennaeRoof", "none", "none", 0, 0, 0);
      loadObjects("IND_AerialsAntennaeGround", "none", "none", 0, 0, 0);
      loadObjects("IND_AirConditioningUnitsWall", "none", "none", 0, 0, 0);
      loadObjects("IND_AirConditioningUnitsGround", "none", "none", 0, 0, 0);
      loadObjects("IND_Bollards", "none", "none", 0, 0, 2);
      loadObjects("IND_FrontPaving", "none", "none", 0, 0.5, 0);
      loadObjects("IND_PathwaysPaving", "none", "none", 0, 0.5, 0);
      loadObjects("IND_Blinds", "none", "none", 0, 0, 0);
      loadObjects("IND_ElectionSigns", "none", "none", 0, 0, 0);
      loadObjects("IND_HotWaterSystems", "none", "none", 0, 0, 0);
      loadObjects("IND_RainwaterTanks", "none", "none", 3, 0, 0);
      loadObjects("IND_RealEstateSigns", "none", "SignsMenu", 0, 0, 0);
      loadObjects("IND_SolarEnergySystems", "none", "none", 0, 1, 60);
      loadObjects("IND_TemporaryBuildersStructures", "none", "none", 0, 2, 0);
      loadObjects("IND_WallSigns", "none", "none", 0, 0, 0);
      loadObjects("IND_ChangeOfUse", "none", "none", 0, 0, 0);
      loadObjects("IND_Scaffolding", "none", "none", 0, 0, 0);
      loadObjects("IND_SiteFencesHoardings", "none", "none", 0, 0, 0);
      loadObjects("IND_ChangeOfUse", "none", "none", 0, 0, 0);
      loadObjects("IND_SecurityScreens", "none", "none", 0, 0, 0);
      loadObjects("IND_Letterboxes", "none", "none", 0, 0, 1);
      loadObjects("IND_FencesIndustrial", "none", "FencesWallsMenu", 0, 0, 0);
      loadObjects("IND_FencesAdjacentRoad", "none", "FencesWallsMenu", 0, 0, 0);
      loadObjects("IND_BuildingIdentificationSigns", "none", "none", 0, 0, 0);
      loadObjects("IND_MinorInternalBuildingAlterations", "none", "none", 0, 0, 0);
      loadObjects("IND_MinorExternalBuildingAlterations", "none", "none", 0, 0, 0);
      loadObjects("IND_InternalSigns", "none", "none", 0, 0, 0);
      loadObjects("IND_BathroomAlterations", "IND_MIBA_Bathroom", "none", 0, 0, 0);
      loadObjects("IND_Hall", "IND_MIBA_Stairs", "none", 0, 0, 0);
      loadObjects("IND_InternalDoor", "IND_MIBA_Stairs", "none", 0, 0, 0);
      loadObjects("IND_InternalShelving", "IND_MIBA_Stairs", "none", 0, 0, 0);
      loadObjects("IND_InternalStair", "IND_MIBA_Stairs", "none", 0, 0, 0);
      loadObjects("IND_KitchenAlterations", "IND_MIBA_Kitchen", "none", 0, 0, 0);
      loadObjects("IND_OfficeAlterations", "IND_MIBA_Office", "none", 0, 0, 0);
      loadObjects("IND_Room", "none", "none", 0, 0, 0);
      loadObjects("IND_IndustrialInterior", "IND_MIBA_Office", "none", 0, 0, 0);
      loadObjects("IND_IndustrialGroundGrass", "none", "none", 0, -6, 0);
      loadObjects("IND_IndustrialGroundRoad", "none", "none", 0, 0, 0);
    }
  }


  function loadObjects(objectName, objectGroup, hotspotGroup, x, y, z)
  {
    renderDepth += 5;
    var thisRenderDepth = renderDepth;

    loader = new THREE.JSONLoader();
    loader.load(base_path + "/json/industrial/" + objectName + ".js", function load_Obj(geometry, materials)
    {
      var material = new THREE.MeshFaceMaterial(materials);

      var obj = new THREE.Mesh(geometry, material);
      ;
      obj.name = objectName;
      obj.scale.set(1, 1, 1);
      obj.position.set(x, y, z);
      obj.group = (objectGroup == "none") ? objectName : objectGroup;
      obj.hotspot = hotspotGroup;
      obj.textureLock = false;
      obj.visible = false;
      obj.renderDepth = thisRenderDepth;


      for (var i = 0; i < obj.material.materials.length; i++) {
        if (obj.material.materials[i].name == "outline") {
          obj.material.materials[i].color.setHex(objectColour['outline']);
        } else if (obj.material.materials[i].name == "body") {
          obj.material.materials[i].color.setHex(objectColour['body']);
        } else if (obj.material.materials[i].name == "grass") {
          obj.material.materials[i].color.setHex(objectColour['grass']);
        }
        obj.material.materials[i].overdraw = 1;
        obj.material.materials[i].defaultOpacity = obj.material.materials[i].opacity;
      }

      $.each(objInfo, function (key, info) {
        if (key.toLowerCase() == objectName.toLowerCase()) {
          obj.menu = info.menu;
          obj.ghost = info.ghost;
          obj.highlightable = info.highlightable;
        }
      });

      if (obj.menu == undefined) {
        obj.menu = "none";
      }

      scene.add(obj);

      objArray.push(obj);
      loaded++;

      if (loaded == numObjects) {
        toggleableArray = objArray.slice(0);

        if (webgl)
        {
          setAlwaysVisible('IND_IndustrialRoof');
          setAlwaysVisible('IND_IndustrialWalls');
        } else
        {
          setAlwaysVisible('IND_IndustrialRoofCanvas');
          setAlwaysVisible('IND_IndustrialWallsCanvas');
        }
        setAlwaysVisible('IND_IndustrialGroundGrass');
        setAlwaysVisible('IND_IndustrialGroundRoad');
        setAlwaysVisible('IND_IndustrialDoors');
        setAlwaysVisible('IND_IndustrialPaving');
        setAlwaysVisible('IND_AccessRamps');
        setAlwaysVisible('IND_Bollards');
        setAlwaysVisible('IND_FencesIndustrial');
        setAlwaysVisible('IND_FencesAdjacentRoad');
        setAlwaysVisible('IND_RealEstateSigns');
        setAlwaysVisible('IND_Letterboxes');
        setAlwaysVisible('IND_Room');

        // BANSW0006-142: If "nid" field is present in query string then select object by node ID
        var selectedObj = getValueFromQueryString("nid");
        if (selectedObj != null && selectedObj != undefined) {
          $('.view-alteration-object').find('.moveLink[data-altobj="' + selectedObj + '"]').click().parent('li').parent('ul').addClass('active').prev('h3').addClass('active');
        }
      }

      if (obj.name == "IND_FencesIndustrial" || obj.name == "IND_IndustrialWalls" || obj.name == "IND_IndustrialWallsCanvas")
      {
        for (var i = 0; i < obj.material.materials.length; i++) {
          obj.material.materials[i].side = 2;
        }
      }
    });
  }

  projector = new THREE.Projector();
  raycaster = new THREE.Raycaster();


  function setAlwaysVisible(objectName)
  {
    toggleObject(objectName, true);
    for (var i = 0; i < toggleableArray.length; i++)
    {
      if (objectName.toLowerCase() == toggleableArray[i].name.toLowerCase())
      {
        unToggleableArray.push(toggleableArray[i]);
        toggleableArray.splice(i, 1);
      }
    }
  }


  function onDocumentMouseDown(event)
  {
    calcOffsets();

    mouse.x = ((event.clientX - offsetX) / width) * 2 - 1;
    mouse.y = -((event.clientY - offsetY) / (width / (16 / 9))) * 2 + 1;

    vector = new THREE.Vector3(mouse.x, mouse.y, 1);
    projector.unprojectVector(vector, camera);

    raycaster.set(camera.position, vector.sub(camera.position).normalize());

    intersects = raycaster.intersectObjects(scene.children);

    if (intersects.length > 0)
    {
      for (var i = 0; intersects.length > i; i++) {
        if (currentAnimation == "" && intersects[ i ].object.hotspot != "none" && intersects[i].object.hotspot != undefined) {

          //Functions triggered by hotspot start from here, open the menu and close the content panel
          $("#object_content_section").animate({height: 'hide'}, 'fast');
          $('.object-menu').animate({width: 'show'}, 'fast');
          $(".object-menu-wrapper .menu-link").animate({left: "0"}, 'fast');

          //When hotspot is clicked, the original menu should be active
          $('.view-filters').animate({width: 'show'}, 'fast');
          $('.object-menu .view-alteration-object .view-content').show();
          $('.object-menu .search-results-menu .view-content').hide();

          //If the camera angle is inside the menu, set the hotspot as "BuildingAlterationsInternalMenu"
          if (houseInternal == true)
          {
            hotspotObj = "BuildingAlterationsInternalMenu";
          } else {
            hotspotObj = intersects[ i ].object.hotspot;
            hotspotObj = hotspotObj.charAt(0).toUpperCase() + hotspotObj.slice(1);
          }

          //If correct menu is already open don't close it
          if (!$(".view-alteration-object h3" + ' #' + hotspotObj).parent().hasClass('active')) {
            $('ul').removeClass('active');
            $(".view-alteration-object h3" + ' #' + hotspotObj).parent().click();
          }

          //Google analytics tracking code
          ga("send", "event", hotspotObj, "hotspot_click", this.href);
          break;
        }
      }
    } else
    {
      INTERSECTED = null;
    }
  }


  function onDocumentMouseMove(event)
  {
    if (currentAnimation == "") {

      calcOffsets();

      mouse.x = ((event.clientX - offsetX) / width) * 2 - 1;
      mouse.y = -((event.clientY - offsetY) / (width / (16 / 9))) * 2 + 1;

      var mouseCanPosX = event.clientX - offsetX;      //Mouse position X in canvas
      var mouseCanPosY = event.clientY - offsetY;      //Mouse position Y in canvas

      vector = new THREE.Vector3(mouse.x, mouse.y, 1);
      projector.unprojectVector(vector, camera);

      raycaster.set(camera.position, vector.sub(camera.position).normalize());

      intersects = raycaster.intersectObjects(scene.children);

      if (intersects.length > 0)
      {
        for (var i = 0; intersects.length > i; i++) {
          resetHighlight();
          if (intersects[i].object.visible && intersects[i].object.hotspot != "none" && intersects[i].object.hotspot != undefined) {

            highlightHotspotGroup(intersects[i].object.hotspot, "hover");
            if (houseInternal == true)
            {
              displayHotspotLabel("BuildingAlterationsInternalMenu", mouseCanPosX, mouseCanPosY);
            } else
            {
              displayHotspotLabel(intersects[i].object.hotspot, mouseCanPosX, mouseCanPosY);
            }

            break;
          } else
          {
            $('#popup_label_scene').hide();
          }
        }
      } else
      {
        resetHighlight();
        INTERSECTED = null;
      }
    }
  }
  // hide labels when mouse leaves the 3D viewer or mouse enters one of the ui items
  $('#canvas_container').mouseleave(function () {
    $('#popup_label_scene').hide();
  });
  $(".menu-link, .object-menu, .reset-link, #object-wrapper").mouseover(function () {
    $('#popup_label_scene').hide();
  });


  function onDocumentTouchStart(event)
  {
    if (currentAnimation == "") {

      calcOffsets();

      mouse.x = ((event.touches[0].clientX - offsetX) / width) * 2 - 1;
      mouse.y = -((event.touches[0].clientY - offsetY) / (width / (16 / 9))) * 2 + 1;

      vector = new THREE.Vector3(mouse.x, mouse.y, 1);
      projector.unprojectVector(vector, camera);

      raycaster.set(camera.position, vector.sub(camera.position).normalize());

      intersects = raycaster.intersectObjects(scene.children);

      if (intersects.length > 0) {
        if (INTERSECTED != intersects[ 0 ].object)
        {

          for (var i = 0; intersects.length > i; i++) {
            resetHighlight();
            if (intersects[i].object.visible && intersects[i].object.hotspot != "none" && intersects[i].object.hotspot != undefined) {

              $("#object_content_section").animate({height: 'hide'}, 'fast');
              $('.object-menu').animate({width: 'show'}, 'fast');
              $(".object-menu-wrapper .menu-link").animate({left: "0"}, 'fast');

              //When hotspot is clicked, the original menu should be active
              $('.view-filters').animate({width: 'show'}, 'fast');
              $('.object-menu .view-alteration-object .view-content').show();
              $('.object-menu .search-results-menu .view-content').hide();

              $('ul').removeClass('active');
              hotspotObj = intersects[ i ].object.hotspot;
              hotspotObj = hotspotObj.charAt(0).toUpperCase() + hotspotObj.slice(1);

              //If correct menu is already open don't close it
              if (!$(".view-alteration-object h3" + ' #' + hotspotObj).parent().hasClass('active')) {
                $('ul').removeClass('active');
                $(".view-alteration-object h3" + ' #' + hotspotObj).parent().click();
              }

              $(".view-alteration-object h3").removeClass('active');
              $(".view-alteration-object h3" + ' #' + hotspotObj).parent().addClass('active');

              ga("send", "event", hotspotObj, "hotspot_click", this.href);
              break;
            } else
            {
              $('#popup_label_scene').hide();
            }
          }
        }
      } else
      {
        resetHighlight();
        INTERSECTED = null;
      }
    }
  }


  function detectUserDevice()
  {
    if ((Detector.webgl) || (navigator.appVersion.indexOf("MSIE 10") > 0) || (navigator.userAgent.indexOf("Safari") > 0))
    {
      if (navigator.userAgent.indexOf("iPad") < 0)
      {
        document.getElementById("CanvasID").addEventListener('mousemove', onDocumentMouseMove, false);
        document.getElementById("CanvasID").addEventListener('mousedown', onDocumentMouseDown, false);
      } else
      {
        document.getElementById("CanvasID").addEventListener('touchstart', onDocumentTouchStart, false);
      }
    } else
    {
      document.getElementById("CanvasID").addEventListener('touchstart', onDocumentTouchStart, false);
    }
  }


  var render = function () {
    requestAnimationFrame(render);
    TWEEN.update();
    renderer.render(scene, camera);
  };


  //Display the hotspot name in a popup box when mouse is hovering on the hotspot object
  function displayHotspotLabel(hotspotName, mouseCanPosX, mouseCanPosY)
  {
    var objects = findHotspotGroup(hotspotName);      //hotspotName is fencesWallsMenu, roofMenu

    for (var i = 0; i < objects.length; i++)
    {
      if (hotspotName.toLowerCase() == objects[i].hotspot.toLowerCase())
      {
        $('#popup_label_scene span').removeClass("active");
        $('#popup_label_scene').show();
        $('#popup_label_scene #popup_box').show();
        hotspotName = hotspotName.charAt(0).toUpperCase() + hotspotName.slice(1) + "_Label";
        $('#popup_label_scene #' + hotspotName).addClass("active");

        var popup_box_half_width = ($("#popup_label_scene span").width()) / 2;
        var popup_box_height = $("#popup_label_scene #popup_box").height() + 22;

        mouseCanPosX = mouseCanPosX - popup_box_half_width;
        mouseCanPosY = mouseCanPosY - popup_box_height;

        $('#popup_label_scene #popup_box').css({"top": mouseCanPosY, "left": mouseCanPosX});

      }
    }
  }


  function highlightObjectGroup(objectName, highlight)
  {
    if (objectName == "IND_MIBA_Stairs") {
      findObject('IND_Room').visible = false;
    } else
    {
      findObject('IND_Room').visible = true;
    }
    var objects = findObjectGroup(objectName);
    for (var i = 0; i < objArray.length; i++)
    {
      for (var j = 0; j < objects.length; j++)
      {
        if (objArray[i].name.toLowerCase() == objects[j].name.toLowerCase() && objArray[i].highlightable == true)
        {
          if (!objects[j].textureLock)
          {
            setObjectMaterial(objects[j], "body", highlight);
          }
        }
      }
    }
  }


  function highlightHotspotGroup(hotspotName, highlight)
  {
    var objects = findHotspotGroup(hotspotName);
    for (var i = 0; i < objects.length; i++)
    {
      if (!objects[i].textureLock)
      {
        setObjectMaterial(objects[i], "grass", highlight);
        setObjectMaterial(objects[i], "body", highlight);
      }
    }
  }


  function resetOpacity()
  {
    for (var i = 0; i < objArray.length; i++)
    {
      for (var j = 0; j < objArray[i].material.materials.length; j++)
      {
        objArray[i].material.materials[j].opacity = objArray[i].material.materials[j].defaultOpacity;
      }
    }
  }


  function resetHighlight()
  {
    for (var i = 0; i < objArray.length; i++)
    {
      if (!objArray[i].textureLock)
      {
        setObjectMaterial(objArray[i], "grass", "grass");
        setObjectMaterial(objArray[i], "body", "body");
      }
    }
  }


  function setObjectMaterial(object, material, toMaterial)
  {
    for (var i = 0; i < object.material.materials.length; i++)
    {
      if (object.material.materials[i].name == material)
      {
        object.material.materials[i].color.setHex(objectColour[toMaterial]);
      }
    }
  }


  function makeObjectsInvisible()
  {
    for (var i = 0; i < unToggleableArray.length; i++)
    {
      unToggleableArray[i].visible = true;
    }
    for (var i = 0; i < toggleableArray.length; i++)
    {
      toggleableArray[i].visible = false;
    }
  }


  //Remove all the popup content for showing object details
  function removePopupContent()
  {
    $('#object-wrapper h1#page-title').hide();
    $('#object_content_section').animate({height: 'hide'}, 'fast');
  }


  function closeMenu()
  {
    if ($(".object-menu").is(':visible'))
    {
      $(".object-menu .search-results-menu .view-content").hide();
      $(".object-menu .view-alteration-object .view-content").show();
      $('.object-menu #edit-keyword').val("");
      $('.object-menu #edit-keyword').removeClass("x");
    } else
    {
      $(".object-menu-wrapper .menu-link").animate({left: "0"}, 'fast');
      $(".object-menu").animate({width: 'show'}, 'fast');
    }
  }


  function findObject(objectName)
  {
    var object;
    for (var i = 0; i < objArray.length; i++)
    {
      if (objectName.toLowerCase() == objArray[i].name.toLowerCase())
      {
        object = objArray[i];
      }
    }
    return object;
  }


  function findObjectGroup(objectGroup)
  {
    var objects = [];
    for (var i = 0; i < objArray.length; i++)
    {
      if (objectGroup.toLowerCase() == objArray[i].group.toLowerCase())
      {
        objects.push(objArray[i]);
      }
    }
    return objects;
  }


  function findHotspotGroup(hotspotGroup)
  {
    var objects = [];
    for (var i = 0; i < objArray.length; i++) {
      if (hotspotGroup.toLowerCase() == objArray[i].hotspot.toLowerCase()) {
        objects.push(objArray[i]);
      }
    }
    return objects;
  }


  function toggleObjectGroup(name, visibilty)
  {
    if (name == "IND_SiteFencesHoardings" || name == "IND_Scaffolding") {
      findObject('IND_Scaffolding').visible = visibilty;
      findObject('IND_SiteFencesHoardings').visible = visibilty;
    }
    if (name == "IND_InternalSigns") {
      findObject('IND_IndustrialInterior').visible = visibilty;
    }
    for (var i = 0; i < objArray.length; i++) {
      if (name.toLowerCase() == objArray[i].group.toLowerCase()) {
        objArray[i].visible = visibilty;
      }
    }
  }


  function toggleObject(name, visibilty)
  {
    for (var i = 0; i < objArray.length; i++) {
      if (name.toLowerCase() == objArray[i].name.toLowerCase()) {
        objArray[i].visible = visibilty;
      }
    }
  }


  function ghostMenuObjectGroup(menuName)
  {
    for (var i = 0; i < objArray.length; i++) {
      if (menuName.toLowerCase() == objArray[i].menu.toLowerCase())
      {
        objArray[i].visible = true;
        if (objArray[i].ghost)
        {
          for (var j = 0; j < objArray[i].material.materials.length; j++) {
            objArray[i].material.materials[j].opacity = 0.5;
          }
        }
      }
    }
  }


  function resetTextureLock()
  {
    for (var i = 0; i < objArray.length; i++)
    {
      objArray[i].textureLock = false;
    }
  }


  function toggleTextureLock(objectGroup, toggle)
  {
    var objects = findObjectGroup(objectGroup);
    for (var i = 0; i < objects.length; i++) {
      objects[i].textureLock = toggle;
    }
  }


  //Calculate the start position of the canvas
  function calcOffsets()
  {
    var rect = canvas.getBoundingClientRect();
    offsetX = rect.left;
    offsetY = rect.top;
  }


  //Find the URL that the user is looking at, which is used for Google Analytics
  function findTabUrl(tabName)
  {
    var objName = $('.moveLink.selected').text();
    objName = objName.replace(/[^a-zA-Z0-9]/g, '').toLowerCase();
    var tabUrl = "/planning-industrial/" + objName + "/" + tabName;
    return tabUrl;
  }


  //Find the URL (FAQ)  that the user is looking at, which is used for Google Analytics
  function findFaqUrl(faq)
  {
    var objName = $('.moveLink.selected').text();
    objName = objName.replace(/[^a-zA-Z0-9]/g, '').toLowerCase();
    faq = faq.replace(/[^a-zA-Z0-9]/g, '').toLowerCase();
    var faqUrl = "/planning-industrial/" + objName + "/faq/" + faq;
    return faqUrl;
  }


  function getValueFromQueryString(key)
  {
    var param = location.search.match(new RegExp(key + "=(.*?)($|\&)", "i"));
    if (param == undefined) {
      return undefined;
    } else {
      return param[1];
    }
  }


  //Check whether the current camera view is inside house or outside house
  function isInsideHouse(objId)
  {
    if ($('.moveLink[data-attr="' + objId + '"]').parent().parent().prev('h3').find('#BuildingAlterationsInternalMenu').length > 0 || objId == "IND_InternalSigns")
    {
      houseInternal = true;
    } else
    {
      houseInternal = false;
    }
  }


  // FAQ dropdown
  $("#object-wrapper").on('click', 'h3.node-title', function () {

    $(this).parent().next($(".field-name-body")).toggle();
    $(this).toggleClass("active");

    if ($(this).hasClass('active'))
    {
      ga("send", "pageview", {page: findFaqUrl($(this).text())});
    }
  });


  //Get the URL when user clicks the requirements tab
  $('#object-wrapper').on('click', '#requirements-tab', function () {

    if (($(this).hasClass('ui-tabs-active')) && (tabRequireOpen == false))
    {
      tabRestrictOpen = false;
      tabRequireOpen = true;
      ga("send", "pageview", {page: findTabUrl("requirements")});
    }
  });


  //Get the URL when user clicks the restrictions tab
  $('#object-wrapper').on('click', '#restrictions-tab', function () {

    if (($(this).hasClass('ui-tabs-active')) && (tabRestrictOpen == false))
    {
      tabRestrictOpen = true;
      tabRequireOpen = false;
      ga("send", "pageview", {page: findTabUrl("restrictions")});
    }
  });


  // Click on "MENU" tab
  $(".object-menu-wrapper .menu-link").click(function () {
    // UPDATE BY DENIS
    //  $("#object_content_section").animate({height: 'hide'}, 'fast');

    if ($(".object-menu").is(':visible'))
    {
      $(".object-menu").animate({width: 'hide'}, 'fast');
      $(".object-menu-wrapper .menu-link").animate({left: "0"}, 'fast');
    } else
    {
      $(".object-menu").animate({width: 'show'}, 'fast');
      $(".object-menu-wrapper .menu-link").animate({left: "0"}, 'fast');
    }
  });


  //Click on "Content Details Panel"
  $("#object-wrapper").on('click', ' h1', function () {

    if ($("#object_content_section").is(':visible'))
    {
      $("#object_content_section").animate({height: 'hide'}, 'fast');
    } else
    {
      $("#tabs").removeClass('ui-corner-all');
      $("#object_content_section").animate({height: 'show'}, {duration: 100, complete: function () {

          $("#tabs").addClass('ui-corner-all');
        }
      });
    }

    // Give class of active so we can change chevron icon when down
    $(this).toggleClass("active");

    //If "MENU" tab not hidden, close it and slide the menu label to the left
    if ($(".object-menu").is(':visible'))
    {
      $(".object-menu").animate({width: 'hide'}, 'fast');
      $(".object-menu-wrapper .menu-link").animate({left: "0"}, 'fast');
    }
  });

// Checks to see if a matching camera exists and returns the default camera if a matching one is not found
// Arg1: camera ID
// Arg2: object that contains all the camera locations
  function returnApporiateCameraId(cameraId, camLocObj) {
    if (camLocObj[cameraId] == null) {
      cameraId = 'DefaultView';
    }

    return camLocObj[cameraId];
  }

  $(document).ready(function () {

    $('.moveLink').click(function (e) {
      e.preventDefault();
      var id = $(this).attr('data-attr');
      var destination;

      if (webgl)
      {
        destination = returnApporiateCameraId(id, webglCamLoc);
      } else
      {
        destination = returnApporiateCameraId(id, canvasCamLoc);
      }

      if (!$(this).hasClass('selected')) {
        if (destination !== undefined) {

          currentAnimation = id;

          makeObjectsInvisible();
          removePopupContent();
          closeMenu();
          $('#popup_label_scene').hide();
          resetTextureLock();
          resetHighlight();
          resetOpacity();
          toggleObjectGroup(id, true);

          $(this).parents().find('.selected').removeClass('selected');
          $(this).addClass('selected');
          $('.' + id + '_content');
          TWEEN.removeAll();

          if (destination.group !== currentCamLoc.group) {
            // hide things when you click on reset view bouton.
            $('#page-subtitle-object-alteration').hide(); // hide the title of the node
            $('#see_report_in_title_btn').hide(); // hide the button : see reports
            $('#container_related_development_types').hide(); // hide the related dev types section

            tween1 = new TWEEN.Tween(currentCamLoc)
                    .to({posX: 0, posY: 500, posZ: 100, lookX: 0, lookY: 0, lookZ: 0}, 500)
                    .easing(TWEEN.Easing.Circular.InOut)
                    .onUpdate(function () {
                      camera.position.set(currentCamLoc.posX, currentCamLoc.posY, currentCamLoc.posZ);
                      camera.lookAt({x: currentCamLoc.lookX, y: currentCamLoc.lookY, z: currentCamLoc.lookZ});
                    });
            tweenWindow = new TWEEN.Tween(currentCamLoc)
                    .to({posX: -180, posY: 16, posZ: 158, lookX: -180, lookY: 16, lookZ: 126}, 500)
                    .easing(TWEEN.Easing.Circular.InOut)
                    .onUpdate(function () {
                      camera.position.set(currentCamLoc.posX, currentCamLoc.posY, currentCamLoc.posZ);
                      camera.lookAt({x: currentCamLoc.lookX, y: currentCamLoc.lookY, z: currentCamLoc.lookZ});
                    });
          }

          tween2 = new TWEEN.Tween(currentCamLoc)
                  .to({posX: destination.posX, posY: destination.posY, posZ: destination.posZ, lookX: destination.lookX, lookY: destination.lookY, lookZ: destination.lookZ}, 750)
                  .easing(TWEEN.Easing.Circular.InOut)
                  .onUpdate(function () {
                    camera.position.set(currentCamLoc.posX, currentCamLoc.posY, currentCamLoc.posZ);
                    camera.lookAt({x: currentCamLoc.lookX, y: currentCamLoc.lookY, z: currentCamLoc.lookZ});
                  })
                  .onComplete(function () {
                    if (id == currentAnimation) {
                      if (id !== "ResetView") {
                        $('#object_content_section').show();
                        highlightObjectGroup(id, "highlight");
                        toggleTextureLock(id, true);
                        isInsideHouse(id);
                        ga("send", "pageview", {page: findTabUrl("requirements")});
                      } else
                      {

                        removePopupContent();
                        $('.view-alteration-object .view-content').find('.active').removeClass('active');
                        ga("send", "event", "resetview", "button_click", this.href);

                        if (id != null) {
                          houseInternal = false;
                        } else {
                          houseInternal = true;
                        }
                      }
                      currentAnimation = "";
                      currentFocus = id.charAt(0).toLowerCase() + id.slice(1);
                    }
                  });


          if (destination.group !== currentCamLoc.group)
          {
            if (destination.group == 'F')
            {
              if (currentCamLoc.group == 'A')
              {
                tweenWindow.chain(tween2);
                tweenWindow.start();
              } else
              {
                tween1.chain(tweenWindow);
                tweenWindow.chain(tween2);
                tween1.start();
              }
            } else if (currentCamLoc.group == 'F')
            {
              if (destination.group == 'A')
              {
                tweenWindow.chain(tween2);
                tweenWindow.start();
              } else
              {
                tweenWindow.chain(tween1);
                tween1.chain(tween2);
                tweenWindow.start();
              }
            } else
            {
              tween1.chain(tween2);
              tween1.start();
            }
          } else
          {
            tween2.start();
          }

          currentCamLoc.group = destination.group;
        }
      } else
      {
        closeMenu();
        $('.view-alteration-object .view-content').find('.active').removeClass('active');
      }
    });


    $('.view-content h3').click(function (e) {
      e.preventDefault();

      //Active class is for showing the sub menu
      $('ul').not($(this).next('ul')).removeClass('active');
      $(this).next('ul').toggleClass('active');

      var id = $(this).children().not('.arrow').attr('id');
      var destination;

      if (webgl)
      {
        destination = webglCamLoc[id];
      } else
      {
        destination = canvasCamLoc[id];
      }

      //If this is not selected and not active then animate
      if (!$(this).hasClass('selected') && $(this).hasClass('active')) {
        if (destination !== undefined) {
          currentAnimation = id;

          makeObjectsInvisible();
          removePopupContent();
          $('#popup_label_scene').hide();
          resetTextureLock();
          resetHighlight();
          resetOpacity();
          ghostMenuObjectGroup(id);

          //Selected class tells us that the camera is currently focusing on this item
          $(this).parents().find('.selected').removeClass('selected');
          $(this).addClass('selected');
          TWEEN.removeAll();

          if (destination.group !== currentCamLoc.group) {
            tween1 = new TWEEN.Tween(currentCamLoc)
                    .to({posX: 0, posY: 500, posZ: 100, lookX: 0, lookY: 0, lookZ: 0}, 500)
                    .easing(TWEEN.Easing.Circular.InOut)
                    .onUpdate(function () {
                      camera.position.set(currentCamLoc.posX, currentCamLoc.posY, currentCamLoc.posZ);
                      camera.lookAt({x: currentCamLoc.lookX, y: currentCamLoc.lookY, z: currentCamLoc.lookZ});
                    });
            tweenWindow = new TWEEN.Tween(currentCamLoc)
                    .to({posX: 180, posY: 15, posZ: -60, lookX: 0, lookY: 15, lookZ: -60}, 500)
                    .easing(TWEEN.Easing.Circular.InOut)
                    .onUpdate(function () {
                      camera.position.set(currentCamLoc.posX, currentCamLoc.posY, currentCamLoc.posZ);
                      camera.lookAt({x: currentCamLoc.lookX, y: currentCamLoc.lookY, z: currentCamLoc.lookZ});
                    });
          }

          tween2 = new TWEEN.Tween(currentCamLoc)
                  .to({posX: destination.posX, posY: destination.posY, posZ: destination.posZ, lookX: destination.lookX, lookY: destination.lookY, lookZ: destination.lookZ}, 750)
                  .easing(TWEEN.Easing.Circular.InOut)
                  .onUpdate(function () {
                    camera.position.set(currentCamLoc.posX, currentCamLoc.posY, currentCamLoc.posZ);
                    camera.lookAt({x: currentCamLoc.lookX, y: currentCamLoc.lookY, z: currentCamLoc.lookZ});
                  })
                  .onComplete(function () {
                    if (id == currentAnimation) {
                      highlightObjectGroup(id, "highlight");
                      toggleTextureLock(id, true);
                      ga("send", "pageview", {page: findTabUrl("requirements")});
                      currentAnimation = "";
                      currentFocus = id.charAt(0).toLowerCase() + id.slice(1);

                      if (id != null) {
                        houseInternal = false;
                      } else {
                        houseInternal = true;
                      }
                    }
                  });

          if (destination.group !== currentCamLoc.group)
          {
            if (destination.group == 'F')
            {
              if (currentCamLoc.group == 'A')
              {
                tweenWindow.chain(tween2);
                tweenWindow.start();
              } else
              {
                tween1.chain(tweenWindow);
                tweenWindow.chain(tween2);
                tween1.start();
              }
            } else if (currentCamLoc.group == 'F')
            {
              if (destination.group == 'A')
              {
                tweenWindow.chain(tween2);
                tweenWindow.start();
              } else
              {
                tweenWindow.chain(tween1);
                tween1.chain(tween2);
                tweenWindow.start();
              }
            } else
            {
              tween1.chain(tween2);
              tween1.start();
            }
          } else
          {
            tween2.start();
          }

          currentCamLoc.group = destination.group;
        }
      }
    });


    /********* DEBUG CODE START ***********/
    /*  $('.middle_content').append('<span id="cameraTest"><style>#cameraTest input{width:60px;} #cameraTest{display:block; text-align: center;position:absolute; margin-top: 350px;width:100%;}</style><span>posX<input type="text" name="posX" value="0">posY<input type="text" name="posY" value="0">posZ<input type="text" name="posZ" value="0">lookX<input type="text" name="lookX" value="0">lookY<input type="text" name="lookY" value="0">lookZ<input type="text" name="lookZ" value="0"><button id="moveCam">update</button></span></span>');
     $('#moveCam').click(function(e){
     e.preventDefault();
     updateCam(500);
     });
     var increment = 1;
     function updateCam(time){
     tweenMove = new TWEEN.Tween(currentCamLoc)
     .to({posX: parseInt($('input[name="posX"]').val()), posY: parseInt($('input[name="posY"]').val()), posZ: parseInt($('input[name="posZ"]').val()), lookX: parseInt($('input[name="lookX"]').val()), lookY: parseInt($('input[name="lookY"]').val()), lookZ: parseInt($('input[name="lookZ"]').val())}, time)
     .easing(TWEEN.Easing.Circular.InOut)
     .onUpdate(function(){
     camera.position.set(currentCamLoc.posX,currentCamLoc.posY,currentCamLoc.posZ);
     camera.lookAt( {x:currentCamLoc.lookX, y:currentCamLoc.lookY, z:currentCamLoc.lookZ } );
     })
     tweenMove.start();
     }
     $('#cameraTest input').keydown(function(e){
     if(e.which == 13){
     $('#moveCam').click();
     }else if(e.which == 38){
     $(this).val( parseInt($(this).val()) + increment ) ;
     updateCam(1);
     }else if(e.which == 40){
     $(this).val( parseInt($(this).val()) - increment ) ;
     updateCam(1);
     }
     $(window).keydown(function(e) {
     if(e.which == 16){
     increment = 10;
     }
     });
     $(window).keyup(function(e) {
     if(e.which == 16){
     increment = 1;
     }
     });
     });
     if(webgl)
     {
     $('input[name="posX"]').val(webglCamLoc['ResetView'].posX);
     $('input[name="posY"]').val(webglCamLoc['ResetView'].posY);
     $('input[name="posZ"]').val(webglCamLoc['ResetView'].posZ);
     $('input[name="lookX"]').val(webglCamLoc['ResetView'].lookX);
     $('input[name="lookY"]').val(webglCamLoc['ResetView'].lookY);
     $('input[name="lookZ"]').val(webglCamLoc['ResetView'].lookZ);
     }
     else
     {
     $('input[name="posX"]').val(canvasCamLoc['ResetView'].posX);
     $('input[name="posY"]').val(canvasCamLoc['ResetView'].posY);
     $('input[name="posZ"]').val(canvasCamLoc['ResetView'].posZ);
     $('input[name="lookX"]').val(canvasCamLoc['ResetView'].lookX);
     $('input[name="lookY"]').val(canvasCamLoc['ResetView'].lookY);
     $('input[name="lookZ"]').val(canvasCamLoc['ResetView'].lookZ);
     }
     $('.moveLink').click(function(e){
     e.preventDefault();
     var id = $(this).attr('data-attr');
     if(webgl)
     {
     $('input[name="posX"]').val(webglCamLoc[id].posX);
     $('input[name="posY"]').val(webglCamLoc[id].posY);
     $('input[name="posZ"]').val(webglCamLoc[id].posZ);
     $('input[name="lookX"]').val(webglCamLoc[id].lookX);
     $('input[name="lookY"]').val(webglCamLoc[id].lookY);
     $('input[name="lookZ"]').val(webglCamLoc[id].lookZ);
     }
     else
     {
     $('input[name="posX"]').val(canvasCamLoc[id].posX);
     $('input[name="posY"]').val(canvasCamLoc[id].posY);
     $('input[name="posZ"]').val(canvasCamLoc[id].posZ);
     $('input[name="lookX"]').val(canvasCamLoc[id].lookX);
     $('input[name="lookY"]').val(canvasCamLoc[id].lookY);
     $('input[name="lookZ"]').val(canvasCamLoc[id].lookZ);
     }
     });     */
    /********* DEBUG CODE END ***********/

  });

})(jQuery);
