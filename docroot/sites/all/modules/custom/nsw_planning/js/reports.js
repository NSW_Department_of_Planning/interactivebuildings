$(function($) {


	base_path = Drupal.settings.nsw_planning.baseUrl;




	// Clear report cookies
	function clearCookies() {

		var cookies = $.cookie();
		var needle = /node_/;

		for(var cookie in cookies) {

			var found = cookie.search(needle);

			if(found != -1) {
				$.removeCookie(cookie);
			}
		}
	}




	// Saved as $.cookie and $_COOKIE
	function saveViewedItems(){

		nid = $(this).attr('data-altobj');

		$.cookie.json = true;
		$.cookie.raw = true;
		$.cookie('node_'+nid, nid,  { expires: 1, path: '/'});
	}




	// Save alteration objects viewed
	$('a[data-altobj]').on("click",saveViewedItems);



	$(document).ready(function() {

		// Select/de-select all	   
		$('#edit-select-all').click(function(event) {  //on click
		        if(this.checked) { // check select status
		            $('.form-checkbox').each(function() { //loop through each checkbox
		                this.checked = true;  //select all checkboxes with class "checkbox1"              
		            });
		        }else{
		            $('.form-checkbox').each(function() { //loop through each checkbox
		                this.checked = false; //deselect all checkboxes with class "checkbox1"                      
		            });        
		        }
		});

	});



	// ajaxComplete()
	jQuery(document).ajaxComplete(function() {

    $('a[data-altobj]').on("click", saveViewedItems);
    // clear node_ cookie
    $("input.search-result-button.form-submit").click(function () {
      return clearCookies();
    });
	});


	// Clear node_ cookies
	$("input.btn-clear").click(function() {
		return clearCookies();
  });

  $("#edit-clear-address").click(function () {
    return clearCookies();
  });

  $("div.search-result-button-wrap").click(function () {
    return clearCookies();
  });


});

