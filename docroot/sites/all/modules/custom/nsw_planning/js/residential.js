
jq1102(function ($) {

  window.onload = function ()
  {
    threeStart();
    window.addEventListener('resize', resize3dArea, false);
    resize3dArea();
  };


  //$('.object-menu .view-alteration-object').addClass('active-menu');
  var width;
  var marginLeft;   //marginLeft is the number that shows the distance from the Menu label to the left margin of canvas
  function resize3dArea()
  {
    camera.aspect = 16 / 9;
    camera.updateProjectionMatrix();
    width = $('#canvas_container').width();

    if (width > 940)
    {
      width = 940;
    }
    renderer.setSize(width, width / (16 / 9));

    marginLeft = 310; //parseInt($('.view-alteration-object .view-footer').css('marginLeft').replace('px', '')) + parseInt($('.view-alteration-object .view-content').css('width').replace('px', '')) + parseInt($('.view-alteration-object .view-content').css("left").replace('px', ''));

    //popup_label_scene is the section which overlays on the 3D canvas, and it has the same width and height as the canvas, but it is invisible
    $('#popup_label_scene').css({"width": width, "height": width / (16 / 9)});
    var popup_marginLeft = -(width / 2);
    $('#popup_label_scene').css({"margin-left": popup_marginLeft});
  }

  /*
   Collection of camera positions.
   Camera position names should match the ID on the equivalent navigation link.

   posX, posY, posZ = Position of the camera.
   lookX, lookY, lookZ = Where the camera looks at.
   group = Moving between camera positions of the same group will not trigger the flyover effect. The flyover is needed to avoid clipping through the house and other objects.
   */
  var currentCamLoc = {posX: 310, posY: 110, posZ: 160, lookX: 0, lookY: -20, lookZ: -60, group: 'A'};
  var webglCamLoc = {
    ResetView: {posX: 310, posY: 110, posZ: 160, lookX: 0, lookY: -20, lookZ: -60, group: 'A'},
    DefaultView: {posX: 310, posY: 110, posZ: 160, lookX: 0, lookY: -20, lookZ: -60, group: 'A'},
    RES_Blinds: {posX: 160, posY: 30, posZ: -140, lookX: 120, lookY: 18, lookZ: -100, group: 'A'},
    RES_BuildingIdentificationSigns: {posX: 150, posY: 25, posZ: 30, lookX: 120, lookY: 15, lookZ: 13, group: 'A'},
    RES_Carport: {posX: 150, posY: 40, posZ: 140, lookX: 80, lookY: 5, lookZ: 80, group: 'A'},
    RES_CommunityNotice: {posX: 310, posY: 110, posZ: 160, lookX: 0, lookY: -20, lookZ: -60, group: 'A'},
    RES_Demolition: {posX: 310, posY: 110, posZ: 160, lookX: 0, lookY: -20, lookZ: -60, group: 'A'},
    RES_Driveways: {posX: 230, posY: 40, posZ: 140, lookX: 130, lookY: -17, lookZ: 50, group: 'A'},
    RES_ElectionSigns: {posX: 210, posY: 30, posZ: 30, lookX: 180, lookY: 7, lookZ: 5, group: 'A'},
    RES_EmergencyWork: {posX: 310, posY: 110, posZ: 160, lookX: 0, lookY: -20, lookZ: -60, group: 'A'},
    RES_FencesResidential: {posX: 250, posY: 60, posZ: -190, lookX: 55, lookY: -56, lookZ: 0, group: 'A'},
    RES_FencesRural: {posX: 260, posY: 70, posZ: 180, lookX: 65, lookY: -53, lookZ: 0, group: 'A'},
    RES_Flagpoles: {posX: 238, posY: 40, posZ: -114, lookX: 170, lookY: 14, lookZ: -65, group: 'A'},
    RES_GarbageBinStorage: {posX: 150, posY: 30, posZ: 125, lookX: 105, lookY: 13, lookZ: 102, group: 'A'},
    RES_Hardstanding: {posX: 150, posY: 40, posZ: 140, lookX: 80, lookY: 5, lookZ: 80, group: 'A'},
    RES_HomeBasedChildCare: {posX: 310, posY: 110, posZ: 160, lookX: 0, lookY: -20, lookZ: -60, group: 'A'},
    RES_HomeBusinesses: {posX: 310, posY: 110, posZ: 160, lookX: 0, lookY: -20, lookZ: -60, group: 'A'},
    RES_Letterboxes: {posX: 220, posY: 15, posZ: 12, lookX: 190, lookY: 10, lookZ: -5, group: 'A'},
    RES_MaintenanceBuildingsDraftHeritage: {posX: 310, posY: 110, posZ: 160, lookX: 0, lookY: -20, lookZ: -60, group: 'A'},
    RES_MinorExternalBuildingAlterations: {posX: 170, posY: 25, posZ: 30, lookX: 140, lookY: 15, lookZ: 10, group: 'A'},
    RES_PathwaysPaving: {posX: 230, posY: 55, posZ: 50, lookX: 170, lookY: 5, lookZ: 0, group: 'A'},
    RES_RealEstateSigns: {posX: 220, posY: 20, posZ: 70, lookX: 180, lookY: 8, lookZ: 35, group: 'A'},
    RES_ReplacementIdentificationSigns: {posX: 310, posY: 110, posZ: 160, lookX: 0, lookY: -20, lookZ: -60, group: 'A'},
    RES_RollerShutterDoors: {posX: 310, posY: 110, posZ: 160, lookX: 0, lookY: -20, lookZ: -60, group: 'A'},
    RES_ScreenEnclosures: {posX: 190, posY: 32, posZ: 50, lookX: 140, lookY: 14, lookZ: 10, group: 'A'},
    RES_TemporaryEventSigns: {posX: 310, posY: 110, posZ: 160, lookX: 0, lookY: -20, lookZ: -60, group: 'A'},
    RES_Verandahs: {posX: 190, posY: 32, posZ: 50, lookX: 140, lookY: 14, lookZ: 10, group: 'A'},
    RES_WallSigns: {posX: 170, posY: 25, posZ: -20, lookX: 155, lookY: 19, lookZ: -30, group: 'A'},
    RES_AccessRamps: {posX: -30, posY: 20, posZ: 50, lookX: 10, lookY: 5, lookZ: 10, group: 'B'},
    RES_AerialsAntennaeRoof: {posX: 10, posY: 70, posZ: 70, lookX: 50, lookY: 25, lookZ: 10, group: 'B'},
    RES_AerialsAntennaeGround: {posX: -140, posY: 70, posZ: 57, lookX: -80, lookY: 15, lookZ: 0, group: 'B'},
    RES_AirConditioningUnitsWall: {posX: -20, posY: 20, posZ: -100, lookX: 15, lookY: 15, lookZ: -75, group: 'B'},
    RES_AirConditioningUnitsGround: {posX: -5, posY: 20, posZ: 50, lookX: 10, lookY: 15, lookZ: 42, group: 'B'},
    RES_AnimalShelters: {posX: -20, posY: 20, posZ: -100, lookX: 10, lookY: 4, lookZ: -77, group: 'B'},
    RES_Aviaries: {posX: -20, posY: 30, posZ: 70, lookX: -135, lookY: -22, lookZ: -20, group: 'B'},
    RES_AwningsCanopies: {posX: -50, posY: 35, posZ: -75, lookX: 0, lookY: 18, lookZ: -35, group: 'B'},
    RES_BalconiesDecks: {posX: -25, posY: 20, posZ: -85, lookX: 15, lookY: 1, lookZ: -55, group: 'B'},
    RES_Barbecues: {posX: -15, posY: 22, posZ: -95, lookX: 10, lookY: 8, lookZ: -75, group: 'B'},
    RES_CabanaGazebo: {posX: -75, posY: 25, posZ: 16, lookX: -60, lookY: 15, lookZ: -10, group: 'B'},
    RES_ClothesHoistsLines: {posX: -90, posY: 25, posZ: 50, lookX: -60, lookY: 9, lookZ: 10, group: 'B'},
    RES_CubbyHouse: {posX: -35, posY: 30, posZ: -25, lookX: -110, lookY: -12, lookZ: -85, group: 'B'},
    RES_EarthworksRetainingWalls: {posX: -60, posY: 40, posZ: 20, lookX: -100, lookY: 10, lookZ: -10, group: 'B'},
    RES_EvaporativeCoolingUnits: {posX: -10, posY: 60, posZ: -90, lookX: 50, lookY: 32, lookZ: -45, group: 'B'},
    RES_HotWaterSystems: {posX: -10, posY: 25, posZ: -120, lookX: 25, lookY: 10, lookZ: -100, group: 'B'},
    RES_GreenhouseFernery: {posX: -6, posY: 45, posZ: 37, lookX: -110, lookY: -12, lookZ: -35, group: 'B'},
    RES_LandscapingStructures: {posX: -45, posY: 15, posZ: 20, lookX: -10, lookY: -5, lookZ: -28, group: 'B'},
    RES_GardenShed: {posX: -50, posY: 45, posZ: -5, lookX: -145, lookY: -7, lookZ: -85, group: 'B'},
    RES_PatiosTerraces: {posX: -40, posY: 25, posZ: -55, lookX: 5, lookY: 3, lookZ: -35, group: 'B'},
    RES_Pergola: {posX: -45, posY: 40, posZ: -80, lookX: 5, lookY: 15, lookZ: -50, group: 'B'},
    RES_PlaygroundEquipment: {posX: -80, posY: 25, posZ: -20, lookX: -50, lookY: 4, lookZ: -65, group: 'B'},
    RES_PortableSwimmingPools: {posX: -125, posY: 60, posZ: 70, lookX: -55, lookY: 12, lookZ: 25, group: 'B'},
    RES_PrivacyScreens: {posX: -115, posY: 45, posZ: 75, lookX: 25, lookY: -26, lookZ: -15, group: 'B'},
    RES_RainwaterTanks: {posX: -40, posY: 35, posZ: -110, lookX: 15, lookY: 10, lookZ: -85, group: 'B'},
    RES_Scaffolding: {posX: -30, posY: 22, posZ: -120, lookX: 20, lookY: 5, lookZ: -80, group: 'B'},
    RES_SculpturesArtworks: {posX: -60, posY: 15, posZ: 50, lookX: -25, lookY: 5, lookZ: 20, group: 'B'},
    RES_ShadeStructures: {posX: -90, posY: 40, posZ: 50, lookX: -60, lookY: 15, lookZ: 25, group: 'B'},
    RES_SiteFencesHoardings: {posX: -30, posY: 22, posZ: -120, lookX: 20, lookY: 2, lookZ: -80, group: 'B'},
    RES_Skylights: {posX: -10, posY: 50, posZ: -120, lookX: 50, lookY: 25, lookZ: -60, group: 'B'},
    RES_SolarEnergySystems: {posX: -10, posY: 50, posZ: 50, lookX: 35, lookY: 32, lookZ: 5, group: 'B'},
    RES_TemporaryBuildersStructures: {posX: -109, posY: 30, posZ: -50, lookX: -14, lookY: -28, lookZ: -165, group: 'B'},
    RES_WaterPonds: {posX: -85, posY: 25, posZ: 0, lookX: -55, lookY: 5, lookZ: -35, group: 'B'},
    RES_MIBA_Bathroom: {posX: 110, posY: 18, posZ: -50, lookX: 90, lookY: 9, lookZ: -70, group: 'F'},
    RES_MIBA_Bedroom: {posX: 125, posY: 15, posZ: -70, lookX: 100, lookY: 8, lookZ: -60, group: 'F'},
    RES_MIBA_Kitchen: {posX: 100, posY: 15, posZ: -50, lookX: 300, lookY: -40, lookZ: -140, group: 'F'},
    RES_MIBA_Stairs: {posX: 105, posY: 20, posZ: -65, lookX: 35, lookY: -13, lookZ: -79, group: 'F'},
    RES_MinorInternalBuildingAlterations: {posX: 100, posY: 15, posZ: -50, lookX: 300, lookY: 0, lookZ: -140, group: 'F'},
    BuildingAlterationsExternalMenu: {posX: -50, posY: 45, posZ: -120, lookX: 10, lookY: 15, lookZ: -80, group: 'B'},
    CarportsAccessMenu: {posX: 45, posY: 110, posZ: 160, lookX: 70, lookY: 50, lookZ: 85, group: 'A'},
    OutdoorItemsMenu: {posX: -170, posY: 100, posZ: 120, lookX: -90, lookY: 15, lookZ: 30, group: 'B'},
    FencesWallsMenu: {posX: 250, posY: 60, posZ: -190, lookX: 55, lookY: -30, lookZ: 0, group: 'A'},
    RoofMenu: {posX: -10, posY: 60, posZ: -90, lookX: 10, lookY: 50, lookZ: -77, group: 'B'},
    ShedsMenu: {posX: -250, posY: 85, posZ: -85, lookX: -120, lookY: -15, lookZ: -55, group: 'B'},
    SignsMenu: {posX: 220, posY: 45, posZ: 65, lookX: 170, lookY: 15, lookZ: 20, group: 'A'},
    MaintenanceMenu: {posX: -90, posY: 45, posZ: 0, lookX: -30, lookY: 10, lookZ: -80, group: 'B'},
  };

  var canvasCamLoc = {
    ResetView: {posX: 310, posY: 110, posZ: 160, lookX: 0, lookY: -20, lookZ: -60, group: 'A'},
    DefaultView: {posX: 310, posY: 110, posZ: 160, lookX: 0, lookY: -20, lookZ: -60, group: 'A'},
    RES_Blinds: {posX: 160, posY: 30, posZ: -140, lookX: 120, lookY: 18, lookZ: -100, group: 'A'},
    RES_BuildingIdentificationSigns: {posX: 150, posY: 12.5, posZ: 11, lookX: 126, lookY: 12.5, lookZ: 10.7, group: 'A'},
    RES_Carport: {posX: 150, posY: 40, posZ: 140, lookX: 80, lookY: 5, lookZ: 80, group: 'A'},
    RES_CommunityNotice: {posX: 310, posY: 110, posZ: 160, lookX: 0, lookY: -20, lookZ: -60, group: 'A'},
    RES_Demolition: {posX: 310, posY: 110, posZ: 160, lookX: 0, lookY: -20, lookZ: -60, group: 'A'},
    RES_Driveways: {posX: 230, posY: 40, posZ: 140, lookX: 130, lookY: -17, lookZ: 50, group: 'A'},
    RES_ElectionSigns: {posX: 202, posY: 22, posZ: 1, lookX: 179, lookY: 3, lookZ: 1, group: 'A'},
    RES_EmergencyWork: {posX: 310, posY: 110, posZ: 160, lookX: 0, lookY: -20, lookZ: -60, group: 'A'},
    RES_FencesResidential: {posX: 250, posY: 60, posZ: -190, lookX: 55, lookY: -56, lookZ: 0, group: 'A'},
    RES_FencesRural: {posX: 260, posY: 70, posZ: 180, lookX: 65, lookY: -53, lookZ: 0, group: 'A'},
    RES_Flagpoles: {posX: 238, posY: 40, posZ: -114, lookX: 170, lookY: 14, lookZ: -65, group: 'A'},
    RES_GarbageBinStorage: {posX: 150, posY: 30, posZ: 125, lookX: 105, lookY: 11, lookZ: 102, group: 'A'},
    RES_Hardstanding: {posX: 150, posY: 40, posZ: 140, lookX: 80, lookY: 5, lookZ: 80, group: 'A'},
    RES_HomeBasedChildCare: {posX: 310, posY: 110, posZ: 160, lookX: 0, lookY: -20, lookZ: -60, group: 'A'},
    RES_HomeBusinesses: {posX: 310, posY: 110, posZ: 160, lookX: 0, lookY: -20, lookZ: -60, group: 'A'},
    RES_Letterboxes: {posX: 230, posY: 15, posZ: 0, lookX: 190, lookY: 5, lookZ: -4, group: 'A'},
    RES_MaintenanceBuildingsDraftHeritage: {posX: 310, posY: 110, posZ: 160, lookX: 0, lookY: -20, lookZ: -60, group: 'A'},
    RES_MinorExternalBuildingAlterations: {posX: 170, posY: 8, posZ: -4, lookX: 140, lookY: 4.5, lookZ: -4, group: 'A'},
    RES_PathwaysPaving: {posX: 210, posY: 35, posZ: 30, lookX: 170, lookY: 10, lookZ: 0, group: 'A'},
    RES_RealEstateSigns: {posX: 220, posY: 15, posZ: 25, lookX: 150, lookY: 12, lookZ: 25, group: 'A'},
    RES_ReplacementIdentificationSigns: {posX: 310, posY: 110, posZ: 160, lookX: 0, lookY: -20, lookZ: -60, group: 'A'},
    RES_RollerShutterDoors: {posX: 310, posY: 110, posZ: 160, lookX: 0, lookY: -20, lookZ: -60, group: 'A'},
    RES_ScreenEnclosures: {posX: 190, posY: 30, posZ: 10, lookX: 149.6, lookY: 13.5, lookZ: 0.5, group: 'A'},
    RES_TemporaryEventSigns: {posX: 310, posY: 110, posZ: 160, lookX: 0, lookY: -20, lookZ: -60, group: 'A'},
    RES_Verandahs: {posX: 190, posY: 32, posZ: 50, lookX: 140, lookY: 14, lookZ: 10, group: 'A'},
    RES_WallSigns: {posX: 175, posY: 13, posZ: -36.3, lookX: 149.5, lookY: 11, lookZ: -39, group: 'A'},
    RES_AccessRamps: {posX: -30, posY: 20, posZ: 50, lookX: 10, lookY: 5, lookZ: 10, group: 'B'},
    RES_AerialsAntennaeRoof: {posX: 10, posY: 70, posZ: 70, lookX: 50, lookY: 25, lookZ: 10, group: 'B'},
    RES_AerialsAntennaeGround: {posX: -140, posY: 70, posZ: 57, lookX: -80, lookY: 15, lookZ: 0, group: 'B'},
    RES_AirConditioningUnitsWall: {posX: -20, posY: 20, posZ: -100, lookX: 15, lookY: 15, lookZ: -75, group: 'B'},
    RES_AirConditioningUnitsGround: {posX: -5, posY: 20, posZ: 50, lookX: 10, lookY: 15, lookZ: 42, group: 'B'},
    RES_AnimalShelters: {posX: -20, posY: 20, posZ: -100, lookX: 10, lookY: 4, lookZ: -77, group: 'B'},
    RES_Aviaries: {posX: -20, posY: 30, posZ: 70, lookX: -135, lookY: -10, lookZ: -22, group: 'B'},
    RES_AwningsCanopies: {posX: -50, posY: 35, posZ: -75, lookX: 0, lookY: 18, lookZ: -35, group: 'B'},
    RES_BalconiesDecks: {posX: -25, posY: 20, posZ: -85, lookX: 15, lookY: 1, lookZ: -55, group: 'B'},
    RES_Barbecues: {posX: -15, posY: 22, posZ: -92, lookX: 10, lookY: 8, lookZ: -75, group: 'B'},
    RES_CabanaGazebo: {posX: -75, posY: 25, posZ: 16, lookX: -60, lookY: 15, lookZ: -10, group: 'B'},
    RES_ClothesHoistsLines: {posX: -90, posY: 25, posZ: 50, lookX: -60, lookY: 9, lookZ: 10, group: 'B'},
    RES_CubbyHouse: {posX: -35, posY: 30, posZ: -25, lookX: -110, lookY: -16, lookZ: -85, group: 'B'},
    RES_EarthworksRetainingWalls: {posX: -50, posY: 55, posZ: 35, lookX: -100, lookY: 13, lookZ: -10, group: 'B'},
    RES_EvaporativeCoolingUnits: {posX: 0, posY: 60, posZ: -55, lookX: 50, lookY: 32, lookZ: -45, group: 'B'},
    RES_HotWaterSystems: {posX: -10, posY: 25, posZ: -120, lookX: 25, lookY: 10, lookZ: -100, group: 'B'},
    RES_GreenhouseFernery: {posX: -6, posY: 45, posZ: 26, lookX: -110, lookY: -12, lookZ: -35, group: 'B'},
    RES_LandscapingStructures: {posX: -45, posY: 15, posZ: 20, lookX: -10, lookY: -5, lookZ: -28, group: 'B'},
    RES_GardenShed: {posX: -50, posY: 45, posZ: -5, lookX: -145, lookY: -7, lookZ: -85, group: 'B'},
    RES_PatiosTerraces: {posX: -40, posY: 25, posZ: -55, lookX: 5, lookY: 3, lookZ: -35, group: 'B'},
    RES_Pergola: {posX: -45, posY: 20, posZ: -80, lookX: 5, lookY: 15, lookZ: -50, group: 'B'},
    RES_PlaygroundEquipment: {posX: -80, posY: 25, posZ: -20, lookX: -50, lookY: 10, lookZ: -65, group: 'B'},
    RES_PortableSwimmingPools: {posX: -125, posY: 60, posZ: 70, lookX: -55, lookY: 12, lookZ: 25, group: 'B'},
    RES_PrivacyScreens: {posX: -115, posY: 45, posZ: 75, lookX: 25, lookY: -26, lookZ: -15, group: 'B'},
    RES_RainwaterTanks: {posX: -20, posY: 25, posZ: -100, lookX: 15, lookY: 10, lookZ: -85, group: 'B'},
    RES_Scaffolding: {posX: -30, posY: 22, posZ: -110, lookX: 35, lookY: 12, lookZ: -75, group: 'B'},
    RES_SculpturesArtworks: {posX: -60, posY: 15, posZ: 50, lookX: -25, lookY: 5, lookZ: 20, group: 'B'},
    RES_ShadeStructures: {posX: -90, posY: 40, posZ: 50, lookX: -60, lookY: 15, lookZ: 25, group: 'B'},
    RES_SiteFencesHoardings: {posX: -30, posY: 22, posZ: -120, lookX: 20, lookY: 12, lookZ: -80, group: 'B'},
    RES_Skylights: {posX: 0, posY: 60, posZ: -110, lookX: 50, lookY: 30, lookZ: -60, group: 'B'},
    RES_SolarEnergySystems: {posX: 0, posY: 55, posZ: 25, lookX: 35, lookY: 32, lookZ: 5, group: 'B'},
    RES_TemporaryBuildersStructures: {posX: -109, posY: 30, posZ: -50, lookX: -14, lookY: -28, lookZ: -165, group: 'B'},
    RES_WaterPonds: {posX: -85, posY: 25, posZ: 0, lookX: -55, lookY: 5, lookZ: -35, group: 'B'},
    RES_MIBA_Bathroom: {posX: 85, posY: 11, posZ: -37, lookX: 85, lookY: 8, lookZ: -65, group: 'F'},
    RES_MIBA_Bedroom: {posX: 120, posY: 12, posZ: -53, lookX: 102, lookY: 12, lookZ: -53, group: 'F'},
    RES_MIBA_Kitchen: {posX: 125, posY: 15, posZ: -40, lookX: 120, lookY: 0, lookZ: -140, group: 'F'},
    RES_MIBA_Stairs: {posX: 113, posY: 20, posZ: -54, lookX: 36, lookY: -13, lookZ: -71, group: 'F'},
    RES_MinorInternalBuildingAlterations: {posX: 120, posY: 15, posZ: -40, lookX: 120, lookY: 10, lookZ: -140, group: 'F'},
    BuildingAlterationsExternalMenu: {posX: -50, posY: 45, posZ: -120, lookX: 10, lookY: 15, lookZ: -80, group: 'B'},
    CarportsAccessMenu: {posX: 45, posY: 110, posZ: 160, lookX: 70, lookY: 50, lookZ: 85, group: 'A'},
    OutdoorItemsMenu: {posX: -170, posY: 100, posZ: 120, lookX: -90, lookY: 15, lookZ: 30, group: 'B'},
    FencesWallsMenu: {posX: 230, posY: 100, posZ: -210, lookX: 55, lookY: -30, lookZ: 0, group: 'A'},
    RoofMenu: {posX: -10, posY: 60, posZ: -90, lookX: 10, lookY: 50, lookZ: -77, group: 'B'},
    ShedsMenu: {posX: -250, posY: 85, posZ: -85, lookX: -120, lookY: -15, lookZ: -55, group: 'B'},
    SignsMenu: {posX: 220, posY: 45, posZ: 65, lookX: 170, lookY: 15, lookZ: 20, group: 'A'},
    MaintenanceMenu: {posX: -90, posY: 45, posZ: 0, lookX: -30, lookY: 10, lookZ: -80, group: 'B'},
  };

  //Contains infomation on all the objects. Instead of having too many arguments in the loadObjects function, we want to eventually put all the object info here instead.
  //"ghost" parameter indicates whether the object needs to be transparency
  //"highlightable" parameter indicates whether the object can have texture, such as blue color
  var objInfo = {
    RES_BalconiesDecks: {menu: "BuildingAlterationsExternalMenu", ghost: true, highlightable: true},
    RES_AirConditioningUnitsWall: {menu: "BuildingAlterationsExternalMenu", ghost: true, highlightable: true},
    RES_HotWaterSystems: {menu: "BuildingAlterationsExternalMenu", ghost: true, highlightable: true},
    RES_ScreenEnclosures: {menu: "BuildingAlterationsExternalMenu", ghost: true, highlightable: true},
    RES_Verandahs: {menu: "BuildingAlterationsExternalMenu", ghost: true, highlightable: true},
    RES_AwningsCanopies: {menu: "BuildingAlterationsExternalMenu", ghost: false, highlightable: true},
    RES_Demolition: {menu: "BuildingAlterationsExternalMenu", ghost: true, highlightable: true},
    RES_MinorExternalBuildingAlterations: {menu: "BuildingAlterationsExternalMenu", ghost: false, highlightable: true},
    RES_Blinds: {menu: "BuildingAlterationsExternalMenu", ghost: false, highlightable: true},
    RES_MinorInternalBuildingAlterations: {menu: "BuildingAlterationsInternalMenu", ghost: false, highlightable: true},
    RES_Bathroom: {menu: "BuildingAlterationsInternalMenu", ghost: false, highlightable: true},
    RES_BathroomAlterations: {menu: "BuildingAlterationsInternalMenu", ghost: false, highlightable: true},
    RES_Bedroom: {menu: "BuildingAlterationsInternalMenu", ghost: false, highlightable: false},
    RES_BedroomAlterations: {menu: "BuildingAlterationsInternalMenu", ghost: false, highlightable: true},
    RES_Kitchen: {menu: "BuildingAlterationsInternalMenu", ghost: false, highlightable: false},
    RES_KitchenAlterations: {menu: "BuildingAlterationsInternalMenu", ghost: false, highlightable: true},
    RES_Room: {menu: "BuildingAlterationsInternalMenu", ghost: false, highlightable: false},
    RES_Hall: {menu: "BuildingAlterationsInternalMenu", ghost: false, highlightable: false},
    RES_InternalDoor: {menu: "BuildingAlterationsInternalMenu", ghost: false, highlightable: false},
    RES_InternalShelving: {menu: "BuildingAlterationsInternalMenu", ghost: false, highlightable: false},
    RES_InternalStair: {menu: "BuildingAlterationsInternalMenu", ghost: false, highlightable: true},
    RES_AccessRamps: {menu: "CarportsAccessMenu", ghost: true, highlightable: true},
    RES_Carport: {menu: "CarportsAccessMenu", ghost: false, highlightable: true},
    RES_PathwaysPaving: {menu: "CarportsAccessMenu", ghost: false, highlightable: true},
    RES_Driveways: {menu: "CarportsAccessMenu", ghost: false, highlightable: true},
    RES_Hardstanding: {menu: "CarportsAccessMenu", ghost: false, highlightable: true},
    RES_FencesResidential: {menu: "FencesWallsMenu", ghost: true, highlightable: true},
    RES_FencesRural: {menu: "FencesWallsMenu", ghost: true, highlightable: true},
    RES_RainWaterTanks: {menu: "OutdoorItemsMenu", ghost: true, highlightable: true},
    RES_PortableSwimmingPools: {menu: "OutdoorItemsMenu", ghost: true, highlightable: true},
    RES_PlaygroundEquipment: {menu: "OutdoorItemsMenu", ghost: true, highlightable: true},
    RES_WaterPonds: {menu: "OutdoorItemsMenu", ghost: true, highlightable: true},
    RES_LandscapingStructures: {menu: "OutdoorItemsMenu", ghost: true, highlightable: true},
    RES_Flagpoles: {menu: "OutdoorItemsMenu", ghost: true, highlightable: true},
    RES_ClothesHoistsLines: {menu: "OutdoorItemsMenu", ghost: true, highlightable: true},
    RES_Barbecues: {menu: "OutdoorItemsMenu", ghost: true, highlightable: true},
    RES_PrivacyScreens1: {menu: "OutdoorItemsMenu", ghost: true, highlightable: true},
    RES_PrivacyScreens2: {menu: "OutdoorItemsMenu", ghost: true, highlightable: true},
    RES_AntennaGround: {menu: "OutdoorItemsMenu", ghost: true, highlightable: true},
    RES_AntennaDishGround: {menu: "OutdoorItemsMenu", ghost: true, highlightable: true},
    RES_AirConditioningUnitsGround: {menu: "OutdoorItemsMenu", ghost: true, highlightable: true},
    RES_SculpturesArtworks: {menu: "OutdoorItemsMenu", ghost: true, highlightable: true},
    RES_PatiosTerraces: {menu: "OutdoorItemsMenu", ghost: true, highlightable: true},
    RES_Letterboxes: {menu: "OutdoorItemsMenu", ghost: true, highlightable: true},
    RES_SolarEnergySystem1: {menu: "RoofMenu", ghost: true, highlightable: true},
    RES_SolarEnergySystem2: {menu: "RoofMenu", ghost: true, highlightable: true},
    RES_Skylights: {menu: "RoofMenu", ghost: true, highlightable: true},
    RES_EvaporativeCoolingUnits: {menu: "RoofMenu", ghost: true, highlightable: true},
    RES_AntennaDish: {menu: "RoofMenu", ghost: true, highlightable: true},
    RES_AntennaMast: {menu: "RoofMenu", ghost: true, highlightable: true},
    RES_Aviaries: {menu: "ShedsMenu", ghost: true, highlightable: true},
    RES_GardenShed: {menu: "ShedsMenu", ghost: true, highlightable: true},
    RES_AnimalShelters: {menu: "SheadsMenu", ghost: true, highlightable: true},
    RES_CubbyHouse: {menu: "ShedsMenu", ghost: true, highlightable: true},
    RES_ShadeStructures: {menu: "ShedsMenu", ghost: true, highlightable: true},
    RES_Pergola: {menu: "ShedsMenu", ghost: true, highlightable: true},
    RES_CabanaGazebo: {menu: "ShedsMenu", ghost: true, highlightable: true},
    RES_GarbageBinStorage: {menu: "ShedsMenu", ghost: true, highlightable: true},
    RES_GreenhouseFernery: {menu: "ShedsMenu", ghost: true, highlightable: true},
    RES_BuildingIdentificationSigns: {menu: "SignsMenu", ghost: true, highlightable: true},
    RES_WallSigns: {menu: "SignsMenu", ghost: true, highlightable: true},
    RES_RealEstateSigns: {menu: "SignsMenu", ghost: true, highlightable: true},
    RES_ElectionSigns: {menu: "SignsMenu", ghost: true, highlightable: true},
    RES_TemporaryBuildersStructures: {menu: "MaintenanceMenu", ghost: true, highlightable: true},
    RES_Scaffolding: {menu: "MaintenanceMenu", ghost: true, highlightable: true},
    RES_SiteFencesHoardings: {menu: "MaintenanceMenu", ghost: true, highlightable: true},
    RES_ChangeOfUse: {menu: "ChangeOfUseMenu", ghost: true, highlightable: true},
  };


  var scene, renderer, camera, light1, light2, light3, light4, loader, projector, raycaster;
  var mouse = new THREE.Vector2(), INTERSECTED;

  var obj, vector, intersects;
  var currentAnimation = "";
  var currentFocus = "default";
  var numObjects = 70;   //The total number of objects
  var loaded = 0;
  var objArray = [];
  var toggleableArray = [];
  var unToggleableArray = [];
  var objectColour = {
    body: "0x99a1a3",
    outline: "0x4D4D4D",
    highlight: "0x009be4",
    hover: "0x4D4D4D",
    grass: "0x80BB80"
  };
  var webgl = false;
  var canvas = null;
  var offsetX, offsetY;
  var renderDepth = 0;
  var hotspotObj = null;
  var tabRequireOpen = true;
  var tabRestrictOpen = false;
  var houseInternal = false;


  function threeStart()
  {
    initScene();
    initCamera();
    initSky();
    initLight();
    initObjects();
    detectUserDevice();
    render();
  }


  function initScene()
  {
    scene = new THREE.Scene();
    canvas = document.createElement('canvas');
    canvas.id = "CanvasID";
    if (Detector.webgl)
    {
      renderer = new THREE.WebGLRenderer({canvas: canvas, antialias: true});
      webgl = true;
    } else
    {
      renderer = new THREE.CanvasRenderer({canvas: canvas});
      webgl = false;
    }
    renderer.setSize(window.innerWidth, window.innerHeight);
    renderer.setClearColor(0xFFFFFF, 1, 0);
    document.getElementById("canvas_container").appendChild(renderer.domElement);
    $("#canvas_container").css({"-webkit-tap-highlight-color": "rgba(0, 0, 0, 0)", "-webkit-touch-callout": "none"});
  }


  function initCamera()
  {
    camera = new THREE.PerspectiveCamera(55, window.innerWidth / window.innerHeight, 1, 4500);
    camera.position.set(currentCamLoc.posX, currentCamLoc.posY, currentCamLoc.posZ);
    camera.up.x = 0;
    camera.up.y = 1;
    camera.up.z = 0;

    camera.lookAt({x: currentCamLoc.lookX, y: currentCamLoc.lookY, z: currentCamLoc.lookZ});
    scene.add(camera);
  }


  function initSky()
  {
    if (webgl)
    {
      scene.fog = new THREE.Fog(0xffffff, 1, 5000);
      var vertexShader = "varying vec3 vWorldPosition; void main() { vec4 worldPosition = modelMatrix * vec4( position, 1.0 ); vWorldPosition = worldPosition.xyz; gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 ); }";
      var fragmentShader = "uniform vec3 topColor; uniform vec3 bottomColor; uniform float offset; uniform float exponent; varying vec3 vWorldPosition; void main() { float h = normalize( vWorldPosition + offset ).y; gl_FragColor = vec4( mix( bottomColor, topColor, max( pow( h, exponent ), 0.0 ) ), 1.0 );}";
      var uniforms = {
        topColor: {type: "c", value: new THREE.Color(0x0077ff)},
        bottomColor: {type: "c", value: new THREE.Color(0xffffff)},
        offset: {type: "f", value: 1},
        exponent: {type: "f", value: 0.6}
      };
      scene.fog.color.copy(uniforms.bottomColor.value);
      var skyGeo = new THREE.SphereGeometry(750, 32, 15);
      var skyMat = new THREE.ShaderMaterial({vertexShader: vertexShader, fragmentShader: fragmentShader, uniforms: uniforms, side: THREE.BackSide});
      var skybox = new THREE.Mesh(skyGeo, skyMat);


      var groundGeo = new THREE.PlaneGeometry(10000, 10000);
      var groundMat = new THREE.MeshPhongMaterial({ambient: 0xffffff, color: 0x80BB80, specular: 0x050505});
      var ground = new THREE.Mesh(groundGeo, groundMat);
      ground.rotation.x = -Math.PI / 2;
      ground.position.y = -1;
      scene.add(skybox);
      scene.add(ground);
    }
  }


  function initLight()
  {
    if (webgl)
    {
      light1 = new THREE.HemisphereLight(0xFFFFFF, 1, 1.4);
      light1.position.set(303, 127, 142.5);
      light2 = new THREE.PointLight(0xFFFFFF, 1, 0);
      light2.position.set(-267.58, 80.82, -150.86);
      light3 = new THREE.PointLight(0xFFFFFF, 1, 300);
      light3.position.set(190.67, 69.69, -228.23);
      scene.add(light1);
      scene.add(light2);
      scene.add(light3);
    } else
    {
      light1 = new THREE.DirectionalLight(0xFFFFFF, 1.1, 0);
      light1.position.set(200, 178, -265);
      light2 = new THREE.DirectionalLight(0xFFFFFF, 0.9, 0);
      light2.position.set(172, 173, 280);
      light3 = new THREE.DirectionalLight(0xFFFFFF, 0.8, 0);
      light3.position.set(-494, 142, 30);
      light4 = new THREE.PointLight(0xFFFFFF, 0.2, 0);
      light4.position.set(291, 39, -24);
      scene.add(light1);
      scene.add(light2);
      scene.add(light3);
      scene.add(light4);
    }
  }


  /*
   loadObjects( ObjectName, ObjectGroup, Hotspot, x, y, z );

   ObjectName = Should match filename. (e.g if json file is 'house.js', the objectName should be 'house')
   ObjectGroup = Should match ID on navigation link. For grouping models together (e.g. Air conditioning units consists of two models).
   Hotspot = Should match ID on navigation link. For grouping hotspots together (e.g. Carport hotspot consists of both carport and drivewayHardStandSpaces models).
   */
  function initObjects()
  {
    if (webgl)
    {
      loadObjects("RES_ShadeStructures", "none", "none", -26, 0.5, -1);
      loadObjects("RES_Aviaries", "none", "none", -85, 0.5, 20);
      loadObjects("RES_FencesResidential", "none", "FencesWallsMenu", 0, 0, 0);
      loadObjects("RES_BalconiesDecks", "none", "none", 0, 0.5, -5.6);
      loadObjects("RES_ScreenEnclosures", "none", "none", 132.42, 0.5, -5.9);
      loadObjects("RES_HouseWalls", "none", "BuildingAlterationsExternalMenu", 84.5, 0.5, -33.51);
      loadObjects("RES_Roof", "none", "RoofMenu", 84.44, 0, -31.04);
      loadObjects("RES_Carport", "none", "CarportsAccessMenu", 0, 0, 0);
      loadObjects("RES_AirConditioningUnitsWall", "none", "none", 27.34, 2.18, -65.13);
      loadObjects("RES_AirConditioningUnitsGround", "none", "none", 45.33, 0.5, 23.63);
      loadObjects("RES_GardenShed", "none", "ShedsMenu", -127, 0.5, -72);
      loadObjects("RES_AnimalShelters", "none", "none", 15, 0.5, -70);
      loadObjects("RES_AccessRamps", "none", "none", 0.34, 1, 0.27);
      loadObjects("RES_CubbyHouse", "none", "none", -83, 1, -70);
      loadObjects("RES_FencesRural", "none", "none", 0, 0, 0);
      loadObjects("RES_EarthworksRetainingWalls", "none", "none", 0, 0.5, 0);
      loadObjects("RES_SolarEnergySystem1", "RES_SolarEnergySystems", "none", 40, 34, -8.64);
      loadObjects("RES_SolarEnergySystem2", "RES_SolarEnergySystems", "none", 40, 34, 5.93);
      loadObjects("RES_RainwaterTanks", "none", "none", 24.04, 0.5, -79.72);
      loadObjects("RES_PortableSwimmingPools", "none", "none", -23, 0.5, 10);
      loadObjects("RES_PlaygroundEquipment", "none", "none", -48, 0.5, -73);
      loadObjects("RES_WaterPonds", "none", "none", -48.26, 0.5, -44);
      loadObjects("RES_PathwaysPaving", "none", "none", 0, 0, 0);
      loadObjects("RES_Letterboxes", "none", "none", 189.9, 2.83, -6.6);
      loadObjects("RES_LandscapingStructures", "none", "none", -20, 0.5, -16);
      loadObjects("RES_GarbageBinStorage", "none", "none", 83, 0.5, 97);
      loadObjects("RES_Blinds", "none", "none", 0, 0, 0);
      loadObjects("RES_Flagpoles", "none", "none", 163, 0.5, -59);
      loadObjects("RES_ClothesHoistsLines", "none", "none", -55.8, 0.5, 0);
      loadObjects("RES_Barbecues", "none", "none", 23, 0, -60);
      loadObjects("RES_Pergola", "none", "none", -1, 0, 1);
      loadObjects("RES_CabanaGazebo", "none", "none", -42, 0, -50);
      loadObjects("RES_Skylights", "none", "none", 45, 36, -62.07);
      loadObjects("RES_EvaporativeCoolingUnits", "none", "none", 45, 37.26, -45.15);
      loadObjects("RES_AntennaDish", "RES_AerialsAntennaeRoof", "none", 34.66, 28.6, 17.22);
      loadObjects("RES_AntennaMast", "RES_AerialsAntennaeRoof", "none", 64.06, 41.03, 17.26);
      loadObjects("RES_AntennaGround", "RES_AerialsAntennaeGround", "none", -73, 0.5, -17);
      loadObjects("RES_AntennaDishGround", "RES_AerialsAntennaeGround", "none", -73, 0.5, 0);
      loadObjects("RES_PrivacyScreens1", "RES_PrivacyScreens", "none", -23, 0.5, 43);
      loadObjects("RES_PrivacyScreens2", "RES_PrivacyScreens", "none", -26, 0.5, -25);
      loadObjects("RES_Scaffolding", "none", "none", 0, 0.5, 0);
      loadObjects("RES_SiteFencesHoardings", "none", "none", 0, 0.5, 0);
      loadObjects("RES_GreenhouseFernery", "none", "none", -110, 0.5, -24);
      loadObjects("RES_PatiosTerraces", "none", "none", 0, 0.5, 20);
      loadObjects("RES_Verandahs", "none", "none", 132.42, 0.5, -5.9);
      loadObjects("RES_AwningsCanopies", "none", "none", 0, 0, 0);
      loadObjects("RES_BuildingIdentificationSigns", "none", "none", 0, 0, 0);
      loadObjects("RES_WallSigns", "none", "none", 0, 0, 0);
      loadObjects("RES_MinorExternalBuildingAlterations", "none", "none", 0, 0.5, 0);
      loadObjects("RES_MinorInternalBuildingAlterations", "RES_MinorInternalBuildingAlterations", "none", -2, 0, 0);
      loadObjects("RES_BathroomAlterations", "RES_MIBA_Bathroom", "none", 0, 0, 0);
      loadObjects("RES_Bedroom", "RES_MIBA_Bedroom", "none", 0, 0, 0);
      loadObjects("RES_BedroomAlterations", "RES_MIBA_Bedroom", "none", 0, 0, 0);
      loadObjects("RES_Hall", "RES_MIBA_Stairs", "none", 0, 0, 0);
      loadObjects("RES_InternalDoor", "RES_MIBA_Stairs", "none", 0, 0, 0);
      loadObjects("RES_InternalShelving", "RES_MIBA_Stairs", "none", 0, 0, 0);
      loadObjects("RES_InternalStair", "RES_MIBA_Stairs", "none", 0, 0, 0);
      loadObjects("RES_Kitchen", "RES_MIBA_Kitchen", "none", 0, 0, 0);
      loadObjects("RES_KitchenAlterations", "RES_MIBA_Kitchen", "none", 0, 0, 0);
      loadObjects("RES_Room", "none", "none", 0, 0.01, 0);
      loadObjects("RES_HotWaterSystems", "none", "none", 39, 0.5, -94);
      loadObjects("RES_RealEstateSigns", "none", "signsMenu", 0, 0, 0);
      loadObjects("RES_ElectionSigns", "none", "none", 0, 0, 0);
      loadObjects("RES_SculpturesArtworks", "none", "none", -19, 0.5, 17);
      loadObjects("RES_BackyardGround", "none", "OutdoorItemsMenu", 0, 0, 0);
      loadObjects("RES_DoorsWindows", "none", "BuildingAlterationsInternalMenu", 84.6, 0.5, -33.62);
      loadObjects("RES_TemporaryBuildersStructures", "none", "none", -48, 0.5, -110);
      loadObjects("RES_Driveways", "none", "CarportsAccessMenu", 0, 0, 0);
      loadObjects("RES_Hardstanding", "none", "CarportsAccessMenu", 0, 0, 0);
      loadObjects("RES_ResidentialGround", "none", "none", 0, 0, 0);
    } else
    {
      loadObjects("RES_Roof", "none", "RoofMenu", 86.44, 0, -33.04);
      loadObjects("RES_Carport", "none", "CarportsAccessMenu", 0, 0.5, 0);
      loadObjects("RES_ResidentialGroundCanvas", "none", "none", 0, 0, 0);
      loadObjects("RES_Aviaries", "none", "none", -85, 5, 20);
      loadObjects("RES_AirConditioningUnitsWall", "none", "none", 22, 2.18, -65.13);
      loadObjects("RES_AirConditioningUnitsGround", "none", "none", 43, 0.5, 23.63);
      loadObjects("RES_GardenShed", "none", "ShedsMenu", -127, 3, -72);
      loadObjects("RES_AnimalShelters", "none", "none", 15, 2, -70);
      loadObjects("RES_AccessRamps", "none", "none", 0, 0.5, 0.27);
      loadObjects("RES_CubbyHouse", "none", "none", -83, 4, -70);
      loadObjects("RES_FencesRural", "none", "none", 0, 0, 0);
      loadObjects("RES_EarthworksRetainingWalls", "none", "none", 20, 6, 0);
      loadObjects("RES_SolarEnergySystem1", "RES_SolarEnergySystems", "none", 40, 37, -8.64);
      loadObjects("RES_SolarEnergySystem2", "RES_SolarEnergySystems", "none", 40, 37, 5.93);
      loadObjects("RES_ShadeStructures", "none", "none", -26, 0.5, -1);
      loadObjects("RES_RainwaterTanks", "none", "none", 24.04, 0.5, -79.72);
      loadObjects("RES_PortableSwimmingPools", "none", "none", -23, 3, 10);
      loadObjects("RES_PlaygroundEquipment", "none", "none", -48, 1, -73);
      loadObjects("RES_WaterPonds", "none", "none", -48.26, 2.2, -44);
      loadObjects("RES_PathwaysPaving", "none", "none", 0, 0, 0);
      loadObjects("RES_Letterboxes", "none", "none", 190, 3, -6.7);
      loadObjects("RES_LandscapingStructures", "none", "none", -20, 3, -16);
      loadObjects("RES_GarbageBinStorage", "none", "none", 83, 0.5, 97);
      loadObjects("RES_Blinds", "none", "none", 0, 0, 0);
      loadObjects("RES_Flagpoles", "none", "none", 163, 0.5, -59);
      loadObjects("RES_ClothesHoistsLines", "none", "none", -55.8, 4.3, 0);
      loadObjects("RES_Barbecues", "none", "none", 23, 0.5, -60);
      loadObjects("RES_Pergola", "none", "none", -3, 0, 1);
      loadObjects("RES_CabanaGazebo", "none", "none", -42, 0.5, -50);
      loadObjects("RES_Skylights", "none", "none", 40, 36, -62.07);
      loadObjects("RES_EvaporativeCoolingUnits", "none", "none", 42, 37.26, -45.15);
      loadObjects("RES_AntennaDish", "RES_AerialsAntennaeRoof", "none", 34.66, 28.6, 17.22);
      loadObjects("RES_AntennaMast", "RES_AerialsAntennaeRoof", "none", 64.06, 41.03, 17.26);
      loadObjects("RES_AntennaGround", "RES_AerialsAntennaeGround", "none", -73, 0.5, -17);
      loadObjects("RES_AntennaDishGround", "RES_AerialsAntennaeGround", "none", -73, 3, 0);
      loadObjects("RES_ScreenEnclosures", "none", "none", 132.42, 0.5, -5.9);
      loadObjects("RES_PrivacyScreens1", "RES_PrivacyScreens", "none", -23, 0.5, 43);
      loadObjects("RES_PrivacyScreens2", "RES_PrivacyScreens", "none", -26, 0.5, -25);
      loadObjects("RES_Scaffolding", "none", "none", 0, 0.5, 6.5);
      loadObjects("RES_SiteFencesHoardings", "none", "none", 0, 0.5, 0);
      loadObjects("RES_GreenhouseFernery", "none", "none", -110, 2, -24);
      loadObjects("RES_PatiosTerraces", "none", "none", 0, 2, 20);
      loadObjects("RES_Verandahs", "none", "none", 132.42, 0.5, -5.9);
      loadObjects("RES_BalconiesDecks", "none", "none", 0, 2, -5.6);
      loadObjects("RES_AwningsCanopies", "none", "none", 0, 0, 0);
      loadObjects("RES_BuildingIdentificationSigns", "none", "none", 2, 0, 0);
      loadObjects("RES_WallSigns", "none", "none", 1.8, 0, 0);
      loadObjects("RES_MinorExternalBuildingAlterations", "none", "none", 3.5, 0.5, 0);
      loadObjects("RES_MinorInternalBuildingAlterations", "RES_MinorInternalBuildingAlterations", "none", -7, 0.5, 1);
      loadObjects("RES_BathroomAlterations", "RES_MIBA_Bathroom", "none", 0, 0, 0);
      loadObjects("RES_Bedroom", "RES_MIBA_Bedroom", "none", 0, 0, 0);
      loadObjects("RES_BedroomAlterations", "RES_MIBA_Bedroom", "none", 0, 0, 0);
      loadObjects("RES_Hall", "RES_MIBA_Stairs", "none", 0, 0, 0);
      loadObjects("RES_InternalDoor", "RES_MIBA_Stairs", "none", 0, 0, 0);
      loadObjects("RES_InternalShelving", "RES_MIBA_Stairs", "none", 0, 0, 0);
      loadObjects("RES_InternalStair", "RES_MIBA_Stairs", "none", 0, 0, 0);
      loadObjects("RES_Kitchen", "RES_MIBA_Kitchen", "none", 0, 0, 0);
      loadObjects("RES_KitchenAlterations", "RES_MIBA_Kitchen", "none", 0, 0, 0);
      loadObjects("RES_Room", "none", "none", 0, 0.01, 0);
      loadObjects("RES_HotWaterSystems", "none", "none", 39, 0.5, -94);
      loadObjects("RES_RealEstateSigns", "none", "SignsMenu", 0, 0, 0);
      loadObjects("RES_ElectionSigns", "none", "none", 0, 0, 0);
      loadObjects("RES_SculpturesArtworks", "none", "none", -19, 2, 17);
      loadObjects("RES_BackyardGround", "none", "OutdoorItemsMenu", -2, -2, 0);
      loadObjects("RES_Driveways", "none", "CarportsAccessMenu", 0, 0, 0);
      loadObjects("RES_Hardstanding", "none", "CarportsAccessMenu", 0, 0, 0);
      loadObjects("RES_DoorsWindows", "none", "BuildingAlterationsInternalMenu", 86, 0.5, -33.62);
      loadObjects("RES_HouseWalls", "none", "BuildingAlterationsExternalMenu", 86, 0, -33.6);
      loadObjects("RES_TemporaryBuildersStructures", "none", "none", -48, 0.5, -110);
      loadObjects("RES_FencesResidential", "none", "FencesWallsMenu", 0, 0, 0);
    }
  }


  function loadObjects(objectName, objectGroup, hotspotGroup, x, y, z)
  {
    renderDepth += 5;
    var thisRenderDepth = renderDepth;

    loader = new THREE.JSONLoader();
    loader.load(base_path + "/json/residential/" + objectName + ".js", function load_Obj(geometry, materials)
    {
      var material = new THREE.MeshFaceMaterial(materials);

      var obj = new THREE.Mesh(geometry, material);
      obj.name = objectName;
      obj.scale.set(1, 1, 1);
      if ((!webgl) && (obj.name == "RES_DoorsWindows"))
      {
        obj.scale.set(1.04, 1, 1);
      } else if ((webgl) && (obj.name == "RES_DoorsWindows"))
      {
        obj.scale.set(1.01, 1, 1);
      }

      if (obj.name == "RES_GardenShed")
      {
        obj.scale.set(1.5, 1.5, 1.5);
      }

      switch (obj.name)
      {
        case "RES_TemporaryBuildersStructures":
        case "RES_Aviaries":
          obj.rotation.set(3.14, 1.57, 3.14);
          break;
        case "RES_HotWaterSystems":
          obj.rotation.set(0, 3.14, 0);
          break;
        case "RES_AnimalShelters":
          obj.rotation.set(3.14, 0, 3.14);
          break;
        case "RES_Barbecues":
          obj.rotation.set(0, 4.71, 0);
          break;
      }


      obj.position.set(x, y, z);
      obj.group = (objectGroup == "none") ? objectName : objectGroup;
      obj.hotspot = hotspotGroup;
      obj.textureLock = false;
      obj.visible = false;
      obj.renderDepth = thisRenderDepth;

      for (var i = 0; i < obj.material.materials.length; i++) {
        //Set material color
        if (obj.material.materials[i].name == "outline") {
          obj.material.materials[i].color.setHex(objectColour['outline']);
        } else if (obj.material.materials[i].name == "body") {
          obj.material.materials[i].color.setHex(objectColour['body']);
        } else if (obj.material.materials[i].name == "grass") {
          obj.material.materials[i].color.setHex(objectColour['grass']);
        }
        obj.material.materials[i].overdraw = 1;

        //Set default opacity
        obj.material.materials[i].defaultOpacity = obj.material.materials[i].opacity;
      }

      //set menu name - the menu that the object belongs to
      $.each(objInfo, function (key, info) {
        if (key.toLowerCase() == objectName.toLowerCase()) {
          obj.menu = info.menu;
          obj.ghost = info.ghost;
          obj.highlightable = info.highlightable;
        }
      });

      if (obj.menu == undefined) {
        obj.menu = "none";
      }

      scene.add(obj);

      objArray.push(obj);
      loaded++;

      if (loaded == numObjects) {
        toggleableArray = objArray.slice(0);

        if (webgl)
        {
          setAlwaysVisible('RES_ResidentialGround');
        } else
        {
          setAlwaysVisible('RES_ResidentialGroundCanvas');
        }
        setAlwaysVisible('RES_HouseWalls');
        setAlwaysVisible('RES_Carport');
        setAlwaysVisible('RES_Driveways');
        setAlwaysVisible('RES_Hardstanding');
        setAlwaysVisible('RES_GardenShed');
        setAlwaysVisible('RES_BackyardGround');
        setAlwaysVisible('RES_Roof');
        setAlwaysVisible('RES_PathwaysPaving');
        setAlwaysVisible('RES_DoorsWindows');
        setAlwaysVisible('RES_FencesResidential');
        setAlwaysVisible('RES_Letterboxes');
        setAlwaysVisible('RES_RealEstateSigns');
        setAlwaysVisible('RES_Room');

        // BANSW0006-142: If "nid" field is present in query string then select object by node ID
        var selectedObj = getValueFromQueryString("nid");
        if (selectedObj != null && selectedObj != undefined) {
          $('.view-alteration-object').find('.moveLink[data-altobj="' + selectedObj + '"]').click().parent('li').parent('ul').addClass('active').prev('h3').addClass('active');
        }
      }

      switch (obj.name)
      {
        case "RES_PortableSwimmingPools":
        case "RES_FencesResidential":
        case "RES_PathwaysPaving":
        case "RES_HouseWalls":
        case "RES_ResidentialInterior":
        case "RES_Roof":
        case "RES_SiteFencesHoardings":

        for (var i = 0; i < obj.material.materials.length; i++) {
          obj.material.materials[i].side = 2;
        }
      }
    });
  }

  projector = new THREE.Projector();
  raycaster = new THREE.Raycaster();


  function setAlwaysVisible(objectName)
  {
    toggleObject(objectName, true);
    for (var i = 0; i < toggleableArray.length; i++)
    {
      if (objectName.toLowerCase() == toggleableArray[i].name.toLowerCase())
      {
        unToggleableArray.push(toggleableArray[i]);
        toggleableArray.splice(i, 1);
      }
    }
  }


  function onDocumentMouseDown(event)
  {
    calcOffsets();

    mouse.x = ((event.clientX - offsetX) / width) * 2 - 1;
    mouse.y = -((event.clientY - offsetY) / (width / (16 / 9))) * 2 + 1;

    vector = new THREE.Vector3(mouse.x, mouse.y, 1);
    projector.unprojectVector(vector, camera);

    raycaster.set(camera.position, vector.sub(camera.position).normalize());

    intersects = raycaster.intersectObjects(scene.children);

    if (intersects.length > 0)
    {
      for (var i = 0; intersects.length > i; i++)
      {
        if (currentAnimation == "" && intersects[ i ].object.hotspot != "none" && intersects[i].object.hotspot != undefined) {

          //Functions triggered by hotspot start from here, open the menu and close the content panel
          $("#object_content_section").animate({height: 'hide'}, 'fast');
          $('.object-menu').animate({width: 'show'}, 'fast');
          $(".object-menu-wrapper .menu-link").animate({left: "0"}, 'fast');

          //When hotspot is clicked, the original menu should be active
          $('.view-filters').animate({width: 'show'}, 'fast');
          $('.object-menu .view-alteration-object .view-content').show();
          $('.object-menu .search-results-menu .view-content').hide();

          //If the camera angle is inside the menu, set the hotspot as "BuildingAlterationsInternalMenu"
          if (houseInternal == true)
          {
            hotspotObj = "BuildingAlterationsInternalMenu";
          } else {
            hotspotObj = intersects[ i ].object.hotspot;
            hotspotObj = hotspotObj.charAt(0).toUpperCase() + hotspotObj.slice(1);
          }

          //If correct menu is already open don't close it
          if (!$(".view-alteration-object h3" + ' #' + hotspotObj).parent().hasClass('active')) {
            $('ul').removeClass('active');
            $(".view-alteration-object h3" + ' #' + hotspotObj).parent().click();
          }

          //Google analytics tracking code
          ga("send", "event", hotspotObj, "hotspot_click", this.href);
          break;
        }
      }
    } else
    {
      INTERSECTED = null;
    }
  }


  function onDocumentMouseMove(event)
  {
    if (currentAnimation == "") {

      calcOffsets();

      mouse.x = ((event.clientX - offsetX) / width) * 2 - 1;
      mouse.y = -((event.clientY - offsetY) / (width / (16 / 9))) * 2 + 1;

      var mouseCanPosX = event.clientX - offsetX;      //Mouse position X in canvas
      var mouseCanPosY = event.clientY - offsetY;      //Mouse position Y in canvas

      vector = new THREE.Vector3(mouse.x, mouse.y, 1);
      projector.unprojectVector(vector, camera);

      raycaster.set(camera.position, vector.sub(camera.position).normalize());

      intersects = raycaster.intersectObjects(scene.children);

      if (intersects.length > 0)
      {
        for (var i = 0; intersects.length > i; i++)
        {
          resetHighlight();
          if (intersects[i].object.visible && intersects[i].object.hotspot != "none" && intersects[i].object.hotspot != undefined)
          {
            highlightHotspotGroup(intersects[i].object.hotspot, "hover");

            if (houseInternal == true)
            {
              displayHotspotLabel("BuildingAlterationsInternalMenu", mouseCanPosX, mouseCanPosY);
            } else
            {
              displayHotspotLabel(intersects[i].object.hotspot, mouseCanPosX, mouseCanPosY);
            }

            break;
          } else
          {
            $('#popup_label_scene').hide();
          }
        }
      } else
      {
        resetHighlight();
        INTERSECTED = null;
      }
    }
  }
// hide labels when mouse leaves the 3D viewer or mouse enters one of the ui items
  $('#canvas_container').mouseleave(function () {
    $('#popup_label_scene').hide();
  });
  $(".menu-link, .object-menu, .reset-link, #object-wrapper").mouseover(function () {
    $('#popup_label_scene').hide();
  });

  function onDocumentTouchStart(event)
  {
    if (currentAnimation == "")
    {
      calcOffsets();

      mouse.x = ((event.touches[0].clientX - offsetX) / width) * 2 - 1;
      mouse.y = -((event.touches[0].clientY - offsetY) / (width / (16 / 9))) * 2 + 1;

      vector = new THREE.Vector3(mouse.x, mouse.y, 1);
      projector.unprojectVector(vector, camera);

      raycaster.set(camera.position, vector.sub(camera.position).normalize());

      intersects = raycaster.intersectObjects(scene.children);

      if (intersects.length > 0)
      {
        if (INTERSECTED != intersects[ 0 ].object)
        {
          for (var i = 0; intersects.length > i; i++)
          {
            resetHighlight();
            if (intersects[i].object.visible && intersects[i].object.hotspot != "none" && intersects[i].object.hotspot != undefined)
            {
              //Functions triggered by hotspot start from here, open the menu and close the content panel
              $("#object_content_section").animate({height: 'hide'}, 'fast');
              $('.object-menu').animate({width: 'show'}, 'fast');
              $(".object-menu-wrapper .menu-link").animate({left: "0"}, 'fast');

              //When hotspot is clicked, the original menu should be active
              $('.view-filters').animate({width: 'show'}, 'fast');
              $('.object-menu .view-alteration-object .view-content').show();
              $('.object-menu .search-results-menu .view-content').hide();

              $('ul').removeClass('active');
              hotspotObj = intersects[ i ].object.hotspot;
              hotspotObj = hotspotObj.charAt(0).toUpperCase() + hotspotObj.slice(1);

              //If correct menu is already open don't close it
              if (!$(".view-alteration-object h3" + ' #' + hotspotObj).parent().hasClass('active'))
              {
                $('ul').removeClass('active');
                $(".view-alteration-object h3" + ' #' + hotspotObj).parent().click();
              }

              $(".view-alteration-object h3").removeClass('active');
              $(".view-alteration-object h3" + ' #' + hotspotObj).parent().addClass('active');

              ga("send", "event", hotspotObj, "hotspot_click", this.href);
              break;
            } else
            {
              $('#popup_label_scene').hide();
            }
          }
        }
      } else
      {
        resetHighlight();
        INTERSECTED = null;
      }
    }
  }


  function detectUserDevice()
  {
    if ((Detector.webgl) || (navigator.appVersion.indexOf("MSIE 10") > 0) || (navigator.userAgent.indexOf("Safari") > 0))
    {
      if (navigator.userAgent.indexOf("iPad") < 0)
      {
        document.getElementById("CanvasID").addEventListener('mousemove', onDocumentMouseMove, false);
        document.getElementById("CanvasID").addEventListener('mousedown', onDocumentMouseDown, false);
      } else
      {
        document.getElementById("CanvasID").addEventListener('touchstart', onDocumentTouchStart, false);
      }
    } else
    {
      document.getElementById("CanvasID").addEventListener('touchstart', onDocumentTouchStart, false);
    }
  }


  var render = function ()
  {
    requestAnimationFrame(render);
    TWEEN.update();
    renderer.render(scene, camera);
  };


  //Display the hotspot name in a popup box when mouse is hovering on the hotspot object
  function displayHotspotLabel(hotspotName, mouseCanPosX, mouseCanPosY)
  {
    var objects = findHotspotGroup(hotspotName);      //hotspotName is fencesWallsMenu, roofMenu

    for (var i = 0; i < objects.length; i++)
    {
      if (hotspotName.toLowerCase() == objects[i].hotspot.toLowerCase())
      {
        $('#popup_label_scene span').removeClass("active");
        $('#popup_label_scene').show();
        $('#popup_label_scene #popup_box').show();
        hotspotName = hotspotName.charAt(0).toUpperCase() + hotspotName.slice(1) + "_Label";
        $('#popup_label_scene #' + hotspotName).addClass("active");

        var popup_box_half_width = ($("#popup_label_scene span").width()) / 2;
        var popup_box_height = $("#popup_label_scene #popup_box").height() + 22;

        mouseCanPosX = mouseCanPosX - popup_box_half_width;
        mouseCanPosY = mouseCanPosY - popup_box_height;

        $('#popup_label_scene #popup_box').css({"top": mouseCanPosY, "left": mouseCanPosX});

      }
    }
  }


  function highlightObjectGroup(objectName, highlight)
  {
    if (objectName == "RES_MIBA_Stairs") {
      findObject('RES_Room').visible = false;
    } else
    {
      findObject('RES_Room').visible = true;
    }
    var objects = findObjectGroup(objectName);
    for (var i = 0; i < objArray.length; i++)
    {
      for (var j = 0; j < objects.length; j++)
      {
        if (objArray[i].name.toLowerCase() == objects[j].name.toLowerCase() && objArray[i].highlightable == true)
        {
          if (!objects[j].textureLock)
          {
            setObjectMaterial(objects[j], "body", highlight);
          }
        }
      }
    }
  }


  function highlightHotspotGroup(hotspotName, highlight)
  {
    var objects = findHotspotGroup(hotspotName);
    for (var i = 0; i < objects.length; i++)
    {
      if (!objects[i].textureLock)
      {
        setObjectMaterial(objects[i], "grass", highlight);
        setObjectMaterial(objects[i], "body", highlight);
      }
    }
  }


  function resetOpacity()
  {
    for (var i = 0; i < objArray.length; i++)
    {
      for (var j = 0; j < objArray[i].material.materials.length; j++)
      {
        objArray[i].material.materials[j].opacity = objArray[i].material.materials[j].defaultOpacity;
      }
    }
  }


  function resetHighlight()
  {
    for (var i = 0; i < objArray.length; i++)
    {
      if (!objArray[i].textureLock)
      {
        setObjectMaterial(objArray[i], "grass", "grass");
        setObjectMaterial(objArray[i], "body", "body");
      }
    }
  }


  function setObjectMaterial(object, material, toMaterial)
  {
    for (var i = 0; i < object.material.materials.length; i++)
    {
      if (object.material.materials[i].name == material)
      {
        object.material.materials[i].color.setHex(objectColour[toMaterial]);
      }
    }
  }


  function makeObjectsInvisible()
  {
    for (var i = 0; i < unToggleableArray.length; i++)
    {
      unToggleableArray[i].visible = true;
    }
    for (var i = 0; i < toggleableArray.length; i++)
    {
      toggleableArray[i].visible = false;
    }
  }


  function removePopupContent()
  {
    $('#object-wrapper h1#page-title').hide();
    $('#object_content_section').animate({height: 'hide'}, 'fast');
  }


  function closeMenu()
  {
    if ($(".object-menu").is(':visible'))
    {
      // $(".object-menu-wrapper .menu-link").animate({left: "0"}, 'fast');
      // $(".object-menu").animate({width: 'hide'}, 'fast');
      $(".object-menu .search-results-menu .view-content").hide();
      $(".object-menu .view-alteration-object .view-content").show();
      $('.object-menu #edit-keyword').val("");
      $('.object-menu #edit-keyword').removeClass("x");
    } else
    {
      $(".object-menu-wrapper .menu-link").animate({left: "0"}, 'fast');
      $(".object-menu").animate({width: 'show'}, 'fast');
    }
  }


  function findObject(objectName)
  {
    var object;
    for (var i = 0; i < objArray.length; i++)
    {
      if (objectName.toLowerCase() == objArray[i].name.toLowerCase())
      {
        object = objArray[i];
      }
    }
    return object;
  }


  function findObjectGroup(objectGroup)
  {
    var objects = [];
    for (var i = 0; i < objArray.length; i++)
    {
      if (objectGroup.toLowerCase() == objArray[i].group.toLowerCase())
      {
        objects.push(objArray[i]);
      }
    }
    return objects;
  }


  function findHotspotGroup(hotspotGroup)
  {
    var objects = [];
    for (var i = 0; i < objArray.length; i++) {
      if (hotspotGroup.toLowerCase() == objArray[i].hotspot.toLowerCase()) {
        objects.push(objArray[i]);
      }
    }
    return objects;
  }


  function toggleObjectGroup(name, visibilty)
  {
    if (name == "RES_FencesRural") {
      findObject('RES_FencesResidential').visible = !visibilty;
      findObject('RES_Letterboxes').visible = !visibilty;
    }
    if (name == "RES_PortableSwimmingPools" || name == "RES_PrivacyScreens") {
      findObject('RES_PrivacyScreens1').visible = visibilty;
      findObject('RES_PrivacyScreens2').visible = visibilty;
      findObject('RES_PortableSwimmingPools').visible = visibilty;
    }
    if (name == "RES_SiteFencesHoardings" || name == "RES_Scaffolding") {
      findObject('RES_Scaffolding').visible = visibilty;
      findObject('RES_SiteFencesHoardings').visible = visibilty;
    }
    if (name == "RES_ScreenEnclosures") {
      findObject('RES_Verandahs').visible = visibilty;
    }
    for (var i = 0; i < objArray.length; i++) {
      if (name.toLowerCase() == objArray[i].group.toLowerCase()) {
        objArray[i].visible = visibilty;
      }
    }
  }


  function toggleObject(name, visibilty)
  {
    for (var i = 0; i < objArray.length; i++) {
      if (name.toLowerCase() == objArray[i].name.toLowerCase()) {
        objArray[i].visible = visibilty;
      }
    }
  }


  function ghostMenuObjectGroup(menuName)
  {
    for (var i = 0; i < objArray.length; i++) {
      if (menuName.toLowerCase() == objArray[i].menu.toLowerCase())
      {
        objArray[i].visible = true;
        if (objArray[i].ghost)
        {
          for (var j = 0; j < objArray[i].material.materials.length; j++) {
            objArray[i].material.materials[j].opacity = 0.5;
          }
        }
      }
    }
  }


  function resetTextureLock()
  {
    for (var i = 0; i < objArray.length; i++)
    {
      objArray[i].textureLock = false;
    }
  }


  function toggleTextureLock(objectGroup, toggle)
  {
    var objects = findObjectGroup(objectGroup);
    for (var i = 0; i < objects.length; i++) {
      objects[i].textureLock = toggle;
    }
  }


  //Calculate the start position of the canvas
  function calcOffsets()
  {
    var rect = canvas.getBoundingClientRect();
    offsetX = rect.left;
    offsetY = rect.top;
  }


  //Find the URL that the user is looking at, which is used for Google Analytics
  function findTabUrl(tabName)
  {
    var objName = $('.moveLink.selected').text();
    objName = objName.replace(/[^a-zA-Z0-9]/g, '').toLowerCase();
    var tabUrl = "/planning-residential/" + objName + "/" + tabName;
    return tabUrl;
  }


  //Find the URL (FAQ)  that the user is looking at, which is used for Google Analytics
  function findFaqUrl(faq)
  {
    var objName = $('.moveLink.selected').text();
    objName = objName.replace(/[^a-zA-Z0-9]/g, '').toLowerCase();
    faq = faq.replace(/[^a-zA-Z0-9]/g, '').toLowerCase();
    var faqUrl = "/planning-residential/" + objName + "/faq/" + faq;
    return faqUrl;
  }


  function getValueFromQueryString(key)
  {
    var param = location.search.match(new RegExp(key + "=(.*?)($|\&)", "i"));
    if (param == undefined) {
      return undefined;
    } else {
      return param[1];
    }
  }


  //Check whether the current camera view is inside house or outside house
  function isInsideHouse(objId)
  {
    if ($('.moveLink[data-attr="' + objId + '"]').parent().parent().prev('h3').find('#BuildingAlterationsInternalMenu').length > 0 || objId == "RES_InternalSigns")
    {
      houseInternal = true;
    } else
    {
      houseInternal = false;
    }
  }


  // FAQ dropdown
  $("#object-wrapper").on('click', 'h3.node-title', function () {

    $(this).parent().next($(".field-name-body")).toggle();
    $(this).toggleClass("active");

    if ($(this).hasClass('active'))
    {
      ga("send", "pageview", {page: findFaqUrl($(this).text())});
    }
  });


  //Get the URL when user clicks the requirements tab
  $('#object-wrapper').on('click', '#requirements-tab', function () {

    if (($(this).hasClass('ui-tabs-active')) && (tabRequireOpen == false))
    {
      tabRestrictOpen = false;
      tabRequireOpen = true;
      ga("send", "pageview", {page: findTabUrl("requirements")});
    }
  });


  //Get the URL when user clicks the restrictions tab
  $('#object-wrapper').on('click', '#restrictions-tab', function () {

    if (($(this).hasClass('ui-tabs-active')) && (tabRestrictOpen == false))
    {
      tabRestrictOpen = true;
      tabRequireOpen = false;
      ga("send", "pageview", {page: findTabUrl("restrictions")});
    }
  });


  // Click on "MENU" tab
  $(".object-menu-wrapper .menu-link").click(function () {
    // UPDATE BY DENIS
    //$("#object_content_section").animate({height: 'hide'}, 'fast');

    if ($(".object-menu").is(':visible'))
    {
      $(".object-menu").animate({width: 'hide'}, 'fast');
      $(".object-menu-wrapper .menu-link").animate({left: "0"}, 'fast');
    } else
    {
      $(".object-menu").animate({width: 'show'}, 'fast');
      $(".object-menu-wrapper .menu-link").animate({left: "0"}, 'fast');
    }
  });


  //Click on "Content Details Panel"
  $("#object-wrapper").on('click', ' h1', function () {

    if ($("#object_content_section").is(':visible'))
    {
      $("#object_content_section").animate({height: 'hide'}, 'fast');
    } else
    {
      $("#tabs").removeClass('ui-corner-all');
      $("#object_content_section").animate({height: 'show'}, {duration: 100, complete: function () {

          $("#tabs").addClass('ui-corner-all');
        }
      });
    }

    // Give class of active so we can change chevron icon when down
    $(this).toggleClass("active");

    //If "MENU" tab not hidden, close it and slide the menu label to the left
    if ($(".object-menu").is(':visible'))
    {
      $(".object-menu").animate({width: 'hide'}, 'fast');
      $(".object-menu-wrapper .menu-link").animate({left: "0"}, 'fast');
    }
  });

// Checks to see if a matching camera exists and returns the default camera if a matching one is not found
// Arg1: camera ID
// Arg2: object that contains all the camera locations
  function returnApporiateCameraId(cameraId, camLocObj) {
    if (camLocObj[cameraId] == null) {
      cameraId = 'DefaultView';
    }

    return camLocObj[cameraId];
  }

  $(document).ready(function () {

    $('.moveLink').click(function (e) {
      e.preventDefault();
      var id = $(this).attr('data-attr');
      var destination;

      if (webgl)
      {
        destination = returnApporiateCameraId(id, webglCamLoc);
      } else
      {
        destination = returnApporiateCameraId(id, canvasCamLoc);
      }

      if (!$(this).hasClass('selected')) {
        if (destination !== undefined) {
          currentAnimation = id;

          makeObjectsInvisible();
          removePopupContent();
          closeMenu();
          $('#popup_label_scene').hide();
          resetTextureLock();
          resetHighlight();
          resetOpacity();
          toggleObjectGroup(id, true);

          $(this).parents().find('.selected').removeClass('selected');
          $(this).addClass('selected');
          $('.' + id + '_content');
          TWEEN.removeAll();

          if (destination.group !== currentCamLoc.group) {
            // hide things when you click on reset view bouton.
            $('#page-subtitle-object-alteration').hide(); // hide the title of the node
            $('#see_report_in_title_btn').hide(); // hide the button : see reports
            $('#container_related_development_types').hide(); // hide the related dev types section
            tween1 = new TWEEN.Tween(currentCamLoc)
                    .to({posX: 0, posY: 500, posZ: 100, lookX: 0, lookY: 0, lookZ: 0}, 500)
                    .easing(TWEEN.Easing.Circular.InOut)
                    .onUpdate(function () {
                      camera.position.set(currentCamLoc.posX, currentCamLoc.posY, currentCamLoc.posZ);
                      camera.lookAt({x: currentCamLoc.lookX, y: currentCamLoc.lookY, z: currentCamLoc.lookZ});
                    });
            tweenWindow = new TWEEN.Tween(currentCamLoc)
                    .to({posX: 180, posY: 15, posZ: -60, lookX: 0, lookY: 15, lookZ: -60}, 500)
                    .easing(TWEEN.Easing.Circular.InOut)
                    .onUpdate(function () {
                      camera.position.set(currentCamLoc.posX, currentCamLoc.posY, currentCamLoc.posZ);
                      camera.lookAt({x: currentCamLoc.lookX, y: currentCamLoc.lookY, z: currentCamLoc.lookZ});
                    });
          }

          tween2 = new TWEEN.Tween(currentCamLoc)
                  .to({posX: destination.posX, posY: destination.posY, posZ: destination.posZ, lookX: destination.lookX, lookY: destination.lookY, lookZ: destination.lookZ}, 750)
                  .easing(TWEEN.Easing.Circular.InOut)
                  .onUpdate(function () {
                    camera.position.set(currentCamLoc.posX, currentCamLoc.posY, currentCamLoc.posZ);
                    camera.lookAt({x: currentCamLoc.lookX, y: currentCamLoc.lookY, z: currentCamLoc.lookZ});
                  })
                  .onComplete(function () {
                    if (id == currentAnimation) {
                      if (id !== "ResetView") {
                        $('#object_content_section').show();
                        highlightObjectGroup(id, "highlight");
                        toggleTextureLock(id, true);
                        isInsideHouse(id);
                        ga("send", "pageview", {page: findTabUrl("requirements")});
                      } else
                      {


                        removePopupContent();
                        $('.view-alteration-object .view-content').find('.active').removeClass('active');
                        ga("send", "event", "resetview", "button_click", this.href);

                        if (id != null) {
                          houseInternal = false;
                        } else {
                          houseInternal = true;
                        }
                      }
                      currentAnimation = "";
                      currentFocus = id.charAt(0).toLowerCase() + id.slice(1);
                    }
                  });

          if (destination.group !== currentCamLoc.group)
          {
            if (destination.group == 'F')
            {
              if (currentCamLoc.group == 'A')
              {
                tweenWindow.chain(tween2);
                tweenWindow.start();
              } else
              {
                tween1.chain(tweenWindow);
                tweenWindow.chain(tween2);
                tween1.start();
              }
            } else if (currentCamLoc.group == 'F')
            {
              if (destination.group == 'A')
              {
                tweenWindow.chain(tween2);
                tweenWindow.start();
              } else
              {
                tweenWindow.chain(tween1);
                tween1.chain(tween2);
                tweenWindow.start();
              }
            } else
            {
              tween1.chain(tween2);
              tween1.start();
            }
          } else
          {
            tween2.start();
          }

          currentCamLoc.group = destination.group;
        }
      } else
      {
        closeMenu();
        $('.view-alteration-object .view-content').find('.active').removeClass('active');
      }
    });


    $('.view-content h3').click(function (e) {
      e.preventDefault();

      //Active class is for showing the sub menu
      $('ul').not($(this).next('ul')).removeClass('active');
      $(this).next('ul').toggleClass('active');

      var id = $(this).children().not('.arrow').attr('id');
      var destination;

      if (webgl)
      {
        destination = webglCamLoc[id];
      } else
      {
        destination = canvasCamLoc[id];
      }

      //If this is not selected and not active then animate
      if (!$(this).hasClass('selected') && $(this).hasClass('active')) {
        if (destination !== undefined) {
          currentAnimation = id;

          makeObjectsInvisible();
          removePopupContent();
          $('#popup_label_scene').hide();
          resetTextureLock();
          resetHighlight();
          resetOpacity();
          ghostMenuObjectGroup(id);

          //Selected class tells us that the camera is currently focusing on this item
          $(this).parents().find('.selected').removeClass('selected');
          $(this).addClass('selected');
          TWEEN.removeAll();

          if (destination.group !== currentCamLoc.group) {
            tween1 = new TWEEN.Tween(currentCamLoc)
                    .to({posX: 0, posY: 500, posZ: 100, lookX: 0, lookY: 0, lookZ: 0}, 500)
                    .easing(TWEEN.Easing.Circular.InOut)
                    .onUpdate(function () {
                      camera.position.set(currentCamLoc.posX, currentCamLoc.posY, currentCamLoc.posZ);
                      camera.lookAt({x: currentCamLoc.lookX, y: currentCamLoc.lookY, z: currentCamLoc.lookZ});
                    });
            tweenWindow = new TWEEN.Tween(currentCamLoc)
                    .to({posX: 180, posY: 15, posZ: -60, lookX: 0, lookY: 15, lookZ: -60}, 500)
                    .easing(TWEEN.Easing.Circular.InOut)
                    .onUpdate(function () {
                      camera.position.set(currentCamLoc.posX, currentCamLoc.posY, currentCamLoc.posZ);
                      camera.lookAt({x: currentCamLoc.lookX, y: currentCamLoc.lookY, z: currentCamLoc.lookZ});
                    });
          }

          tween2 = new TWEEN.Tween(currentCamLoc)
                  .to({posX: destination.posX, posY: destination.posY, posZ: destination.posZ, lookX: destination.lookX, lookY: destination.lookY, lookZ: destination.lookZ}, 750)
                  .easing(TWEEN.Easing.Circular.InOut)
                  .onUpdate(function () {
                    camera.position.set(currentCamLoc.posX, currentCamLoc.posY, currentCamLoc.posZ);
                    camera.lookAt({x: currentCamLoc.lookX, y: currentCamLoc.lookY, z: currentCamLoc.lookZ});
                  })
                  .onComplete(function () {
                    if (id == currentAnimation) {
                      highlightObjectGroup(id, "highlight");
                      toggleTextureLock(id, true);
                      ga("send", "pageview", {page: findTabUrl("requirements")});
                      currentAnimation = "";
                      currentFocus = id.charAt(0).toLowerCase() + id.slice(1);

                      if (id != null) {
                        houseInternal = false;
                      } else {
                        houseInternal = true;
                      }
                    }
                  });

          if (destination.group !== currentCamLoc.group)
          {
            if (destination.group == 'F')
            {
              if (currentCamLoc.group == 'A')
              {
                tweenWindow.chain(tween2);
                tweenWindow.start();
              } else
              {
                tween1.chain(tweenWindow);
                tweenWindow.chain(tween2);
                tween1.start();
              }
            } else if (currentCamLoc.group == 'F')
            {
              if (destination.group == 'A')
              {
                tweenWindow.chain(tween2);
                tweenWindow.start();
              } else
              {
                tweenWindow.chain(tween1);
                tween1.chain(tween2);
                tweenWindow.start();
              }
            } else
            {
              tween1.chain(tween2);
              tween1.start();
            }
          } else
          {
            tween2.start();
          }

          currentCamLoc.group = destination.group;
        }
      }
    });



    /********* DEBUG CODE START ***********/
    /*  $('.middle_content').append('<span id="cameraTest"><style>#cameraTest input{width:50px;} #cameraTest{display:block; text-align: center;position:absolute; margin-top: 200px;width:100%;}</style><span>posX<input type="text" name="posX" value="0">posY<input type="text" name="posY" value="0">posZ<input type="text" name="posZ" value="0">lookX<input type="text" name="lookX" value="0">lookY<input type="text" name="lookY" value="0">lookZ<input type="text" name="lookZ" value="0"><button id="moveCam">update</button></span></span>');
     $('#moveCam').click(function(e){
     e.preventDefault();
     updateCam(500);
     });
     var increment = 1;
     function updateCam(time){
     tweenMove = new TWEEN.Tween(currentCamLoc)
     .to({posX: parseInt($('input[name="posX"]').val()), posY: parseInt($('input[name="posY"]').val()), posZ: parseInt($('input[name="posZ"]').val()), lookX: parseInt($('input[name="lookX"]').val()), lookY: parseInt($('input[name="lookY"]').val()), lookZ: parseInt($('input[name="lookZ"]').val())}, time)
     .easing(TWEEN.Easing.Circular.InOut)
     .onUpdate(function(){
     camera.position.set(currentCamLoc.posX,currentCamLoc.posY,currentCamLoc.posZ);
     camera.lookAt( {x:currentCamLoc.lookX, y:currentCamLoc.lookY, z:currentCamLoc.lookZ } );
     })
     tweenMove.start();
     }
     $('#cameraTest input').keydown(function(e){
     if(e.which == 13){
     $('#moveCam').click();
     }else if(e.which == 38){
     $(this).val( parseInt($(this).val()) + increment ) ;
     updateCam(1);
     }else if(e.which == 40){
     $(this).val( parseInt($(this).val()) - increment ) ;
     updateCam(1);
     }
     $(window).keydown(function(e) {
     if(e.which == 16){
     increment = 10;
     }
     });
     $(window).keyup(function(e) {
     if(e.which == 16){
     increment = 1;
     }
     });
     });
     if(webgl)
     {
     $('input[name="posX"]').val(webglCamLoc['ResetView'].posX);
     $('input[name="posY"]').val(webglCamLoc['ResetView'].posY);
     $('input[name="posZ"]').val(webglCamLoc['ResetView'].posZ);
     $('input[name="lookX"]').val(webglCamLoc['ResetView'].lookX);
     $('input[name="lookY"]').val(webglCamLoc['ResetView'].lookY);
     $('input[name="lookZ"]').val(webglCamLoc['ResetView'].lookZ);
     }
     else
     {
     $('input[name="posX"]').val(canvasCamLoc['ResetView'].posX);
     $('input[name="posY"]').val(canvasCamLoc['ResetView'].posY);
     $('input[name="posZ"]').val(canvasCamLoc['ResetView'].posZ);
     $('input[name="lookX"]').val(canvasCamLoc['ResetView'].lookX);
     $('input[name="lookY"]').val(canvasCamLoc['ResetView'].lookY);
     $('input[name="lookZ"]').val(canvasCamLoc['ResetView'].lookZ);
     }
     $('.moveLink').click(function(e){
     e.preventDefault();
     var id = $(this).attr('data-attr');
     if(webgl)
     {
     $('input[name="posX"]').val(webglCamLoc[id].posX);
     $('input[name="posY"]').val(webglCamLoc[id].posY);
     $('input[name="posZ"]').val(webglCamLoc[id].posZ);
     $('input[name="lookX"]').val(webglCamLoc[id].lookX);
     $('input[name="lookY"]').val(webglCamLoc[id].lookY);
     $('input[name="lookZ"]').val(webglCamLoc[id].lookZ);
     }
     else
     {
     $('input[name="posX"]').val(canvasCamLoc[id].posX);
     $('input[name="posY"]').val(canvasCamLoc[id].posY);
     $('input[name="posZ"]').val(canvasCamLoc[id].posZ);
     $('input[name="lookX"]').val(canvasCamLoc[id].lookX);
     $('input[name="lookY"]').val(canvasCamLoc[id].lookY);
     $('input[name="lookZ"]').val(canvasCamLoc[id].lookZ);
     }
     });    */
    /********* DEBUG CODE END ***********/



  });

})(jQuery);
