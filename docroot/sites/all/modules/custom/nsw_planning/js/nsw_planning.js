$(function($) {

		// Set Global base_path defined in hook_init()
	 	base_path = Drupal.settings.nsw_planning.baseUrl;


	/** Mobile detection **/


		//see https://github.com/kaimallea/isMobile for more information
	 	if(isMobile.phone) {
 			$(".is-mobile").show();
 			$(".is-desktop").detach();
		}

		if(! isMobile.phone) {
 			$(".is-desktop").show();
 			$(".is-mobile").detach();
		}


	/** IE detection **/


		function getIEVersion(){
		    var agent = navigator.userAgent;
		    var reg = /MSIE\s?(\d+)(?:\.(\d+))?/i;
		    var matches = agent.match(reg);
		    if (matches != null) {
		        return { major: matches[1], minor: matches[2] };
		    }
		    return { major: "-1", minor: "-1" };
		}

		var ie_version =  getIEVersion();

		if (ie_version.major == 9) {
			$('body').addClass('ie9');
			$('.hide-from-ie').hide();
			$('.ie-browser-message').show();
		}
		if (ie_version.major == 8) {
			$('body').addClass('ie8');
			$('.hide-from-ie').hide();
			$('.ie-browser-message').show();
		}
		if (ie_version.major == 7) {
			$('body').addClass('ie7');
			$('.hide-from-ie').hide();
			$('.ie-browser-message').show();
		}


	/** Model UI **/
		// NOTIFICATION SOLUTION
		//('html').append('<div id="notification-label" style="position:fixed; bottom:0; left: 50%; z-index:999; padding:10px; background:orange; display:none;" >See info below</div>');

		//Alteration Objects/Rules load content
		$('.view-alteration-object a').click(function(){

			// Upon selecting an item hide menu
			$(".object-menu").animate({width: 'toggle'}, 'fast');
			//$(".view-alteration-object .view-footer").addClass("float-left");

			// Set our default active tab
			$( "#tabs" ).tabs({ active: 0 });

			// TODO When we have an animated progress GIF place url here
			var ajax_load = "<img src='" + base_path + "/images/ajax-loader.gif' alt='loading...' class='animated-gif' />";

			// Save NID of custom attribute to get our path to content
			var nid = $(this).attr('data-altobj');
			var loadUrl = "/node/" + nid;

			// If we have a nid load the content into appropriate fields
			if(typeof nid != "undefined") {

				try {

					$("#object-wrapper")
						.html(ajax_load)
						.load(loadUrl + " #content" /* , Optional parameters */, function() {

							// Init jQuery UI Tabs Widget
							$("#tabs").tabs();

							// Add class to parent div if FAQ field exists
							if($("#alteration_faq_content").length) {
								$("#alteration_object_content").addClass('faq-move-over');
							}

							// Add class to parent div if FAQ field exists
							if($("#alteration_object_requirements_content").length) {
								$("#alteration_object_requirements_content").addClass('faq-move-over');
							}

							// SCROLL SOLUTION
							// Scroll to the top of the content
                  $('html, body').delay(1000).animate({
			                    scrollTop: $("#page-subtitle-object-alteration").offset().top
			                }, 1000);

							// NOTIFICATION SOLUTION
                  //        $('#notification-label').slideDown(500).delay('10000').slideUp(500);
						});

				}
				catch(e) {
					console.log(e);
				}
			}
			else {
				//TODO Gordon to clarify how this message is to be displayed
				alert("Content for this item not found");
			}

		});

		// Click on "MENU item"
		$(".view-alteration-object h3").click(function() {
			 // Click on menu item and display following "ul" as block
			 // Give "ul" list class dof active to style
			 $(this).next($("ul")).toggle();
			 $(this).toggleClass("active");
			 $(".view-alteration-object h3").not($(this)).removeClass('active');

			 var menu_item = $(this).text().replace(/[^a-zA-Z0-9]/g, '').toLowerCase();
			 if($(this).hasClass('active'))
			 {
			 	var develType = (window.location.pathname).substr(1);
			 	ga("send", "event", develType + "/" + menu_item, "menucategory_click", this.href);
			 }
		});

		// Provide a div to include inject fontawesome icon
		$(".view-alteration-object h3 div").after("<div class='arrow'></div>");

		$('.lodt-node-view .pane-views .field-name-body').hide();

		$(".lodt-node-view .pane-views h3").click(function() {
			$(this).parent().next($(".field-name-body")).toggle().toggleClass('active');
		});

		$(".dev-type-node-content article header h3").click(function() {
			$(this).parent().next($(".field-name-body")).toggle().toggleClass('active');
		});

	/** Terms and Conditions **/

		var b = $("body");

		if(	$(b).hasClass("page-planning-residential") ||
			$(b).hasClass("page-planning-commercial") ||
			$(b).hasClass("page-planning-industrial") ||
			$(b).hasClass("page-search-developments")) {

			if (! navigator.cookieEnabled) {
				alert("Please enable browser cookies to proceed.");
				window.location = "/";
   			}

			// Cookie library see https://github.com/carhartl/jquery-cookie
			var agreed = jq1102.cookie('agreed');

		    if(!agreed) {

		    	jQuery.get("/terms-conditions", function( node ) {

		        var terms = jQuery(node).find("#content").html();

		        // Customise our colorbox for mobile
		        if(isMobile.phone) {
					jQuery.colorbox({html:terms ,open:true,iframe:false,closeButton:false,width:320,height:400,overlayClose:false,trapFocus:true,escKey:false, className:'mobile-me'});
				}
		        else {
          jQuery.colorbox({html: terms, open: true, iframe: false, closeButton: false, width: 640, height: 625, overlayClose: false, trapFocus: true, escKey: false, className: 'desktop-me'});
				}

		      }, 'html');
		    }

    	}

    /** Pop outs **/

    	$(".pop-out").prepend("<div class='pop-out-tab'><span></span>Online Survey</div>");

		$(".pop-out .pop-out-tab").click(function() {

			if($(".pop-out").hasClass('active')) {
				$(".pop-out").removeClass('active');
			} else {
				$(".pop-out").addClass('active');
			}

		});

		$(".not-now, .pop-out a.btn").click(function() {
			$(".pop-out").removeClass('active');
		});

	});

	$(document).ready(function($) {

		if(! jq1102.cookie('not-now')) {

			setTimeout(function() {
                jq1102.cookie('not-now', 1);
                $(".pop-out").addClass('active');
			}, 180000);

		}


		/** Webform **/

		// Add/Remove class to selected radio within a group
		$("div.form-type-radio" ).on( "changeWrapperState", function( e ) {

				$($(this).parent().find('div.form-type-radio')).each(function( index ) {

  					$(this).removeClass('on');
				});

				$(this).addClass('on');

		});

		$('input:radio').on('click', function() {

			radioWrap = $(this).closest("div.form-type-radio");

		    $(radioWrap).trigger( "changeWrapperState");

		});

  // Enable the faq toglle for home page
  $("#alteration_faq_content").on('click', 'h3.node-title', function () {

    $(this).parent().next($(".field-name-body")).toggle();
    $(this).toggleClass("active");

    if ($(this).hasClass('active'))
    {
      ga("send", "pageview", {page: findFaqUrl($(this).text())});
    }
  });


  // home slideshow using file responsiveslides.min.js
  // documentation is here : http://responsiveslides.com/
  $(".rslides").responsiveSlides({
    timeout: 8000,
    speed: 4000,
    pauseControls: true,
  });

});


