<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function nsw_planning_drush_command() {
  $items = array();
  $items['send-email-with-reports'] = array(
    'description' => "Send email with report asked be the visitor",
  );

  return $items;
}








/*
 * function 1 du hook cron
 */

function drush_nsw_planning_send_email_with_reports() {
  $items = views_get_view_result("report_by_email");

  foreach ($items as $item) {

    $case = substr($item->field_field_report_status[0]['raw'][value], 0, 1);
    $n = node_load($item->nid);
    //dsm($n);

    switch ($case) {
      case 1: // 1 = 1-New report => we have to generate the PDF
        // if everything goes well, this case is the only one used. But if some nodes stay in an intermediate state, we will try to send them again.
        nsw_planning_reports_cron_step1($n);
        nsw_planning_reports_cron_step2($n);
        nsw_planning_reports_cron_step4($n);
        break;

      case 2: // 2 = 2-PDF generated => we have to send the email
        nsw_planning_reports_cron_step2($n);
        break;

      case 3: // 3 = 3-Ready to send email
        // this case is not used anymore.
        //nsw_planning_reports_cron_step3($n);
        break;

      case 4: // 4 = 4-Email sent => we have to delete the PDF
        nsw_planning_reports_cron_step4($n);
        break;

      case 5: // 5 =5-PDF deleted
        // this case is not used anymore
        //nsw_planning_reports_cron_step5($n);
        break;

      default:
        break;
    }
  }
}

/**
 * Implements hook_mail().
 */
function nsw_planning_reports_drupal_mail($from = 'default_from', $to, $subject, $message, $filePath, $fileName) {
  $my_module = 'custom';
  $my_mail_token = microtime();
  if ($from == 'default_from') {
    // Change this to your own default 'from' email address.
    $from = variable_get('system_mail', 'My Email Address <example@example.com>');
  }
  $message = array(
    'id' => $my_module . '_' . $my_mail_token,
    'to' => $to,
    'subject' => $subject,
    'body' => array(
      $message),
    'headers' => array(
      'From' => $from,
      'Sender' => $from,
      'Return-Path' => $from,
      'Content-Type' => 'text/html; charset=UTF-8; format=flowed',
    ),
  );

  $message['params']['attachments'][] = array(
    'filepath' => $filePath . '/' . $fileName,
    //'filepath' => $filePath,
    //'filename' => $fileName,
    'filemime' => 'application/pdf',
    'list' => TRUE,
  );

  $system = drupal_mail_system($my_module, $my_mail_token);
  $message = $system->format($message);
  if ($system->mail($message)) {
    return TRUE;
  }
  else {
    return FALSE;
  }
}

/**
 * function used for the Cron.
 *  Step1 : generate PDF
 * @param type $node
 */
function nsw_planning_reports_cron_step1($node) {
  $fileNameWithoutExtension = $node->title;
  $html = $node->body['und'][0]['value'];
  $node->field_report_status['und'][0]['value'] = '2-PDF generated';
  $node->field_update_date['und'][0]['value'] = REQUEST_TIME;
  $node->field_pdf_link['und'][0]['title'] = "";
  $node->field_pdf_link['und'][0]['url'] = "";
  nsw_pdf_using_mpdf_api($html, $fileNameWithoutExtension, 2);
  node_save($node);
}

/**
 * function used for the Cron.
 *  Step2 : send the email
 * @param type $node
 */
function nsw_planning_reports_cron_step2($node) {
  $fileName = $node->title . '.pdf';
  $filePath = "public://nsw_pdf_using_mpdf";

  // BEGIN : section for email configuration
  $from = "no-reply@nsw-planning.com";
  $to = $node->field_report_email['und'][0]['safe_value'];
// loading node/2431 which contains the customizable email
  //$n = node_load(2431);
  $nodes = node_load_multiple(array(), array('type' => 'email'));
  if(sizeof($nodes) != 1) {
    drupal_set_message('error', "Email NOT sent");
    watchdog("cron email", "Message NOT sent");
    return;
  }
  foreach($nodes as $ntemp) {
    $n = $ntemp;
  }
  $subject = $n->title;
  $body = $n->body['und'][0]['value'];
  // END : section for email configuration

  $result = nsw_planning_reports_drupal_mail($from, $to, $subject, $body, $filePath, $fileName);

  if ($result === TRUE) {
    //watchdog("cron email", "Message sent"); // drupal_set_message(t('Your message has been sent.'));
    //drupal_set_message("Message sent"); // DEBUG
    $node->field_report_status['und'][0]['value'] = '4-Email sent';
    $node->field_update_date['und'][0]['value'] = REQUEST_TIME;
    node_save($node);
  }
  else {
    watchdog("cron email", "Message NOT sent"); //drupal_set_message(t('There was a problem sending your message and it was not     sent.'), 'error');
    //drupal_set_message("Message NOT sent"); // DEBUG
  }
}

/**
 * function used for the Cron.
 *  Step3 : Not used for the moment
 * @param type $node
 */
function nsw_planning_reports_cron_step3($node) {

}

/**
 * function used for the Cron.
 *  Step4 : Delete the PDF and the Node
 * @param type $node
 */
function nsw_planning_reports_cron_step4($node) {
  $fileName = $node->title . '.pdf';
  $filePath = "public://nsw_pdf_using_mpdf";
  unlink($filePath . '/' . $fileName);
  $node->field_report_status['und'][0]['value'] = '5-PDF deleted';
  $node->field_update_date['und'][0]['value'] = REQUEST_TIME;
  node_save($node);

  // we delete the node
  node_delete($node->nid);
}

/**
 * function used for the Cron.
 *  Step5 : Not used for the moment
 * @param type $node
 */
function nsw_planning_reports_cron_step5($node) {

}
