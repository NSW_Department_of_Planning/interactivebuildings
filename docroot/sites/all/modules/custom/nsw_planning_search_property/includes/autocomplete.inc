<?php
/**
 * @file
 * NSW Planning Address Search Autocomplete Functions
 */

/**
 * Implement Ajax Callback for Suburb autocomplete
 * @param string $string
 */
function nsw_planning_search_property_suburb_autocomplete_callback($string = '') {

  $matches = array();

  if ($string) {

    $results = array();
    $results_two = array();

    if (is_numeric($string)) {
      $results = db_query_range(
        'SELECT * FROM {lga_suburb_postcode} WHERE postcode LIKE :postcode ORDER BY postcode ASC, suburb_name ASC',
        0,
        10,
        array(
          ':postcode' => db_like($string) . '%'
        )
      );
    }
    else {
      $results = db_query_range(
        'SELECT * FROM {lga_suburb_postcode} WHERE suburb_name LIKE :suburb_start ORDER BY suburb_name ASC ,postcode ASC',
        0,
        10,
        array(
          ':suburb_start' => db_like($string) . '%',
        )
      );

      $results_two = db_query_range(
        'SELECT * FROM {lga_suburb_postcode} WHERE suburb_name LIKE :suburb_contains AND suburb_name NOT LIKE :suburb_start ORDER BY suburb_name ASC ,postcode ASC',
        0,
        10,
        array(
          ':suburb_contains' => '%' . db_like($string) . '%',
          ':suburb_start' => db_like($string) . '%',
        )
      );
    }

    foreach ($results as $key => $row) {
      $matches["{$row->suburb_name} || {$row->postcode}"] = check_plain("{$row->suburb_name}, {$row->postcode}");
    }

    if (sizeof($matches) < 10) {
      foreach ($results_two as $key => $row) {
        $matches["{$row->suburb_name} || {$row->postcode}"] = check_plain("{$row->suburb_name}, {$row->postcode}");
      }
      $matches = array_slice($matches, 0, 10);
    }

    if (sizeof($matches) === 0) {
      $matches["no suburb found"] = $string;
    }
  }

  drupal_json_output($matches);

}

/**
 * Implement Ajax Callback for Suburb autocomplete
 * @param string $string
 */
function nsw_planning_search_property_street_autocomplete_callback($suburb_postcode_id = '', $string = '') {

  $matches = array();

  if ($suburb_postcode_id && $string) {
    $input = $string;
    $string = rawurlencode($string);
    $url = "/GetStreet/{$suburb_postcode_id}/{$string}";
    //dsm($url);
    $results = _nsw_planning_search_property_invoke_ws($url);
    asort($results);
    $temp_priority = array();
    $temp_secondary = array();
    foreach ($results as $key => $result) {
      if (stripos($result['Name'], $input) === 0) {
        $temp_priority[$key] = $result;
      }
      else {
        $temp_secondary[$key] = $result;
      }
    }
    $results = $temp_priority + $temp_secondary;
    $sliced_results = array_slice($results, 0, 10);
    foreach ($sliced_results as $key => $row) {
      $matches[$row['Id']] = check_plain("{$row['Name']} {$row['Type']} {$row['Suffix']}");
    }

    if (sizeof($matches) === 0) {
      $matches["no street found"] = $string;
    }
  }

  drupal_json_output($matches);

}