/**
 * @file
 * NSW Planning Search Address form custom JS
 *
 * JS code is not in use at the moment, this autocomplete have been substituted by Drupal Autocomplete
 */
(function($){
  Drupal.behaviors.nswpsearchproperty = {
    attach: function(context, settings) {

    var numTextInputsLast = 0;

    $("input[name=street_no], input[name=unit_no], input[name=dp_no], input[name=lot_no], input[name=section_no]").on( "keypress", function (e) {
      if (e.keyCode == 13) {
        jQuery('input[name=address_search]').mousedown();
        jQuery('input[name=dp_search]').mousedown();
          return false; // avoid the form to get submit by pressing Enter
      }
    });

    $(document).ready(function () {

      $("#nsw-planning-search-property-form").on("click", "[name='view_details']", function () {
          if (isMobile.phone) {
              jQuery.colorbox({
                  html: $("#property_details_modal").html(),
                  open: true,
                  iframe: false,
                  closeButton: false,
                  width: 320,
                  height: 400,
                  trapFocus: true,
                  escKey: false,
                  className: 'mobile-me modal-property'
              });
          }
          else {
              jQuery.colorbox({
                  html: $("#property_details_modal").html(),
                  open: true,
                  iframe: false,
                  closeButton: false,
                  width: 640,
                  height: 610,
                  trapFocus: true,
                  escKey: false,
                  className: 'desktop-me modal-property'
              });
          }
          return false;
      });
      $("#colorbox").on("click", "#edit-modal-close", function () {
          jQuery.colorbox.close();
      });
    });

    jQuery(document).ajaxComplete(function (event, xhr, settings) {
      // console.log(event);
      // console.log(xhr);
      // console.log(settings);
      // console.log("ajax start" + Math.random());

      jQuery("#nsw_planning_search_property_form_search .grey-block").once('ajaxonce', function () {
        // console.log("ajaxonce" + Math.random());
        // only run rest of function if there are no results, otherwise focus on submit button
        var hasResults = $("#nsw_planning_search_property_form_result").has("*").length;
        var textInputs = $("input[type=text]");
        if (hasResults) {
          $("input[name=address_search]").focus();
          $("input[name=dp_search]").focus();
          $("html, body").animate({scrollTop: $("#nsw_planning_search_property_form_result").offset().top}, 1200);
          // console.log("results found, scroll to results and return");
        } else {
          $.each(textInputs, function () {
            if ($(this).val() === "") {
              $(this).focus();
              // iterate until find empty input or get to end
              // console.log("found empty, change focus and return");
              return false;
            }
          });
          // console.log("No empty found, end of function");
        }

        // hide example text when not needed
        // - street name
        if ($(".edit-street-field").length > 0) {
          // console.log("edit street exists");
          if ($(".edit-street-field").val() !== "") {
            // console.log("content in street, hiding example text");
            $(".form-example-text").hide();
          } else {
            // console.log("no content in street, showing example text");
            $(".form-example-text").show();
          }
        }

        // - dp no
        $(".edit-dp-no-field").blur(function () {
          // console.log("focus out of dp no");
          if ($(".edit-dp-no-field").val() !== "") {
            // console.log("content in dp no, hiding example text");
            $(".form-example-text").hide();
          }
        });

        // show example text when needed again
        // - dp no
        $(".edit-dp-no-field").bind("change keyup", function () {
          // console.log("change to dp no");
          if ($(".edit-dp-no-field").val() === "") {
            // console.log("no content in dp no, showing example text");
            $(".form-example-text").show();
          }
        });

        // Bind a handler to prevent invalid characters being entered into dp-no
        var invalidChars = /[^0-9|D|d|P|p|S|s]/g;
        $(".edit-dp-no-field").on('keyup blur', function (e) {
          if (invalidChars.test(this.value)) {
            this.value = this.value.replace(invalidChars, "");
          }
        });

        // Bind a handler to prevent more than 10 chars in dp/sp, lot and section number fields
        $(".edit-dp-no-field, .edit-lot-no-field, .edit-section-no-field").on('keyup blur', function (e) {
          if (this.value.length > 10) {
            this.value = this.value.substr(0, 10);
          }
        });

      });

    });

    }
  };
}(jq1102));

