/**
 * @file
 * Modify Drupal Autocomplete js
 *
 */

(function ($) {

    /**
     * Fills the suggestion popup with any matches received.
     * Override found remove mousedown hide popup
     */
    Drupal.jsAC.prototype.found = function (matches) {
        // If no value in the textfield, do not show the popup.
        if (!this.input.value.length) {
            return false;
        }

        // Prepare matches.
        var ul = $('<ul></ul>');
        var ac = this;
        for (key in matches) {
            $('<li></li>')
                .html($('<div></div>').html(matches[key]))
                .mouseover(function () {
                    ac.highlight(this);
                })
                .mouseout(function () {
                    ac.unhighlight(this);
                })
                .data('autocompleteValue', key)
                .appendTo(ul);
        }

        // Show popup with matches, if any.
        if (this.popup) {
            if (ul.children().length) {
                $(this.popup).empty().append(ul).show();
                $(this.ariaLive).html(Drupal.t('Autocomplete popup'));
            }
            else {
                $(this.popup).css({visibility: 'hidden'});
                this.hidePopup();
            }
        }
    };

    /**
     * Bind autocomplete on suburb and postcode section
     * Override hidePopup append values to a hidden field
     */
    Drupal.jsAC.prototype.hidePopup = function (keycode) {
        var name = this.input.attributes["name"].value;
        if (name == 'suburb_postcode') {
            var targetName = name + '_value';
        }
        if (name == 'street') {
            var targetName = name + '_id';
        }

        // Select item if the right key or mousebutton was pressed.
        if (this.selected && ((keycode && keycode != 46 && keycode != 8 && keycode != 27) || !keycode)) {
            this.input.value = $(this.selected).text();
            $("input[name='" + targetName + "']").val($(this.selected).data('autocompleteValue'));
            $("input[name='" + targetName + "']").change();
        } else {
            this.input.value = '';
            $("input[name='" + targetName + "']").val('');
        }
        // Hide popup.
        var popup = this.popup;
        if (popup) {
            this.popup = null;
            $(popup).fadeOut('slow', function () {
                $(popup).remove();
            });
        }
        this.selected = false;
        $(this.ariaLive).empty();
    };

})(jQuery);