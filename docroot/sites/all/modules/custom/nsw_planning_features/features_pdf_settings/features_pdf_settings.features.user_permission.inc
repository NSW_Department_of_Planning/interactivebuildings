<?php
/**
 * @file
 * features_pdf_settings.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function features_pdf_settings_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'administer mpdf settings'.
  $permissions['administer mpdf settings'] = array(
    'name' => 'administer mpdf settings',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'nsw_pdf_using_mpdf',
  );

  // Exported permission: 'generate pdf using mpdf'.
  $permissions['generate pdf using mpdf'] = array(
    'name' => 'generate pdf using mpdf',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'nsw_pdf_using_mpdf',
  );

  return $permissions;
}
