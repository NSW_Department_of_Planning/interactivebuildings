<?php
/**
 * @file
 * _feature_alteration_object.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function _feature_alteration_object_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "apachesolr" && $api == "apachesolr_environments") {
    return array("version" => "1");
  }
  if ($module == "ctools_custom_content" && $api == "ctools_content") {
    return array("version" => "1");
  }
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
  if ($module == "panels_mini" && $api == "panels_default") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function _feature_alteration_object_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function _feature_alteration_object_image_default_styles() {
  $styles = array();

  // Exported image style: alteration_image.
  $styles['alteration_image'] = array(
    'label' => 'Alteration image',
    'effects' => array(
      6 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 860,
          'height' => 440,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function _feature_alteration_object_node_info() {
  $items = array(
    'alteration_object' => array(
      'name' => t('Alteration object'),
      'base' => 'node_content',
      'description' => t('These objects such as a house, carport or letterbox for which Alteration rules apply.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
