<?php
/**
 * @file
 * _feature_alteration_object.panels_default.inc
 */

/**
 * Implements hook_default_panels_mini().
 */
function _feature_alteration_object_default_panels_mini() {
  $export = array();

  $mini = new stdClass();
  $mini->disabled = FALSE; /* Edit this to true to make a default mini disabled initially */
  $mini->api_version = 1;
  $mini->name = 'development_types_search';
  $mini->category = '';
  $mini->admin_title = 'Development types search';
  $mini->admin_description = '';
  $mini->requiredcontexts = array();
  $mini->contexts = array();
  $mini->relationships = array();
  $display = new panels_display();
  $display->layout = 'threecol_33_34_33';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'left' => NULL,
      'middle' => NULL,
      'right' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Browse list by:';
  $display->uuid = '78ec6886-168b-4a49-83d5-c8366cd64203';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-052d2f15-5186-4f6d-8422-cf9148ddc7be';
    $pane->panel = 'left';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => '',
      'title' => '',
      'body' => '<ul><li><a href="/search/developments/residential" class="residential-link">Residential</a></li></ul>',
      'format' => 'full_html',
      'substitute' => TRUE,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '052d2f15-5186-4f6d-8422-cf9148ddc7be';
    $display->content['new-052d2f15-5186-4f6d-8422-cf9148ddc7be'] = $pane;
    $display->panels['left'][0] = 'new-052d2f15-5186-4f6d-8422-cf9148ddc7be';
    $pane = new stdClass();
    $pane->pid = 'new-3e1f52d6-b0e1-40fb-bd70-778596cd6ef1';
    $pane->panel = 'middle';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => '',
      'title' => '',
      'body' => '<ul><li><a href="/search/developments/commercial" class="commercial-link">Commercial</a></li></ul>',
      'format' => 'full_html',
      'substitute' => TRUE,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '3e1f52d6-b0e1-40fb-bd70-778596cd6ef1';
    $display->content['new-3e1f52d6-b0e1-40fb-bd70-778596cd6ef1'] = $pane;
    $display->panels['middle'][0] = 'new-3e1f52d6-b0e1-40fb-bd70-778596cd6ef1';
    $pane = new stdClass();
    $pane->pid = 'new-a4ce9b2a-0434-4024-b7c0-dfa2256dd702';
    $pane->panel = 'right';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => '',
      'title' => '',
      'body' => '<ul><li><a href="/search/developments/industrial" class="industrial-link">Industrial</a></li></ul>',
      'format' => 'full_html',
      'substitute' => TRUE,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'a4ce9b2a-0434-4024-b7c0-dfa2256dd702';
    $display->content['new-a4ce9b2a-0434-4024-b7c0-dfa2256dd702'] = $pane;
    $display->panels['right'][0] = 'new-a4ce9b2a-0434-4024-b7c0-dfa2256dd702';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $mini->display = $display;
  $export['development_types_search'] = $mini;

  return $export;
}
