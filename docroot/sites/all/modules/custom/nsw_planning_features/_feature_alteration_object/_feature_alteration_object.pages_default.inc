<?php
/**
 * @file
 * _feature_alteration_object.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function _feature_alteration_object_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'alteration_object_detail_';
  $page->task = 'page';
  $page->admin_title = 'Alteration Object Residential ';
  $page->admin_description = '';
  $page->path = 'residential/%title/%nid';
  $page->access = array();
  $page->menu = array();
  $page->arguments = array(
    'title' => array(
      'id' => 1,
      'identifier' => 'String',
      'name' => 'string',
      'settings' => array(
        'use_tail' => 1,
      ),
    ),
    'nid' => array(
      'id' => 1,
      'identifier' => 'Node: ID',
      'name' => 'entity_id:node',
      'settings' => array(),
    ),
  );
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_alteration_object_detail__panel_context';
  $handler->task = 'page';
  $handler->subtask = 'alteration_object_detail_';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'zen_no_wrapper';
  $display->layout_settings = array(
    'main_classes' => 'lodt-node-view',
  );
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'main' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '%nid:title';
  $display->uuid = 'ab1971f2-9241-4751-93ad-8717a90d9a08';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-0aa12774-142c-46ad-a091-adb7cde36d5f';
    $pane->panel = 'main';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => 'Link to model UI',
      'title' => '',
      'body' => '<p><a href="/planning-residential?select=%nid:field_alteration_object_link_id&nid=%nid:nid" class="btn">View in Interactive Building</a></p>',
      'format' => 'full_html',
      'substitute' => 1,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'model-ui-link is-desktop hide-from-ie',
    );
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '0aa12774-142c-46ad-a091-adb7cde36d5f';
    $display->content['new-0aa12774-142c-46ad-a091-adb7cde36d5f'] = $pane;
    $display->panels['main'][0] = 'new-0aa12774-142c-46ad-a091-adb7cde36d5f';
    $pane = new stdClass();
    $pane->pid = 'new-5b43a030-0a84-4a5c-b56c-b4d0e9077583';
    $pane->panel = 'main';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_development_types';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'taxonomy_term_reference_plain',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(),
      'context' => 'argument_entity_id:node_1',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '5b43a030-0a84-4a5c-b56c-b4d0e9077583';
    $display->content['new-5b43a030-0a84-4a5c-b56c-b4d0e9077583'] = $pane;
    $display->panels['main'][1] = 'new-5b43a030-0a84-4a5c-b56c-b4d0e9077583';
    $pane = new stdClass();
    $pane->pid = 'new-4704f7aa-d9b6-4322-86b6-81202fc12b37';
    $pane->panel = 'main';
    $pane->type = 'block';
    $pane->subtype = 'nsw_planning_search_property-property_search';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = '4704f7aa-d9b6-4322-86b6-81202fc12b37';
    $display->content['new-4704f7aa-d9b6-4322-86b6-81202fc12b37'] = $pane;
    $display->panels['main'][2] = 'new-4704f7aa-d9b6-4322-86b6-81202fc12b37';
    $pane = new stdClass();
    $pane->pid = 'new-af01fbcf-3d8f-4923-9d7f-fe0905b37f85';
    $pane->panel = 'main';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_alteration_image';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'image',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(
        'image_style' => 'alteration_image',
        'image_link' => '',
      ),
      'context' => 'argument_entity_id:node_1',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 3;
    $pane->locks = array();
    $pane->uuid = 'af01fbcf-3d8f-4923-9d7f-fe0905b37f85';
    $display->content['new-af01fbcf-3d8f-4923-9d7f-fe0905b37f85'] = $pane;
    $display->panels['main'][3] = 'new-af01fbcf-3d8f-4923-9d7f-fe0905b37f85';
    $pane = new stdClass();
    $pane->pid = 'new-c076532d-bd32-41d7-a2bb-07f22c608278';
    $pane->panel = 'main';
    $pane->type = 'entity_view';
    $pane->subtype = 'node';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'view_mode' => 'full',
      'context' => 'argument_entity_id:node_1',
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 4;
    $pane->locks = array();
    $pane->uuid = 'c076532d-bd32-41d7-a2bb-07f22c608278';
    $display->content['new-c076532d-bd32-41d7-a2bb-07f22c608278'] = $pane;
    $display->panels['main'][4] = 'new-c076532d-bd32-41d7-a2bb-07f22c608278';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['alteration_object_detail_'] = $page;

  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'ao_detail_commercial';
  $page->task = 'page';
  $page->admin_title = 'Alteration Object Commercial ';
  $page->admin_description = '';
  $page->path = 'commercial/%title/%nid';
  $page->access = array();
  $page->menu = array();
  $page->arguments = array(
    'title' => array(
      'id' => 1,
      'identifier' => 'String',
      'name' => 'string',
      'settings' => array(
        'use_tail' => 1,
      ),
    ),
    'nid' => array(
      'id' => 1,
      'identifier' => 'Node: ID',
      'name' => 'entity_id:node',
      'settings' => array(),
    ),
  );
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_ao_detail_commercial_panel_context';
  $handler->task = 'page';
  $handler->subtask = 'ao_detail_commercial';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'zen_no_wrapper';
  $display->layout_settings = array(
    'main_classes' => 'lodt-node-view',
  );
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'main' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '%nid:title';
  $display->uuid = 'ab1971f2-9241-4751-93ad-8717a90d9a08';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-e955a5ef-5805-4568-9db1-017380bd6851';
    $pane->panel = 'main';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => 'Link to Model UI',
      'title' => '',
      'body' => '<p><a href="/planning-commercial?select=%nid:field_alteration_object_link_id&nid=%nid:nid" class="btn">View in Interactive Building</a></p>',
      'format' => 'full_html',
      'substitute' => 1,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'model-ui-link is-desktop hide-from-ie',
    );
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'e955a5ef-5805-4568-9db1-017380bd6851';
    $display->content['new-e955a5ef-5805-4568-9db1-017380bd6851'] = $pane;
    $display->panels['main'][0] = 'new-e955a5ef-5805-4568-9db1-017380bd6851';
    $pane = new stdClass();
    $pane->pid = 'new-5b43a030-0a84-4a5c-b56c-b4d0e9077583';
    $pane->panel = 'main';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_development_types';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'taxonomy_term_reference_plain',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(),
      'context' => 'argument_entity_id:node_1',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '5b43a030-0a84-4a5c-b56c-b4d0e9077583';
    $display->content['new-5b43a030-0a84-4a5c-b56c-b4d0e9077583'] = $pane;
    $display->panels['main'][1] = 'new-5b43a030-0a84-4a5c-b56c-b4d0e9077583';
    $pane = new stdClass();
    $pane->pid = 'new-cdd47eec-e5d7-4bf2-9609-0cd5db7e9ea0';
    $pane->panel = 'main';
    $pane->type = 'block';
    $pane->subtype = 'nsw_planning_search_property-property_search';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = 'cdd47eec-e5d7-4bf2-9609-0cd5db7e9ea0';
    $display->content['new-cdd47eec-e5d7-4bf2-9609-0cd5db7e9ea0'] = $pane;
    $display->panels['main'][2] = 'new-cdd47eec-e5d7-4bf2-9609-0cd5db7e9ea0';
    $pane = new stdClass();
    $pane->pid = 'new-af01fbcf-3d8f-4923-9d7f-fe0905b37f85';
    $pane->panel = 'main';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_alteration_image';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'image',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(
        'image_style' => 'alteration_image',
        'image_link' => '',
      ),
      'context' => 'argument_entity_id:node_1',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 3;
    $pane->locks = array();
    $pane->uuid = 'af01fbcf-3d8f-4923-9d7f-fe0905b37f85';
    $display->content['new-af01fbcf-3d8f-4923-9d7f-fe0905b37f85'] = $pane;
    $display->panels['main'][3] = 'new-af01fbcf-3d8f-4923-9d7f-fe0905b37f85';
    $pane = new stdClass();
    $pane->pid = 'new-0cade456-6f46-4991-8e45-60398fb54ad3';
    $pane->panel = 'main';
    $pane->type = 'entity_view';
    $pane->subtype = 'node';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'view_mode' => 'full',
      'context' => 'argument_entity_id:node_1',
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 4;
    $pane->locks = array();
    $pane->uuid = '0cade456-6f46-4991-8e45-60398fb54ad3';
    $display->content['new-0cade456-6f46-4991-8e45-60398fb54ad3'] = $pane;
    $display->panels['main'][4] = 'new-0cade456-6f46-4991-8e45-60398fb54ad3';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['ao_detail_commercial'] = $page;

  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'ao_detail_industrial';
  $page->task = 'page';
  $page->admin_title = 'Alteration Object Industrial ';
  $page->admin_description = '';
  $page->path = 'industrial/%title/%nid';
  $page->access = array();
  $page->menu = array();
  $page->arguments = array(
    'title' => array(
      'id' => 1,
      'identifier' => 'String',
      'name' => 'string',
      'settings' => array(
        'use_tail' => 1,
      ),
    ),
    'nid' => array(
      'id' => 1,
      'identifier' => 'Node: ID',
      'name' => 'entity_id:node',
      'settings' => array(),
    ),
  );
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_ao_detail_industrial_panel_context';
  $handler->task = 'page';
  $handler->subtask = 'ao_detail_industrial';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'zen_no_wrapper';
  $display->layout_settings = array(
    'main_classes' => 'lodt-node-view',
  );
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'main' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '%nid:title';
  $display->uuid = 'ab1971f2-9241-4751-93ad-8717a90d9a08';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-ef6e9b5a-abf1-4de4-8fb2-52a0a1a62f34';
    $pane->panel = 'main';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => 'Link to Model UI',
      'title' => '',
      'body' => '<p><a href="/planning-industrial?select=%nid:field_alteration_object_link_id&nid=%nid:nid" class="btn">View in Interactive Building</a></p>',
      'format' => 'full_html',
      'substitute' => 1,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'model-ui-link is-desktop hide-from-ie',
    );
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'ef6e9b5a-abf1-4de4-8fb2-52a0a1a62f34';
    $display->content['new-ef6e9b5a-abf1-4de4-8fb2-52a0a1a62f34'] = $pane;
    $display->panels['main'][0] = 'new-ef6e9b5a-abf1-4de4-8fb2-52a0a1a62f34';
    $pane = new stdClass();
    $pane->pid = 'new-5b43a030-0a84-4a5c-b56c-b4d0e9077583';
    $pane->panel = 'main';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_development_types';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'taxonomy_term_reference_plain',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(),
      'context' => 'argument_entity_id:node_1',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '5b43a030-0a84-4a5c-b56c-b4d0e9077583';
    $display->content['new-5b43a030-0a84-4a5c-b56c-b4d0e9077583'] = $pane;
    $display->panels['main'][1] = 'new-5b43a030-0a84-4a5c-b56c-b4d0e9077583';
    $pane = new stdClass();
    $pane->pid = 'new-aa8204d1-5f1d-40f2-b524-bed57f280b63';
    $pane->panel = 'main';
    $pane->type = 'block';
    $pane->subtype = 'nsw_planning_search_property-property_search';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = 'aa8204d1-5f1d-40f2-b524-bed57f280b63';
    $display->content['new-aa8204d1-5f1d-40f2-b524-bed57f280b63'] = $pane;
    $display->panels['main'][2] = 'new-aa8204d1-5f1d-40f2-b524-bed57f280b63';
    $pane = new stdClass();
    $pane->pid = 'new-af01fbcf-3d8f-4923-9d7f-fe0905b37f85';
    $pane->panel = 'main';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_alteration_image';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'image',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(
        'image_style' => 'alteration_image',
        'image_link' => '',
      ),
      'context' => 'argument_entity_id:node_1',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 3;
    $pane->locks = array();
    $pane->uuid = 'af01fbcf-3d8f-4923-9d7f-fe0905b37f85';
    $display->content['new-af01fbcf-3d8f-4923-9d7f-fe0905b37f85'] = $pane;
    $display->panels['main'][3] = 'new-af01fbcf-3d8f-4923-9d7f-fe0905b37f85';
    $pane = new stdClass();
    $pane->pid = 'new-2c9e0277-7c02-4c5e-a623-0bc4fdc5aec5';
    $pane->panel = 'main';
    $pane->type = 'entity_view';
    $pane->subtype = 'node';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'view_mode' => 'full',
      'context' => 'argument_entity_id:node_1',
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 4;
    $pane->locks = array();
    $pane->uuid = '2c9e0277-7c02-4c5e-a623-0bc4fdc5aec5';
    $display->content['new-2c9e0277-7c02-4c5e-a623-0bc4fdc5aec5'] = $pane;
    $display->panels['main'][4] = 'new-2c9e0277-7c02-4c5e-a623-0bc4fdc5aec5';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['ao_detail_industrial'] = $page;

  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'search_development_types_commercial';
  $page->task = 'page';
  $page->admin_title = '';
  $page->admin_description = '';
  $page->path = 'search/developments/commercial';
  $page->access = array();
  $page->menu = array();
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_search_development_types_commercial_panel_context';
  $handler->task = 'page';
  $handler->subtask = 'search_development_types_commercial';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'zen_no_wrapper';
  $display->layout_settings = array(
    'main_classes' => '',
  );
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'main' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Development types';
  $display->uuid = '4c3c58ee-6cb6-434e-93a5-942b6937276f';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-01d85504-7c91-41ae-ae6f-a66ad6b776ff';
    $pane->panel = 'main';
    $pane->type = 'custom';
    $pane->subtype = 'devevelopment_types';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => '',
      'title' => '',
      'body' => '',
      'format' => 'full_html',
      'substitute' => TRUE,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'dev-type-header',
    );
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '01d85504-7c91-41ae-ae6f-a66ad6b776ff';
    $display->content['new-01d85504-7c91-41ae-ae6f-a66ad6b776ff'] = $pane;
    $display->panels['main'][0] = 'new-01d85504-7c91-41ae-ae6f-a66ad6b776ff';
    $pane = new stdClass();
    $pane->pid = 'new-184049ca-3368-4df5-8fbb-24ae64e06d0b';
    $pane->panel = 'main';
    $pane->type = 'block';
    $pane->subtype = 'nsw_planning_search_property-property_search';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '184049ca-3368-4df5-8fbb-24ae64e06d0b';
    $display->content['new-184049ca-3368-4df5-8fbb-24ae64e06d0b'] = $pane;
    $display->panels['main'][1] = 'new-184049ca-3368-4df5-8fbb-24ae64e06d0b';
    $pane = new stdClass();
    $pane->pid = 'new-c01b060f-7d32-4200-94b3-c9c86d21c62f';
    $pane->panel = 'main';
    $pane->type = 'panels_mini';
    $pane->subtype = 'development_types_search';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = 'c01b060f-7d32-4200-94b3-c9c86d21c62f';
    $display->content['new-c01b060f-7d32-4200-94b3-c9c86d21c62f'] = $pane;
    $display->panels['main'][2] = 'new-c01b060f-7d32-4200-94b3-c9c86d21c62f';
    $pane = new stdClass();
    $pane->pid = 'new-edf78fbb-6578-43e6-b7f0-04f01889d3a1';
    $pane->panel = 'main';
    $pane->type = 'views';
    $pane->subtype = 'commercial_search';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 0,
      'nodes_per_page' => '0',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'block_1',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 3;
    $pane->locks = array();
    $pane->uuid = 'edf78fbb-6578-43e6-b7f0-04f01889d3a1';
    $display->content['new-edf78fbb-6578-43e6-b7f0-04f01889d3a1'] = $pane;
    $display->panels['main'][3] = 'new-edf78fbb-6578-43e6-b7f0-04f01889d3a1';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['search_development_types_commercial'] = $page;

  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'search_development_types_industrial';
  $page->task = 'page';
  $page->admin_title = '';
  $page->admin_description = '';
  $page->path = 'search/developments/industrial';
  $page->access = array();
  $page->menu = array();
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_search_development_types_industrial_panel_context';
  $handler->task = 'page';
  $handler->subtask = 'search_development_types_industrial';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'zen_no_wrapper';
  $display->layout_settings = array(
    'main_classes' => '',
  );
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'main' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Development types';
  $display->uuid = '415caef3-3b5c-458d-b2c3-5523c381363a';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-cb0e86d7-a485-4e00-827f-5120b43eb93d';
    $pane->panel = 'main';
    $pane->type = 'custom';
    $pane->subtype = 'devevelopment_types';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => '',
      'title' => '',
      'body' => '',
      'format' => 'full_html',
      'substitute' => TRUE,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'dev-type-header',
    );
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'cb0e86d7-a485-4e00-827f-5120b43eb93d';
    $display->content['new-cb0e86d7-a485-4e00-827f-5120b43eb93d'] = $pane;
    $display->panels['main'][0] = 'new-cb0e86d7-a485-4e00-827f-5120b43eb93d';
    $pane = new stdClass();
    $pane->pid = 'new-2da4f565-245e-40be-a5fc-673409133309';
    $pane->panel = 'main';
    $pane->type = 'block';
    $pane->subtype = 'nsw_planning_search_property-property_search';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '2da4f565-245e-40be-a5fc-673409133309';
    $display->content['new-2da4f565-245e-40be-a5fc-673409133309'] = $pane;
    $display->panels['main'][1] = 'new-2da4f565-245e-40be-a5fc-673409133309';
    $pane = new stdClass();
    $pane->pid = 'new-a3594c30-9484-437c-952a-f10d79631daf';
    $pane->panel = 'main';
    $pane->type = 'panels_mini';
    $pane->subtype = 'development_types_search';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = 'a3594c30-9484-437c-952a-f10d79631daf';
    $display->content['new-a3594c30-9484-437c-952a-f10d79631daf'] = $pane;
    $display->panels['main'][2] = 'new-a3594c30-9484-437c-952a-f10d79631daf';
    $pane = new stdClass();
    $pane->pid = 'new-400c0e87-2858-4fc9-8fed-67e577cc3b3f';
    $pane->panel = 'main';
    $pane->type = 'views';
    $pane->subtype = 'industrial_search';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 0,
      'nodes_per_page' => '0',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'block_1',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 3;
    $pane->locks = array();
    $pane->uuid = '400c0e87-2858-4fc9-8fed-67e577cc3b3f';
    $display->content['new-400c0e87-2858-4fc9-8fed-67e577cc3b3f'] = $pane;
    $display->panels['main'][3] = 'new-400c0e87-2858-4fc9-8fed-67e577cc3b3f';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['search_development_types_industrial'] = $page;

  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'search_development_types_residential';
  $page->task = 'page';
  $page->admin_title = 'Search development types residential';
  $page->admin_description = '';
  $page->path = 'search/developments/residential';
  $page->access = array();
  $page->menu = array();
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_search_development_types_residential_panel_context';
  $handler->task = 'page';
  $handler->subtask = 'search_development_types_residential';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'zen_no_wrapper';
  $display->layout_settings = array(
    'main_classes' => 'clearfix',
  );
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'main' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'A - Z Listing';
  $display->uuid = '2c5cf294-dd9d-4a6e-bf5a-78dc1e06d89d';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-f9b1a40a-426c-439d-8c54-6d7afeab9ea9';
    $pane->panel = 'main';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => '',
      'title' => '',
      'body' => '',
      'format' => 'full_html',
      'substitute' => TRUE,
      'name' => 'devevelopment_types',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'dev-type-header',
    );
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'f9b1a40a-426c-439d-8c54-6d7afeab9ea9';
    $display->content['new-f9b1a40a-426c-439d-8c54-6d7afeab9ea9'] = $pane;
    $display->panels['main'][0] = 'new-f9b1a40a-426c-439d-8c54-6d7afeab9ea9';
    $pane = new stdClass();
    $pane->pid = 'new-c886d34f-f350-4fcf-b293-bdcee7367d3f';
    $pane->panel = 'main';
    $pane->type = 'block';
    $pane->subtype = 'nsw_planning_search_property-property_search';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = 'c886d34f-f350-4fcf-b293-bdcee7367d3f';
    $display->content['new-c886d34f-f350-4fcf-b293-bdcee7367d3f'] = $pane;
    $display->panels['main'][1] = 'new-c886d34f-f350-4fcf-b293-bdcee7367d3f';
    $pane = new stdClass();
    $pane->pid = 'new-c0fd0817-b57a-4568-afab-950e29e3f44b';
    $pane->panel = 'main';
    $pane->type = 'panels_mini';
    $pane->subtype = 'development_types_search';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = 'c0fd0817-b57a-4568-afab-950e29e3f44b';
    $display->content['new-c0fd0817-b57a-4568-afab-950e29e3f44b'] = $pane;
    $display->panels['main'][2] = 'new-c0fd0817-b57a-4568-afab-950e29e3f44b';
    $pane = new stdClass();
    $pane->pid = 'new-653b66b5-d29b-4e5b-b4dd-fc3f6436033a';
    $pane->panel = 'main';
    $pane->type = 'views';
    $pane->subtype = 'custom_search';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 0,
      'nodes_per_page' => '0',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'block_1',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 3;
    $pane->locks = array();
    $pane->uuid = '653b66b5-d29b-4e5b-b4dd-fc3f6436033a';
    $display->content['new-653b66b5-d29b-4e5b-b4dd-fc3f6436033a'] = $pane;
    $display->panels['main'][3] = 'new-653b66b5-d29b-4e5b-b4dd-fc3f6436033a';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['search_development_types_residential'] = $page;

  return $pages;

}
