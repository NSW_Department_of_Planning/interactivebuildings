<?php
/**
 * @file
 * _feature_alteration_object.apachesolr_environments.inc
 */

/**
 * Implements hook_apachesolr_environments().
 */
function _feature_alteration_object_apachesolr_environments() {
  $export = array();

  $environment = new stdClass();
  $environment->api_version = 1;
  $environment->env_id = 'acquia_search_server_1';
  $environment->name = 'Acquia Search';
  $environment->url = 'http://apseast1-c3.acquia-search.com/solr/EHRY-55286';
  $environment->service_class = 'AcquiaSearchService';
  $environment->conf = array(
    'apachesolr_direct_commit' => 0,
    'apachesolr_read_only' => '0',
    'apachesolr_search_changed_boost' => '0:0',
    'apachesolr_search_comment_boost' => '0:0',
    'apachesolr_search_date_boost' => '0:0',
    'apachesolr_search_promote_boost' => '0',
    'apachesolr_search_sticky_boost' => '0',
    'apachesolr_search_type_boosts' => array(
      'alteration_object' => '0',
      'article' => '0',
      'alteration_faq' => '0',
      'page' => '0',
      'panel' => '0',
      'plain_page' => '0',
      'terms_and_conditions' => '0',
    ),
    'apachesolr_soft_commit' => 0,
    'field_bias' => array(
      'bs_promote' => '0',
      'bs_status' => '0',
      'bs_sticky' => '0',
      'bs_translate' => '0',
      'bundle' => '0',
      'bundle_name' => '0',
      'content' => '0',
      'ds_changed' => '0',
      'ds_created' => '0',
      'ds_last_comment_or_change' => '0',
      'entity_id' => '0',
      'entity_type' => '0',
      'hash' => '0',
      'id' => '0',
      'im_field_alteration_faq' => '0',
      'im_field_alteration_object_categ' => '0',
      'im_field_alteration_object_rest_' => '0',
      'im_field_development_types' => '0',
      'im_field_developments_synonyms' => '0',
      'im_vid_2' => '0',
      'im_vid_4' => '0',
      'im_vid_6' => '0',
      'is_tnid' => '0',
      'is_uid' => '0',
      'label' => '21.0',
      'path' => '0',
      'path_alias' => '0',
      'site' => '0',
      'sm_field_alteration_object_link_' => '0',
      'sm_vid_Alteration_object_category' => '0',
      'sm_vid_Development_types' => '0',
      'sm_vid_Developments_synonyms' => '0',
      'sort_label' => '0',
      'sort_language' => '0',
      'sort_name' => '0',
      'sort_name_formatted' => '0',
      'spell' => '0',
      'ss_language' => '0',
      'ss_name' => '0',
      'ss_name_formatted' => '0',
      'tags_a' => '0',
      'tags_h1' => '0',
      'tags_h2_h3' => '0',
      'tags_h4_h5_h6' => '0',
      'tags_inline' => '0',
      'taxonomy_names' => '0',
      'tid' => '0',
      'timestamp' => '0',
      'tm_vid_2_names' => '0',
      'tm_vid_4_names' => '0',
      'tm_vid_6_names' => '13.0',
      'tos_content_extra' => '0',
      'tos_name' => '0',
      'tos_name_formatted' => '0',
      'ts_comments' => '0',
      'url' => '0',
    ),
  );
  $environment->index_bundles = array(
    'node' => array(
      0 => 'additional_requirement',
      1 => 'alteration_object',
      2 => 'email',
      3 => 'report_by_email',
      4 => 'tag',
    ),
  );
  $export['acquia_search_server_1'] = $environment;

  $environment = new stdClass();
  $environment->api_version = 1;
  $environment->env_id = 'solr';
  $environment->name = 'localhost server';
  $environment->url = 'http://localhost:8983/solr';
  $environment->service_class = '';
  $environment->conf = array();
  $environment->index_bundles = array(
    'node' => array(
      0 => 'additional_requirement',
      1 => 'alteration_faq',
      2 => 'alteration_object',
      3 => 'article',
      4 => 'email',
      5 => 'page',
      6 => 'panel',
      7 => 'plain_page',
      8 => 'report_by_email',
      9 => 'slideshow',
      10 => 'tag',
      11 => 'terms_and_conditions',
      12 => 'webform',
    ),
  );
  $export['solr'] = $environment;

  return $export;
}
