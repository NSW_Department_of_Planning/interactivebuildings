<?php
/**
 * @file
 * _feature_alteration_object.ctools_content.inc
 */

/**
 * Implements hook_default_ctools_custom_content().
 */
function _feature_alteration_object_default_ctools_custom_content() {
  $export = array();

  $content = new stdClass();
  $content->disabled = FALSE; /* Edit this to true to make a default content disabled initially */
  $content->api_version = 1;
  $content->name = 'devevelopment_types';
  $content->admin_title = 'Development types header';
  $content->admin_description = '';
  $content->category = 'Reusable custom content';
  $content->settings = array(
    'admin_title' => '',
    'title' => '',
    'body' => '<p>Find out the planning requirements and restrictions that may apply to your property.</p>',
    'format' => 'full_html',
    'substitute' => 1,
  );
  $export['devevelopment_types'] = $content;

  $content = new stdClass();
  $content->disabled = FALSE; /* Edit this to true to make a default content disabled initially */
  $content->api_version = 1;
  $content->name = 'reports_link';
  $content->admin_title = 'Reports link';
  $content->admin_description = '';
  $content->category = 'Reusable custom content';
  $content->settings = array(
    'admin_title' => 'Reports link',
    'title' => '',
    'body' => '<h2 class="icon-reports">Reports</h2><p>Save/Print/Download reports of your recently viewed development types.<span><a href="/reports" target="_blank" class="btn">View reports now</a></span></p>',
    'format' => 'full_html',
    'substitute' => 1,
  );
  $export['reports_link'] = $content;

  return $export;
}
