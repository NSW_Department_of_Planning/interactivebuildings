<?php
/**
 * @file
 * _feature_landing_page.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function _feature_landing_page_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['block-form_title'] = array(
    'cache' => -1,
    'css_class' => 'is-desktop',
    'custom' => 0,
    'machine_name' => 'form_title',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '/home
<front>',
    'roles' => array(
      'anonymous user' => 1,
    ),
    'themes' => array(
      'nsw' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'nsw',
        'weight' => -27,
      ),
      'zen' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'zen',
        'weight' => 0,
      ),
    ),
    'title' => 'Form Title',
    'visibility' => 1,
  );

  $export['block-form_title_mobile'] = array(
    'cache' => -1,
    'css_class' => 'is-mobile',
    'custom' => 0,
    'machine_name' => 'form_title_mobile',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'nsw' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'nsw',
        'weight' => -26,
      ),
      'zen' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'zen',
        'weight' => 0,
      ),
    ),
    'title' => 'Form Title - mobile',
    'visibility' => 0,
  );

  $export['block-radiobutton1'] = array(
    'cache' => -1,
    'css_class' => 'is-desktop',
    'custom' => 0,
    'machine_name' => 'radiobutton1',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'nsw' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'nsw',
        'weight' => -12,
      ),
      'zen' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'zen',
        'weight' => 0,
      ),
    ),
    'title' => 'RadioButton1 - I have a specific property in mind',
    'visibility' => 0,
  );

  $export['block-radiobutton1_mobile'] = array(
    'cache' => -1,
    'css_class' => 'is-mobile',
    'custom' => 0,
    'machine_name' => 'radiobutton1_mobile',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'nsw' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'nsw',
        'weight' => -11,
      ),
      'zen' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'zen',
        'weight' => 0,
      ),
    ),
    'title' => 'RadioButton1 - I have a specific property in mind - mobile',
    'visibility' => 0,
  );

  $export['block-radiobutton2'] = array(
    'cache' => -1,
    'css_class' => 'is-desktop',
    'custom' => 0,
    'machine_name' => 'radiobutton2',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'nsw' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'nsw',
        'weight' => -10,
      ),
      'zen' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'zen',
        'weight' => 0,
      ),
    ),
    'title' => 'RadioButton2 - I don\'t have a specific property in mind',
    'visibility' => 0,
  );

  $export['block-radiobutton3_js'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'machine_name' => 'radiobutton3_js',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'nsw' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'nsw',
        'weight' => 20,
      ),
      'zen' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'zen',
        'weight' => 0,
      ),
    ),
    'title' => 'javascript toggle_v init',
    'visibility' => 0,
  );

  return $export;
}
