<?php
/**
 * @file
 * _feature_landing_page.features.fe_block_boxes.inc
 */

/**
 * Implements hook_default_fe_block_boxes().
 */
function _feature_landing_page_default_fe_block_boxes() {
  $export = array();

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Form Title';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'form_title';
  $fe_block_boxes->body = '<h2 class="search-form-top-title" align="center">Want to know what you can change on a property <br /><strong>without</strong> any planning approval?</h2>';

  $export['form_title'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Form Title for mobile';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'form_title_mobile';
  $fe_block_boxes->body = '<h2 class="search-form-top-title" align="center">Want to know what you can change on a property <strong>without</strong> any planning approval?</h2>';

  $export['form_title_mobile'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'RadioButton1 - I have a specific property in mind';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'radiobutton1';
  $fe_block_boxes->body = '<div id="radio_toggle_one-wrapper"><input id="radio_toggle_one" onclick="toggle_v()" type="radio" name="radio_toggle" value="one" /><label for="radio_toggle_one">I have a specific property in mind</label></div>';

  $export['radiobutton1'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'RadioButton1 - I have a specific property in mind - mobile';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'radiobutton1_mobile';
  $fe_block_boxes->body = '<div id="radio_toggle_one-wrapper"><input id="radio_toggle_one" onclick="toggle_v()" type="radio" name="radio_toggle" value="one" /><label for="radio_toggle_one">I have a specific property in mind</label></div>';

  $export['radiobutton1_mobile'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'RadioButton2 - I don\'t have a specific property in mind';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'radiobutton2';
  $fe_block_boxes->body = '<div id="radio_toggle_two-wrapper" class="clear-both"><input id="radio_toggle_two" onclick="toggle_v()" type="radio" name="radio_toggle" value="two" /><label for="radio_toggle_two">I don\'t have a specific property in mind</label></div><table id="view_general_rules" style="width: 100%; display: none;"><tbody><tr><td class="text-align-center" colspan="3"><div class="home-subsection-title">View general rules based on building type</div></td></tr><tr><td class="first" style="width: 33%;"><div class="display-home-icon"><img src="/sites/all/themes/nsw/images/sprint2/home-icon-residential.png" alt="" /></div><div class="home-subsection-subtitle">Residential</div><div class="home-subsection-example">e.g. houses, apartments and rural property</div><div><a href="/planning-residential">Explore in 3D model &gt;</a></div><div><a href="/search/developments/residential">View A-Z listing &gt;</a></div></td><td style="width: 34%;"><div class="display-home-icon"><img src="/sites/all/themes/nsw/images/sprint2/home-icon-commercial.png" alt="" /></div><div class="home-subsection-subtitle">Commercial</div><div class="home-subsection-example">e.g. office buildings and retail stores</div><div><a href="/planning-commercial">Explore in 3D model &gt; </a></div><div><a href="/search/developments/commercial">View A-Z listing &gt;</a></div></td><td class="last" style="width: 33%;"><div class="display-home-icon"><img src="/sites/all/themes/nsw/images/sprint2/home-icon-industrial.png" alt="" /></div><div class="home-subsection-subtitle">Industrial</div><div class="home-subsection-example">e.g. warehouses and factories</div><div><a href="/planning-industrial">Explore in 3D model &gt;</a></div><div><a href="/search/developments/industrial">View A-Z listing &gt;</a></div></td></tr></tbody></table>';

  $export['radiobutton2'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'javascript toggle_v init';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'radiobutton3_js';
  $fe_block_boxes->body = '<script type="text/javascript">// <![CDATA[
function toggle_v() {
  var one = document.getElementById(\'radio_toggle_one\').checked ;
  var two = document.getElementById(\'radio_toggle_two\').checked ;  
  
  if(one) {
    document.getElementById(\'nsw-planning-search-property-form\').style.display = \'block\';
    document.getElementById(\'view_general_rules\').style.display = \'none\';
    document.getElementById(\'radio_toggle_one-wrapper\').className =\'toggle-active\';
    document.getElementById(\'radio_toggle_two-wrapper\').className =\'\';
  } 
  if(two) {
    document.getElementById(\'nsw-planning-search-property-form\').style.display = \'none\';
    document.getElementById(\'view_general_rules\').style.display = \'block\';
    document.getElementById(\'radio_toggle_one-wrapper\').className =\'\';
    document.getElementById(\'radio_toggle_two-wrapper\').className =\'toggle-active\';
  } 
  // case : a property is already selected and the user come back to the homepage 
  if(!one && !two && document.getElementById(\'nsw_planning_search_property_form_property_details\')){
    document.getElementById(\'nsw-planning-search-property-form\').style.display = \'block\';
    document.getElementById(\'view_general_rules\').style.display = \'none\';
    document.getElementById(\'radio_toggle_one-wrapper\').className =\'toggle-active\';
    document.getElementById(\'radio_toggle_two-wrapper\').className =\'\';
    document.getElementById(\'radio_toggle_one\').checked = true;
  }
}
// ]]></script>
<script type="text/javascript">// <![CDATA[
toggle_v();
// ]]></script>';

  $export['radiobutton3_js'] = $fe_block_boxes;

  return $export;
}
