<?php
/**
 * @file
 * _feature_landing_page.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function _feature_landing_page_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ctools_custom_content" && $api == "ctools_content") {
    return array("version" => "1");
  }
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
  if ($module == "panels_mini" && $api == "panels_default") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function _feature_landing_page_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function _feature_landing_page_image_default_styles() {
  $styles = array();

  // Exported image style: highlight.
  $styles['highlight'] = array(
    'label' => 'Highlight',
    'effects' => array(),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function _feature_landing_page_node_info() {
  $items = array(
    'homepage' => array(
      'name' => t('HomePage'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'page' => array(
      'name' => t('Landing page'),
      'base' => 'node_content',
      'description' => t('Use <em>landing pages</em> for content including a \'Highlight\' and other optional fields'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'slideshow' => array(
      'name' => t('Slideshow'),
      'base' => 'node_content',
      'description' => t('Header Slideshow'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
