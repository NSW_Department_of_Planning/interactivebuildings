<?php
/**
 * @file
 * _feature_landing_page.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_handlers().
 */
function _feature_landing_page_default_page_manager_handlers() {
  $export = array();

  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'node_view_panel_context';
  $handler->task = 'node_view';
  $handler->subtask = '';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Landing page',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'access' => array(
      'plugins' => array(
        0 => array(
          'name' => 'node_type',
          'settings' => array(
            'type' => array(
              'page' => 'page',
            ),
          ),
          'context' => 'argument_entity_id:node_1',
          'not' => FALSE,
        ),
      ),
      'logic' => 'and',
    ),
  );
  $display = new panels_display();
  $display->layout = 'zen_no_wrapper';
  $display->layout_settings = array(
    'main_classes' => '',
  );
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'main' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = 'b56cf0bb-6ee2-4271-a369-e6268543745a';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-bb50113f-2cb6-4edd-8fb2-db32e9738cf6';
    $pane->panel = 'main';
    $pane->type = 'block';
    $pane->subtype = 'block-56';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'bb50113f-2cb6-4edd-8fb2-db32e9738cf6';
    $display->content['new-bb50113f-2cb6-4edd-8fb2-db32e9738cf6'] = $pane;
    $display->panels['main'][0] = 'new-bb50113f-2cb6-4edd-8fb2-db32e9738cf6';
    $pane = new stdClass();
    $pane->pid = 'new-5228ac0b-1e08-4d8e-8d82-c34e0da0a80e';
    $pane->panel = 'main';
    $pane->type = 'block';
    $pane->subtype = 'block-61';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '5228ac0b-1e08-4d8e-8d82-c34e0da0a80e';
    $display->content['new-5228ac0b-1e08-4d8e-8d82-c34e0da0a80e'] = $pane;
    $display->panels['main'][1] = 'new-5228ac0b-1e08-4d8e-8d82-c34e0da0a80e';
    $pane = new stdClass();
    $pane->pid = 'new-534ed3b8-c11a-46ec-aea4-d423a3bea28e';
    $pane->panel = 'main';
    $pane->type = 'block';
    $pane->subtype = 'block-66';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = '534ed3b8-c11a-46ec-aea4-d423a3bea28e';
    $display->content['new-534ed3b8-c11a-46ec-aea4-d423a3bea28e'] = $pane;
    $display->panels['main'][2] = 'new-534ed3b8-c11a-46ec-aea4-d423a3bea28e';
    $pane = new stdClass();
    $pane->pid = 'new-bd6fc44d-aa0a-4d30-a415-d55eb207b5af';
    $pane->panel = 'main';
    $pane->type = 'block';
    $pane->subtype = 'block-71';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 3;
    $pane->locks = array();
    $pane->uuid = 'bd6fc44d-aa0a-4d30-a415-d55eb207b5af';
    $display->content['new-bd6fc44d-aa0a-4d30-a415-d55eb207b5af'] = $pane;
    $display->panels['main'][3] = 'new-bd6fc44d-aa0a-4d30-a415-d55eb207b5af';
    $pane = new stdClass();
    $pane->pid = 'new-cc0e1508-101d-47de-bd6f-971038364b83';
    $pane->panel = 'main';
    $pane->type = 'block';
    $pane->subtype = 'nsw_planning_search_property-property_search';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'hide-from-ie',
    );
    $pane->extras = array();
    $pane->position = 4;
    $pane->locks = array();
    $pane->uuid = 'cc0e1508-101d-47de-bd6f-971038364b83';
    $display->content['new-cc0e1508-101d-47de-bd6f-971038364b83'] = $pane;
    $display->panels['main'][4] = 'new-cc0e1508-101d-47de-bd6f-971038364b83';
    $pane = new stdClass();
    $pane->pid = 'new-aa0c0b1c-63bd-454e-9619-ef21e3e29d09';
    $pane->panel = 'main';
    $pane->type = 'block';
    $pane->subtype = 'block-76';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 5;
    $pane->locks = array();
    $pane->uuid = 'aa0c0b1c-63bd-454e-9619-ef21e3e29d09';
    $display->content['new-aa0c0b1c-63bd-454e-9619-ef21e3e29d09'] = $pane;
    $display->panels['main'][5] = 'new-aa0c0b1c-63bd-454e-9619-ef21e3e29d09';
    $pane = new stdClass();
    $pane->pid = 'new-a1f60d3f-8be4-4a4f-a583-7f434fab16ca';
    $pane->panel = 'main';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => '',
      'title' => '',
      'body' => '',
      'format' => 'full_html',
      'substitute' => TRUE,
      'name' => 'list_of_development_types_link',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'is-mobile',
    );
    $pane->extras = array();
    $pane->position = 6;
    $pane->locks = array();
    $pane->uuid = 'a1f60d3f-8be4-4a4f-a583-7f434fab16ca';
    $display->content['new-a1f60d3f-8be4-4a4f-a583-7f434fab16ca'] = $pane;
    $display->panels['main'][6] = 'new-a1f60d3f-8be4-4a4f-a583-7f434fab16ca';
    $pane = new stdClass();
    $pane->pid = 'new-499af1fa-dc0f-43c5-b596-67cfed923f25';
    $pane->panel = 'main';
    $pane->type = 'custom';
    $pane->subtype = 'browse_list_exempt_development_types';
    $pane->shown = FALSE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => '',
      'title' => '',
      'body' => '',
      'format' => 'full_html',
      'substitute' => TRUE,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'is-desktop hide-from-ie',
    );
    $pane->extras = array();
    $pane->position = 7;
    $pane->locks = array();
    $pane->uuid = '499af1fa-dc0f-43c5-b596-67cfed923f25';
    $display->content['new-499af1fa-dc0f-43c5-b596-67cfed923f25'] = $pane;
    $display->panels['main'][7] = 'new-499af1fa-dc0f-43c5-b596-67cfed923f25';
    $pane = new stdClass();
    $pane->pid = 'new-1e940843-adef-4819-a969-49784d286c18';
    $pane->panel = 'main';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => '',
      'title' => '',
      'body' => '',
      'format' => 'full_html',
      'substitute' => TRUE,
      'name' => 'mobile_message',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'is-mobile messages--info message info',
    );
    $pane->extras = array();
    $pane->position = 8;
    $pane->locks = array();
    $pane->uuid = '1e940843-adef-4819-a969-49784d286c18';
    $display->content['new-1e940843-adef-4819-a969-49784d286c18'] = $pane;
    $display->panels['main'][8] = 'new-1e940843-adef-4819-a969-49784d286c18';
    $pane = new stdClass();
    $pane->pid = 'new-fec9b31c-862c-4c2f-883d-08ab24894dc2';
    $pane->panel = 'main';
    $pane->type = 'block';
    $pane->subtype = 'block-16';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 9;
    $pane->locks = array();
    $pane->uuid = 'fec9b31c-862c-4c2f-883d-08ab24894dc2';
    $display->content['new-fec9b31c-862c-4c2f-883d-08ab24894dc2'] = $pane;
    $display->panels['main'][9] = 'new-fec9b31c-862c-4c2f-883d-08ab24894dc2';
    $pane = new stdClass();
    $pane->pid = 'new-e156109c-83c6-46c3-94ae-1db3509fb0d3';
    $pane->panel = 'main';
    $pane->type = 'block';
    $pane->subtype = 'block-81';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 10;
    $pane->locks = array();
    $pane->uuid = 'e156109c-83c6-46c3-94ae-1db3509fb0d3';
    $display->content['new-e156109c-83c6-46c3-94ae-1db3509fb0d3'] = $pane;
    $display->panels['main'][10] = 'new-e156109c-83c6-46c3-94ae-1db3509fb0d3';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $export['node_view_panel_context'] = $handler;

  return $export;
}
