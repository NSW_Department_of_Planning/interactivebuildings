<?php
/**
 * @file
 * _feature_landing_page.panels_default.inc
 */

/**
 * Implements hook_default_panels_mini().
 */
function _feature_landing_page_default_panels_mini() {
  $export = array();

  $mini = new stdClass();
  $mini->disabled = FALSE; /* Edit this to true to make a default mini disabled initially */
  $mini->api_version = 1;
  $mini->name = 'development_types';
  $mini->category = '';
  $mini->admin_title = 'Development types';
  $mini->admin_description = '';
  $mini->requiredcontexts = array();
  $mini->contexts = array();
  $mini->relationships = array();
  $display = new panels_display();
  $display->layout = 'threecol_33_34_33';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'left' => NULL,
      'middle' => NULL,
      'right' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Select your property type';
  $display->uuid = '54743c7d-fcca-49f6-9f8b-be5c2093a431';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-14cc2ddd-14a5-4dbc-81e7-cc6eddc35715';
    $pane->panel = 'left';
    $pane->type = 'block';
    $pane->subtype = 'imageblock-1';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '14cc2ddd-14a5-4dbc-81e7-cc6eddc35715';
    $display->content['new-14cc2ddd-14a5-4dbc-81e7-cc6eddc35715'] = $pane;
    $display->panels['left'][0] = 'new-14cc2ddd-14a5-4dbc-81e7-cc6eddc35715';
    $pane = new stdClass();
    $pane->pid = 'new-7148b2ab-021a-4486-892d-6b6e4f3c2755';
    $pane->panel = 'middle';
    $pane->type = 'block';
    $pane->subtype = 'imageblock-6';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '7148b2ab-021a-4486-892d-6b6e4f3c2755';
    $display->content['new-7148b2ab-021a-4486-892d-6b6e4f3c2755'] = $pane;
    $display->panels['middle'][0] = 'new-7148b2ab-021a-4486-892d-6b6e4f3c2755';
    $pane = new stdClass();
    $pane->pid = 'new-15ad3791-83ca-4016-9445-87244d7497e8';
    $pane->panel = 'right';
    $pane->type = 'block';
    $pane->subtype = 'imageblock-11';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '15ad3791-83ca-4016-9445-87244d7497e8';
    $display->content['new-15ad3791-83ca-4016-9445-87244d7497e8'] = $pane;
    $display->panels['right'][0] = 'new-15ad3791-83ca-4016-9445-87244d7497e8';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $mini->display = $display;
  $export['development_types'] = $mini;

  return $export;
}
