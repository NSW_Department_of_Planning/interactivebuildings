<?php
/**
 * @file
 * _feature_landing_page.ctools_content.inc
 */

/**
 * Implements hook_default_ctools_custom_content().
 */
function _feature_landing_page_default_ctools_custom_content() {
  $export = array();

  $content = new stdClass();
  $content->disabled = FALSE; /* Edit this to true to make a default content disabled initially */
  $content->api_version = 1;
  $content->name = 'browse_list_exempt_development_types';
  $content->admin_title = 'Browse the full list of exempt development types - desk';
  $content->admin_description = '';
  $content->category = 'Reusable custom content';
  $content->settings = array(
    'admin_title' => 'Browse the full list of exempt development types',
    'title' => '',
    'body' => '<h2 style="text-align: left;">Only want general information?&nbsp;</h2><p style="text-align: left;">View general planning requirements and restrictions for <a href="/clear/property?destination=planning-residential">residential</a>, <a href="/clear/property?destination=planning-commercial">commercial</a> or <a href="/clear/property?destination=planning-industrial">industrial</a> property types.&nbsp;</p>',
    'format' => 'full_html',
    'substitute' => 1,
  );
  $export['browse_list_exempt_development_types'] = $content;

  $content = new stdClass();
  $content->disabled = FALSE; /* Edit this to true to make a default content disabled initially */
  $content->api_version = 1;
  $content->name = 'list_of_development_types_link';
  $content->admin_title = 'Browse the full list of exempt development types - mobile';
  $content->admin_description = '';
  $content->category = '';
  $content->settings = array(
    'admin_title' => 'Browse the full list of exempt development types - mobile',
    'title' => '',
    'body' => '<div id="radio_toggle_two-wrapper" class="clear-both"><input id="radio_toggle_two" onclick="toggle_v()" type="radio" name="radio_toggle" value="two" /><label for="radio_toggle_two">I don\'t have a specific property in mind</label></div><div id="view_general_rules" style="width: 100%; display: none;"><!--<h2 style="text-align: center;">Browse the full list of exempt development types<strong>&nbsp;</strong></h2>--><p style="text-align: center; padding : 0 20px">View the A-Z Listing to find out the general rules that may apply to your property.<!--Find out the planning requirements and restrictions that may apply to your property.&nbsp;--></p><p style="text-align: center;"><a href="/search/developments/residential" class="btn">View A-Z Listing</a></p></div>',
    'format' => 'full_html',
    'substitute' => 1,
  );
  $export['list_of_development_types_link'] = $content;

  $content = new stdClass();
  $content->disabled = FALSE; /* Edit this to true to make a default content disabled initially */
  $content->api_version = 1;
  $content->name = 'mobile_message';
  $content->admin_title = 'Mobile message';
  $content->admin_description = '';
  $content->category = 'Reusable custom content';
  $content->settings = array(
    'admin_title' => 'Mobile message',
    'title' => '',
    'body' => '<!--<p>To view the Interactive Buildings, please use a desktop PC, laptop or tablet device.</p>-->
<p>To view the 3D Models experience of Interactive Buildings, please use a desktop PC, laptop or tablet device</p>',
    'format' => 'full_html',
    'substitute' => 1,
  );
  $export['mobile_message'] = $content;

  return $export;
}
