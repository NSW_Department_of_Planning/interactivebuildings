<?php
/**
 * @file
 * _feature_web_analytics_and_tracking.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function _feature_web_analytics_and_tracking_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['block-facebook_pixel'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'machine_name' => 'facebook_pixel',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '<front>',
    'roles' => array(),
    'themes' => array(
      'nsw' => array(
        'region' => 'header_top',
        'status' => 1,
        'theme' => 'nsw',
        'weight' => 0,
      ),
      'zen' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'zen',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  $export['block-google_adwords'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'machine_name' => 'google_adwords',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '<front>',
    'roles' => array(),
    'themes' => array(
      'nsw' => array(
        'region' => 'bottom',
        'status' => 1,
        'theme' => 'nsw',
        'weight' => 0,
      ),
      'zen' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'zen',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  return $export;
}
