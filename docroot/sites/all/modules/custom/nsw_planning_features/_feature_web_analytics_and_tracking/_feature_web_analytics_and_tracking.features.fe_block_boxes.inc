<?php
/**
 * @file
 * _feature_web_analytics_and_tracking.features.fe_block_boxes.inc
 */

/**
 * Implements hook_default_fe_block_boxes().
 */
function _feature_web_analytics_and_tracking_default_fe_block_boxes() {
  $export = array();

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Facebook Pixel Tracking';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'facebook_pixel';
  $fe_block_boxes->body = '<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version=\'2.0\';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,\'script\',\'https://connect.facebook.net/en_US/fbevents.js\');

fbq(\'init\', \'500266730178049\');
fbq(\'track\', "PageView");</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=500266730178049&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->';

  $export['facebook_pixel'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Google AdWords Tracking';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'google_adwords';
  $fe_block_boxes->body = '<!-- Google Code for Interactive Buildings Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 924543225;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "Xzc-COHwsWcQ-dHtuAM";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript"  
src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt=""  
src="//www.googleadservices.com/pagead/conversion/924543225/?label=Xzc-COHwsWcQ-dHtuAM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>';

  $export['google_adwords'] = $fe_block_boxes;

  return $export;
}
