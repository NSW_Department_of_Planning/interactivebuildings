<?php
/**
 * @file
 * _feature_property_taxonomies.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function _feature_property_taxonomies_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_zone_detail'.
  $field_bases['field_zone_detail'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_zone_detail',
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(
      'max_length' => 255,
    ),
    'translatable' => 0,
    'type' => 'text',
  );

  return $field_bases;
}
