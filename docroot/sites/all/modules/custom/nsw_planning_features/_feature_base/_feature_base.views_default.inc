<?php
/**
 * @file
 * _feature_base.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function _feature_base_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'tooltips';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'taxonomy_term_data';
  $view->human_name = 'Tooltips';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Tooltips';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Taxonomy term: Name */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'taxonomy_term_data';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  $handler->display->display_options['fields']['name']['label'] = '';
  $handler->display->display_options['fields']['name']['exclude'] = TRUE;
  $handler->display->display_options['fields']['name']['element_label_colon'] = FALSE;
  /* Field: Taxonomy term: Alteration obj cat link id */
  $handler->display->display_options['fields']['field_alteration_obj_cat_link_id']['id'] = 'field_alteration_obj_cat_link_id';
  $handler->display->display_options['fields']['field_alteration_obj_cat_link_id']['table'] = 'field_data_field_alteration_obj_cat_link_id';
  $handler->display->display_options['fields']['field_alteration_obj_cat_link_id']['field'] = 'field_alteration_obj_cat_link_id';
  $handler->display->display_options['fields']['field_alteration_obj_cat_link_id']['label'] = '';
  $handler->display->display_options['fields']['field_alteration_obj_cat_link_id']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_alteration_obj_cat_link_id']['alter']['text'] = '<span id="[field_alteration_obj_cat_link_id]_Label">[name]</span>';
  $handler->display->display_options['fields']['field_alteration_obj_cat_link_id']['element_type'] = '0';
  $handler->display->display_options['fields']['field_alteration_obj_cat_link_id']['element_label_type'] = '0';
  $handler->display->display_options['fields']['field_alteration_obj_cat_link_id']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_alteration_obj_cat_link_id']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_alteration_obj_cat_link_id']['element_default_classes'] = FALSE;
  /* Filter criterion: Taxonomy vocabulary: Machine name */
  $handler->display->display_options['filters']['machine_name']['id'] = 'machine_name';
  $handler->display->display_options['filters']['machine_name']['table'] = 'taxonomy_vocabulary';
  $handler->display->display_options['filters']['machine_name']['field'] = 'machine_name';
  $handler->display->display_options['filters']['machine_name']['value'] = array(
    'alteration_object_category' => 'alteration_object_category',
  );

  /* Display: Residential tooltips */
  $handler = $view->new_display('block', 'Residential tooltips', 'residential_tooltips');

  /* Display: Commercial tooltips */
  $handler = $view->new_display('block', 'Commercial tooltips', 'commercial_tooltips');

  /* Display: Industrial tooltips */
  $handler = $view->new_display('block', 'Industrial tooltips', 'industrial_tooltips');
  $export['tooltips'] = $view;

  return $export;
}
