<?php
/**
 * @file
 * _feature_base.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function _feature_base_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function _feature_base_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function _feature_base_node_info() {
  $items = array(
    'alteration_faq' => array(
      'name' => t('FAQ'),
      'base' => 'node_content',
      'description' => t('Manage your FAQ relating to your selected rules'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'plain_page' => array(
      'name' => t('Plain page'),
      'base' => 'node_content',
      'description' => t('Use <em>plain pages</em> for your static content, such as an \'About us\' page.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'terms_and_conditions' => array(
      'name' => t('Terms and conditions'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'webform' => array(
      'name' => t('Webform'),
      'base' => 'node_content',
      'description' => t('Create a new form or questionnaire accessible to users. Submission results and statistics are recorded and accessible to privileged users.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
