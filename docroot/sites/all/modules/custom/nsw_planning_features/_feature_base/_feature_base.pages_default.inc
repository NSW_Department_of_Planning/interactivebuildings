<?php
/**
 * @file
 * _feature_base.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_handlers().
 */
function _feature_base_default_page_manager_handlers() {
  $export = array();

  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'node_view_panel_context_2';
  $handler->task = 'node_view';
  $handler->subtask = '';
  $handler->handler = 'panel_context';
  $handler->weight = 1;
  $handler->conf = array(
    'title' => 'Plain page',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'access' => array(
      'plugins' => array(
        0 => array(
          'name' => 'node_type',
          'settings' => array(
            'type' => array(
              'plain_page' => 'plain_page',
            ),
          ),
          'context' => 'argument_entity_id:node_1',
          'not' => FALSE,
        ),
      ),
      'logic' => 'and',
    ),
  );
  $display = new panels_display();
  $display->layout = 'zen_no_wrapper';
  $display->layout_settings = array(
    'main_classes' => '',
  );
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'main' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '<none>';
  $display->uuid = 'fa49a10c-25fc-448c-bd9c-5794fb3458bf';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-8ff5c9e5-79f1-4524-8e71-45c4ccfee2a5';
    $pane->panel = 'main';
    $pane->type = 'node_title';
    $pane->subtype = 'node_title';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'link' => 0,
      'markup' => 'h1',
      'id' => '',
      'class' => '',
      'context' => 'argument_entity_id:node_1',
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '8ff5c9e5-79f1-4524-8e71-45c4ccfee2a5';
    $display->content['new-8ff5c9e5-79f1-4524-8e71-45c4ccfee2a5'] = $pane;
    $display->panels['main'][0] = 'new-8ff5c9e5-79f1-4524-8e71-45c4ccfee2a5';
    $pane = new stdClass();
    $pane->pid = 'new-3fbd0df1-9637-4385-a2c1-5bf8b8647a97';
    $pane->panel = 'main';
    $pane->type = 'node_body';
    $pane->subtype = 'node_body';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'context' => 'argument_entity_id:node_1',
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '3fbd0df1-9637-4385-a2c1-5bf8b8647a97';
    $display->content['new-3fbd0df1-9637-4385-a2c1-5bf8b8647a97'] = $pane;
    $display->panels['main'][1] = 'new-3fbd0df1-9637-4385-a2c1-5bf8b8647a97';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-3fbd0df1-9637-4385-a2c1-5bf8b8647a97';
  $handler->conf['display'] = $display;
  $export['node_view_panel_context_2'] = $handler;

  return $export;
}
