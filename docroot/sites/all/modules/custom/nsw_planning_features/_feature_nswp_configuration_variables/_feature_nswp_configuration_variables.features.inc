<?php
/**
 * @file
 * _feature_nswp_configuration_variables.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function _feature_nswp_configuration_variables_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
