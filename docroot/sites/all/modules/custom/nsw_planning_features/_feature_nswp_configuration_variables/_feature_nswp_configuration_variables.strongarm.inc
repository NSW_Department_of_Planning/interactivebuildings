<?php
/**
 * @file
 * _feature_nswp_configuration_variables.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function _feature_nswp_configuration_variables_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'getproperty_offline';
  $strongarm->value = array(
    'value' => '<p>Unfortunately the property search service is offline. Please try again later.</p><p>Alternatively, you can still view general planning rules by selecting "I don\'t have a specific property in mind" and following the prompts</p>',
    'format' => 'full_html',
  );
  $export['getproperty_offline'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'no_match_found_contact_council';
  $strongarm->value = array(
    'value' => '<p>Unfortunately no matches have been found for the property details you have provided. Please review your property details and try again.</p>',
    'format' => 'full_html',
  );
  $export['no_match_found_contact_council'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'no_match_found_general';
  $strongarm->value = array(
    'value' => '<p>Unfortunately no matches have been found for the property details you have provided. Please review your property details and try again.</p>',
    'format' => 'full_html',
  );
  $export['no_match_found_general'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'no_street_data_general';
  $strongarm->value = array(
    'value' => '<p>Unfortunately no matching street name can be found.</p>',
    'format' => 'full_html',
  );
  $export['no_street_data_general'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'no_suburb_data_contact_council';
  $strongarm->value = array(
    'value' => '<p>General planning controls are available for viewing in your area.</p>',
    'format' => 'full_html',
  );
  $export['no_suburb_data_contact_council'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'no_suburb_data_general';
  $strongarm->value = array(
    'value' => '<p>Unfortunately no matching suburbs or postcode can be found.</p>',
    'format' => 'full_html',
  );
  $export['no_suburb_data_general'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'nsw_planning_not_allowed_intro';
  $strongarm->value = array(
    'value' => '<p>The development you have selected is not allowed as exempt development as it is on land that is identified as:</p>',
    'format' => 'full_html',
  );
  $export['nsw_planning_not_allowed_intro'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'nsw_planning_not_allowed_outro';
  $strongarm->value = array(
    'value' => '<p>Contact your local&nbsp;<a href="http://www.olg.nsw.gov.au/public/my-local-council/find-my-council" target="_blank">Council</a>&nbsp;to discuss other options for this type of development or if you require any further assistance.</p>',
    'format' => 'full_html',
  );
  $export['nsw_planning_not_allowed_outro'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'nsw_planning_partially_allowed_header';
  $strongarm->value = 'Land-based Exclusions Apply';
  $export['nsw_planning_partially_allowed_header'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'nsw_planning_partially_allowed_intro';
  $strongarm->value = array(
    'value' => '<p>The development you have selected is partially affected by the following land-based control(s):</p>',
    'format' => 'full_html',
  );
  $export['nsw_planning_partially_allowed_intro'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'nsw_planning_partially_allowed_outro';
  $strongarm->value = array(
    'value' => '<p>Exempt development can take place on the part of the land unaffected by the control(s) if they meet the following requirements.</p><p>If you require further assistance, please visit the <a href="https://www.planningportal.nsw.gov.au/" target="_blank">NSW Planning Portal</a>.</p>',
    'format' => 'full_html',
  );
  $export['nsw_planning_partially_allowed_outro'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'nsw_planning_requirements_intro';
  $strongarm->value = array(
    'value' => '<p>If your proposal meets these <a href="[url_dev_type]" target="_blank">planning controls</a>, no planning and building approval is required. The key planning controls are:</p>',
    'format' => 'full_html',
  );
  $export['nsw_planning_requirements_intro'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'nsw_planning_requirements_outro';
  $strongarm->value = array(
    'value' => '<p>If you cannot meet all the planning controls, you may be able do the proposed works as complying development. Find out more about complying development at the planning <a href="http://www.planning.nsw.gov.au/exemptandcomplying" target="_blank">website</a> or contact your local <a href="http://www.olg.nsw.gov.au/public/my-local-council/find-my-council" target="_blank">council</a>&nbsp;or an accredited <a href="http://search.bpb.nsw.gov.au/bpb/BPB_Search.jsp" target="_blank">certifier</a>.</p>',
    'format' => 'full_html',
  );
  $export['nsw_planning_requirements_outro'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'nsw_planning_restrictions_intro';
  $strongarm->value = array(
    'value' => '<p>The development you have selected is not allowed as exempt development if it is on land that is identified as:</p>',
    'format' => 'full_html',
  );
  $export['nsw_planning_restrictions_intro'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'nsw_planning_restrictions_outro';
  $strongarm->value = array(
    'value' => '<p>Contact your local&nbsp;<a href="http://www.olg.nsw.gov.au/public/my-local-council/find-my-council" target="_blank">Council</a>&nbsp;to discuss other options for this type of development or if you require any further assistance.</p>',
    'format' => 'full_html',
  );
  $export['nsw_planning_restrictions_outro'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'nsw_planning_ws_host';
  $strongarm->value = 'http://risws.lgss.com.au';
  $export['nsw_planning_ws_host'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'tcs_node';
  $strongarm->value = '6';
  $export['tcs_node'] = $strongarm;

  return $export;
}
