<?php
/**
 * @file
 * Contains the theme's functions to manipulate Drupal's default markup.
 *
 * Complete documentation for this file is available online.
 * @see https://drupal.org/node/1728096
 */

/**
 * Override or insert variables into the maintenance page template.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("maintenance_page" in this case.)
 */
/* -- Delete this line if you want to use this function
function nsw_preprocess_maintenance_page(&$variables, $hook) {
  // When a variable is manipulated or added in preprocess_html or
  // preprocess_page, that same work is probably needed for the maintenance page
  // as well, so we can just re-use those functions to do that work here.
  nsw_preprocess_html($variables, $hook);
  nsw_preprocess_page($variables, $hook);
}
// */

module_load_include('inc', 'nsw_planning', 'includes/_helpers');
module_load_include('inc', 'nsw_planning_search_property', 'includes/_helpers');


function _nsw_preprocess_get_var($var_name) {
  $vars = &drupal_static(__FUNCTION__, array());
  return isset($vars[$var_name]) ? $vars[$var_name] : NULL;
}

function _nsw_preprocess_set_var($var_name, $new_val = NULL) {
  $vars = &drupal_static(__FUNCTION__, array());
  $vars[$var_name] = $new_val;
  return isset($vars[$var_name]) ? $vars[$var_name] : NULL;
}

/**
 * Override or insert variables into the html templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("html" in this case.)
 */
function nsw_preprocess_html(&$variables, $hook) {

  drupal_add_css('http://fonts.googleapis.com/css?family=Roboto:300,700,400', array('type' => 'external'));
}

/**
 * Override or insert variables into the page templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("page" in this case.)
 */
function nsw_preprocess_page(&$variables, $hook) {

  // Strapline
  if (isset($variables['node']->field_basic_strapline['und'][0]['value'])) {
    $variables['strapline'] = "<div class='strapline'>" . $variables['node']->field_basic_strapline['und'][0]['value'] . "</div>";
  }

  // Full width image set as background
  if (isset($variables['node']->field_basic_highlight['und'][0]['uri'])) {

    $img_object = $variables['node']->field_basic_highlight['und'][0]['uri'];

    $img_path = file_create_url($img_object);
    $img_path = image_style_url('highlight', $img_object);

    $variables['highlight'] = $img_path;
  }
  else {
    $variables['highlight'] = NULL;
  }

}

/**
 * Field Collection-specific implementation of template_preprocess_entity().
 */
function nsw_preprocess_node(&$variables) {
  $is_allowed = -1;

  // If current node is a alteration object node
  if (isset($variables['type']) && $variables['type'] === 'alteration_object') {
    // Set theme prefix for model UI rendering
    $theme_suffix = '';
    // Set theme prefix for details view rendering based on path URL
    if (strpos(request_path(), 'node') !== 0) {
      /* $theme_suffix = '_details'; */
      /* TODO Denis : We can delete the file node--alteration_object_unknown_details.tpl.php */
      $theme_suffix = '';
    }
    if (!empty($variables['is_report'])) {
      _nsw_preprocess_set_var('is_report', TRUE);
      $theme_suffix = '_report';
    }
    else {
      _nsw_preprocess_set_var('is_report', FALSE);
    }
    /*
      // Shown unknown view if no property is found in session
      if (empty($_SESSION['nsw_planning_search']['property'])) {
      $variables['theme_hook_suggestions'][] = 'node__alteration_object_unknown' . $theme_suffix;
      return;
      }
     */

    // Load the current property development model and processed data
    if (!empty($_SESSION['nsw_planning_search']['property']['Model'])) {
      $module_type = $_SESSION['nsw_planning_search']['property']['Model'];
      // Get processed data from session
      $processed_property = $_SESSION['nsw_planning_search']['property']['ProcessedData'][$module_type];
    }

    // Loop through processed data and find title matching for current alteration object
    $node_title = html_entity_decode($variables['title'], ENT_QUOTES);
    if (isset($processed_property)) {
      foreach ($processed_property as $key => $value) {
       if ($key === $node_title) {
          $is_allowed = $value['IsAllowed'];
          // Reason will be used only for partial allowed - Case = 2.
          $reason = $value;
        }
      }
    }

    // choose elements to display in the node--alteration_object.tpl.php
    /* DENIS : we can update the object  unknow as object and remove the tpl suggestion
     * - node--alteration_object_unknown.tpl.php
     */
    $variables['display']['field_requirement_tags_title'] = FALSE;
    $variables['display']['field_additionnal_messages'] = FALSE; /* used 2 times in the tpl > is that normal ? */
    $variables['display']['field_requirements_tags'] = FALSE;
    $variables['display']['field_restriction_tags_title'] = FALSE;
    $variables['display']['field_restriction_tags'] = FALSE;
    $variables['display']['field_alteration_faq'] = FALSE; /* used in node--alteration_object.tpl.php */
    $variables['elements']['#node']->display['field_alteration_faq'] = FALSE;  /* used in field--field_alteration_object_faq.tpl.php */
    $variables['display']['field_alteration_object_rest_faq'] = FALSE; /* used in node--alteration_object.tpl.php */
    $variables['elements']['#node']->display['field_alteration_object_rest_faq'] = FALSE;  /* used in field--field_alteration_object_rest_faq.tpl.php */
    $variables['elements']['#node']->type_of_object = "unknown";
    $variables['elements']['#node']->display['title_faq'] = FALSE;
    $variables['elements']['#node']->display['title_rest_faq'] = FALSE;
    $variables['theme_hook_suggestions'][] = 'node__alteration_object_unknown' . $theme_suffix;
    $variables['is_allowed'] = $is_allowed;
    $variables['need_approval'] = empty($variables['elements']['#node']->field_approval_type) ? FALSE : TRUE;
    if($variables['need_approval']) {
      $field = $variables['elements']['#node']->field_approval_type;
      $term = $field['und'][0]['taxonomy_term'];
      $variables['need_approval_intro'] = empty($term->field_intro['und'][0]['safe_value']) ? '' : $term->field_intro['und'][0]['safe_value'];
      $variables['need_approval_outtro'] = empty($term->field_outtro['und'][0]['safe_value']) ? '' : $term->field_outtro['und'][0]['safe_value'];
    }


    //dsm('$is_allowed = ' . $is_allowed);
    // Switch theme hooks suggestions based on is allowed indicator in the matched data
    switch ($is_allowed) {
      // If this is allowed or partially allowed alteration object
      case '0':
        $variables['display']['field_requirement_tags_title'] = TRUE;
        $variables['display']['field_requirements_tags'] = TRUE;
        if (count($variables['field_alteration_faq']) == 0) {
          $variables['display']['field_alteration_faq'] = FALSE;
        }
        else {
          $variables['display']['field_alteration_faq'] = TRUE;
        }
        $variables['elements']['#node']->display['field_alteration_faq'] = TRUE;
        $variables['elements']['#node']->display['title_faq'] = TRUE;
        $variables['isAllowed'] = "allowed";
        break;
      case '2':
        // case for object allowed or partially allowed.
        $variables['display']['field_restriction_tags'] = TRUE;
        $variables['display']['field_requirement_tags_title'] = TRUE;
        $variables['display']['field_requirements_tags'] = TRUE;
        if (count($variables['field_alteration_faq']) == 0) {
          $variables['display']['field_alteration_faq'] = FALSE;
        }
        else {
          $variables['display']['field_alteration_faq'] = TRUE;
        }
        $variables['elements']['#node']->display['field_alteration_faq'] = TRUE;
        $variables['elements']['#node']->display['title_faq'] = TRUE;
        $variables['isAllowed'] = "partial_allowed";
        $variables['reason'] = $reason;
        break;
      case '1':
        // If this is not allowed alteration object
        //$variables['display']['field_requirement_tags_title'] = TRUE;
        //$variables['display']['field_requirements_tags'] = TRUE;
        $variables['display']['field_restriction_tags'] = TRUE;
        $variables['display']['field_restriction_tags_title'] = TRUE;
        if (count($variables['field_alteration_object_rest_faq']) == 0) {
          $variables['display']['field_alteration_object_rest_faq'] = FALSE;
        }
        else {
          $variables['display']['field_alteration_object_rest_faq'] = TRUE;
        }

        $variables['elements']['#node']->display['field_alteration_object_rest_faq'] = TRUE;
        $variables['elements']['#node']->display['title_rest_faq'] = TRUE;
        $variables['isAllowed'] = "not_allowed";

        break;
      // Shown unknown view if filtered item could not be identified
      default:
        if (module_exists('nsw_planning_search_property') && !empty($_SESSION['nsw_planning_search']['property']) && empty($variables['elements']['#node']->field_approval_type)) {
          $property_details = $_SESSION['nsw_planning_search']['property'];
          $suburb_postcode = array(
            $property_details['AddressSummary']['SuburbName'],
            $property_details['AddressSummary']['PostCode']
          );
          $councils = nsw_planning_search_property_get_council_data($suburb_postcode);
          $council_contacts = array();
          foreach ($councils as $council_name => $council_obj) {
            $council_contacts[] = nsw_planning_search_property_form_warning_message_council_contact_details($council_obj);
          }
          $variables['additional_message'] = nsw_planning_text_format_variable_get('unable_to_locate_information');
          $variables['additional_message'] .= '<p>The general planning rules for this development type are shown below.
For property specific planning requirements, please contact ' . implode(', ', $council_contacts) . '</p>';
        }

        $variables['display']['field_requirement_tags_title'] = TRUE;

        // BUSINESS LOGIC TO ADD HERE > WAIT FOR PIA
        $variables['display']['field_additionnal_messages'] = TRUE;

        $variables['display']['field_requirements_tags'] = TRUE;
        $variables['display']['field_restriction_tags_title'] = TRUE;
        $variables['display']['field_restriction_tags'] = TRUE;
        //$variables['display']['field_alteration_faq'] = TRUE;
        if (count($variables['field_alteration_faq']) == 0) {
          $variables['display']['field_alteration_faq'] = FALSE;
        }
        else {
          $variables['display']['field_alteration_faq'] = TRUE;
        }
        $variables['elements']['#node']->display['field_alteration_faq'] = TRUE;
        if (count($variables['field_alteration_object_rest_faq']) == 0) {
          $variables['display']['field_alteration_object_rest_faq'] = FALSE;
        }
        else {
          $variables['display']['field_alteration_object_rest_faq'] = TRUE;
        }
        $variables['elements']['#node']->display['field_alteration_object_rest_faq'] = TRUE;
        // allow us to display only 1 FAQ title
        if (count($variables['field_alteration_faq']) == 0) {
          $variables['elements']['#node']->display['title_faq'] = FALSE;
          $variables['elements']['#node']->display['title_rest_faq'] = TRUE;
        }
        else {
          $variables['elements']['#node']->display['title_faq'] = TRUE;
          $variables['elements']['#node']->display['title_rest_faq'] = FALSE;
        }
        $variables['elements']['#node']->type_of_object = "unknown";

        break;
    }
  }
}

/**
 * Field Collection-specific implementation of template_preprocess_entity().
 */
function nsw_preprocess_entity(&$variables) {
  if (!empty($variables['entity_type']) && $variables['entity_type'] == 'field_collection_item') {
    _nsw_preprocess_tags_collection($variables);
  }

}

/**
 * Override or insert variables into the node templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("node" in this case.)
 */

// function nsw_preprocess_node(&$variables, $hook) {

//   $path = drupal_get_path_alias(current_path());

//   if(stristr($path, 'list-of-development-types') == 'list-of-development-types') {

//     // include terms and conditions modal
//     $js_path = drupal_get_path('module', 'nsw_planning') . '/js/';
//     drupal_add_js($js_path . 'terms_and_conditions.js');
//   }

//   if(stristr($path, 'list-of-development-types-mobile') == 'list-of-development-types-mobile') {

//     // include terms and conditions modal
//     $js_path = drupal_get_path('module', 'nsw_planning') . '/js/';
//     drupal_add_js($js_path . 'terms_and_conditions.js');
//   }

// }

/**
 * Override or insert variables into the comment templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("comment" in this case.)
 */
/* -- Delete this line if you want to use this function
function nsw_preprocess_comment(&$variables, $hook) {
  $variables['sample_variable'] = t('Lorem ipsum.');
}
// */

/**
 * Override or insert variables into the region templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("region" in this case.)
 */
/* -- Delete this line if you want to use this function
function nsw_preprocess_region(&$variables, $hook) {
  // Don't use Zen's region--sidebar.tpl.php template for sidebars.
  //if (strpos($variables['region'], 'sidebar_') === 0) {
  //  $variables['theme_hook_suggestions'] = array_diff($variables['theme_hook_suggestions'], array('region__sidebar'));
  //}
}
// */

/**
 * Override or insert variables into the block templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("block" in this case.)
 */
/* -- Delete this line if you want to use this function
function nsw_preprocess_block(&$variables, $hook) {
  // Add a count to all the blocks in the region.
  // $variables['classes_array'][] = 'count-' . $variables['block_id'];

  // By default, Zen will use the block--no-wrapper.tpl.php for the main
  // content. This optional bit of code undoes that:
  //if ($variables['block_html_id'] == 'block-system-main') {
  //  $variables['theme_hook_suggestions'] = array_diff($variables['theme_hook_suggestions'], array('block__no_wrapper'));
  //}
}
// */


/**
 * preprocess tags collection entity().
 */
function _nsw_preprocess_tags_collection(&$variables) {
  // Load data to variable for later process
  $entity = $variables['elements']['#entity'];
  $tag_collection_type = !empty($entity->field_tag_collection_type['und'][0]['value']) ? $entity->field_tag_collection_type['und'][0]['value'] : NULL;
  $tag_collection_item = entity_load_single('field_collection_item', $entity->item_id);
  $node = $tag_collection_item->hostEntity();

  switch ($tag_collection_type) {
    case 'static';
      return;
    case 'dynamic';
      // Load the current property type
      if (!empty($_SESSION['nsw_planning_search']['property']['Model'])) {
        $module_type = $_SESSION['nsw_planning_search']['property']['Model'];
        // Get processed data from session
        $processed_property = $_SESSION['nsw_planning_search']['property']['ProcessedData'][$module_type];
      }
      // Remove title element from display if this is a dynamic item
      if (!empty($variables['content']['field_tag_collection']['#items']) && !empty($processed_property)) {
        unset($variables['content']['field_tag_collection_title']);
        $messages_set = $processed_property[$node->title]['MessagesSet'];
        $is_allowed = $processed_property[$node->title]['IsAllowed'];
        foreach ($variables['content']['field_tag_collection']['#items'] as $key => $item) {
          $tag_node = $item['node'];
          switch ($is_allowed) {
            case '0':
              if (!in_array($tag_node->title, $messages_set[0])) {
                unset($variables['content']['field_tag_collection']['#items'][$key]);
              }
              break;
            case '1':
              if (!in_array($tag_node->title, $messages_set[1])) {
                unset($variables['content']['field_tag_collection']['#items'][$key]);
              }
              break;
            case '2':
              if (!in_array($tag_node->title, $messages_set[0]) && !in_array($tag_node->title, $messages_set[1]) && !in_array($tag_node->title, $messages_set[2])) {
                unset($variables['content']['field_tag_collection']['#items'][$key]);
              }
              break;

            default:
              break;
          }
        }
      }
      if (!empty($lgss_alteration_object['Notes'])) {
        $variables['content']['#suffix'] .= check_markup($lgss_alteration_object['Notes']);
      }
      break;
    case 'report';
      // Display on reports page only
      if (arg(0) != 'reports') {
        unset($variables['content']['field_tag_collection_title']);
        unset($variables['content']['field_tag_collection']);
      }
      break;
  }
}

function _nsw_preprocess_get_icon_class($label, $is_complying) {
  if ($is_complying) {
    return 'not-allowed-icon need_approval';
  }
  // Skip the process if the property is unknown
  if (empty($_SESSION['nsw_planning_search']['property'])) {
    return;
  }
  // Load the current property development model and processed data
  if (!empty($_SESSION['nsw_planning_search']['property']['Model'])) {
    $module_type = $_SESSION['nsw_planning_search']['property']['Model'];
    // Get processed data from session
    $processed_property = $_SESSION['nsw_planning_search']['property']['ProcessedData'][$module_type];
  }
  $alt_class = '';
  $label = html_entity_decode($label, ENT_QUOTES);
  if (!empty($processed_property[$label])) {
    $is_allowed = $processed_property[$label]['IsAllowed'];
    switch ($is_allowed) {
      case '0';
        $alt_class = 'allowed-icon';
        break;
      case '1';
        $alt_class = 'not-allowed-icon';
        break;
      case '2';
        $alt_class = 'allowed-icon';
        break;
      default:
        $alt_class = '';
        break;
    }
  }
  return $alt_class;
}