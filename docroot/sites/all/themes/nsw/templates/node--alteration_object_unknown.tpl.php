<?php
/**
 * @file
 * Returns the HTML for a node.
 *
 * Complete documentation for this file is available online.
 * @see https://drupal.org/node/1728164
 */
$defaults = array(
  'value' => '',
  'format' => filter_default_format(),
);

$nsw_planning_requirements_intro = variable_get(
        'nsw_planning_requirements_intro', $defaults
);
$nsw_planning_requirements_outro = variable_get(
        'nsw_planning_requirements_outro', $defaults
);
// BANSW0006-141: Display different messages for restrictions and not-allowed
$nsw_planning_restrictions_intro = variable_get(
        'nsw_planning_restrictions_intro', $defaults);
$nsw_planning_not_allowed_intro = variable_get(
        'nsw_planning_not_allowed_intro', $defaults
);
$nsw_planning_restrictions_outro = variable_get(
        'nsw_planning_restrictions_outro', $defaults
);

$display_field_requirement_tags_title = variable_get(
        'display_field_requirement_tags_title'
);
//dsm($variables['display']);
/* dsm($nsw_planning_requirements_intro);
  dsm($nsw_planning_requirements_outro);
  dsm($nsw_planning_restrictions_intro);
  dsm($nsw_planning_restrictions_outro);
 * */
?>

<div id="object_content_section">

    <!-- BEGIN : ELEMENTS -->
    <div id="elements">
      <!-- begin : additional message : specific to type = unknown -->
        <?php if (isset($variables['additional_message'])
            && $variables['display']['field_additionnal_messages']
        ): ?>
            <div
                class="messages--info messages info"><?php print $variables['additional_message']; ?></div>
        <?php endif; ?>
      <!-- end : additional message : specific to type = unknown -->
      <!-- BEGIN : REQUIREMENTS -->
          <?php
          if (isset($content['field_requirement_tags']['#title']) && $variables['display']['field_requirement_tags_title'] && !$variables['need_approval']
          ):
            ?>
            <div id="elements-1">
              <div id="requirements-element" class="my_alteration_object">
          <h2><span
              class="info-icon"></span> <?php
              print render(
                              $content['field_requirement_tags']['#title']
              );
              ?>
          </h2>

            <?php
            if (isset($content['field_requirement_tags']) && $variables['display']['field_requirements_tags']
            ):
              ?>
              <div id="alteration_object_content">
                <?php
                if (!empty($content['field_intro_link_url']['#items'][0]['safe_value'])) {
                  print str_replace(
                                  '[url_dev_type]', $content['field_intro_link_url']['#items'][0]['safe_value'], $nsw_planning_requirements_intro['value']
                  );
                }
                else {
                  print $nsw_planning_requirements_intro['value'];
                }
                ?>
                <?php
                print render(
                                $content['field_requirement_tags']
                );
                ?>
                <?php print $nsw_planning_requirements_outro['value']; ?>
              </div>
            </div>
                <?php endif; ?>
              </div>
          <br/>
          <?php endif; ?>
          <!-- END : REQUIREMENTS -->

    <!-- BEGIN RESTRICTION -->
    <?php
    if (isset($content['field_restriction_tags']['#title']) && $variables['display']['field_restriction_tags_title']
    ):
      ?>
        <div id="restrictions-element" class="my_alteration_object">
        <?php
        if ($variables['is_allowed'] == 1 || $variables['need_approval']
        ) :
          ?>
          <h2><span
              class="not-allowed-icon"></span> <?php
              print t(
                              'Not Allowed'
              );
              ?>
          </h2>
        <?php else : ?>
          <h2><span
              class="alert-icon"></span> <?php
              print render(
                              $content['field_restriction_tags']['#title']
              );
              ?>
          </h2>
        <?php endif; ?>
        <?php if ($variables['need_approval']): ?>

        <div id="alteration_object_requirements_content">
              <?php print $variables['need_approval_intro']; ?>
              <?php
              print render(
                              $content['field_restriction_tags']
              );
              ?>
              <?php print $variables['need_approval_outtro']; ?>
            </div>

            <?php
          elseif (isset($content['field_restriction_tags']) && $variables['display']['field_restriction_tags']
          ):
            ?>
            <div id="alteration_object_requirements_content">
              <?php
              // BANSW0006-141: Display different messages for restrictions and not-allowed
                  if ($variables['is_allowed'] == 1) {
                print $nsw_planning_not_allowed_intro['value'];
              }
              else {
                print $nsw_planning_restrictions_intro['value'];
              }
              ?>
              <?php
              print render(
                              $content['field_restriction_tags']
              );
              ?>
              <?php print $nsw_planning_restrictions_outro['value']; ?>
            </div>
          <?php endif; ?>
      </div>
        <br/>
      <?php endif; ?>
      <!-- END RESTRICTION -->

  </div>
  <!-- END ELEMENTS -->

      <!-- BEGIN FAQ -->
    <?php if ($variables['display']['field_alteration_faq']
        || $variables['display']['field_alteration_object_rest_faq']
    ):
  ?>
          <div id="alteration_faq_content" class="my_alteration_object">
            <?php if (isset($content['field_alteration_faq'])) : ?>
                <?php print render(
                    $content['field_alteration_faq']
                ); ?> <!-- FAQ RELATED TO REQUIREMENTS -->
            <?php endif; ?>
            <?php if (isset($content['field_alteration_object_rest_faq'])) : ?>
                <?php print render(
                    $content['field_alteration_object_rest_faq']
                ); ?> <!-- FAQ RELATED TO RESTRICTIONS -->
            <?php endif; ?>
        </div>
    <?php endif; ?>
    <!-- END FAQ -->

</div>

<div style="clear:both"></div>

<!-- BEGIN RELATED DEV TYPES -->
<?php if (count($content['related_development_types']['#values']) > 0) : ?>
  <div id="container_related_development_types">
    <br/>
    <div id="related_development_types"
         class="cta block block-view icon-dev-types">
      <h2 class="block-title field-label"></span>Related Development
        Types</h2>

          <div class="group-wrapper">
          <?php
          $nbItems = count(
                  $content['related_development_types']['#values']
          );
          for ($i = 0; $i < $nbItems; $i++) {
            echo '<div class="views-row"><a href="/'
            . drupal_lookup_path(
                    'alias', 'node/'
                    . $content['related_development_types']['#values'][$i]['values']->nid
            ) . '/'
            . $content['related_development_types']['#values'][$i]['values']->nid
            . '">' . render(
                    $content['related_development_types']['#values'][$i]['values']->title
            ) . '</a></div>';
          }
          ?>
        </div>
      </div>
    </div>
  <?php endif; ?>
<!-- END RELATED DEV TYPES -->
