<div id="alteration_object_faq_requirements_content">
  <div class="field field-name-field-alteration-object-rest-faq field-type-node-reference field-label-above">
    <div >
      <h2 class="field-label"><span class="question-icon"><?php echo t('FAQ'); ?>:&nbsp;</h2>
    </div>



      <div class="field-item" id="alteration_faq_content">
        <?php foreach ($node->field_alteration_faq['und'] as $faq) : ?>
        <div class="field-item even" >
              <!-- <article class="node-<?php print $faq['nid']; ?> node-alteration-faq contextual-links-region view-mode-full node-by-viewer clearfix field-name-field-alteration-faq<?php //print $classes;    ?> clearfix field-name-field-alteration-faq"<?php print $attributes; ?>> -->
      <article class="node node-<?php print $faq['nid']; ?> <?php //print $classes;     ?> clearfix"<?php print $attributes; ?>>

          <header>
                <h3 class="node__title node-title"><?php print $faq['node']->title; ?> </h3>
          </header>
            <div class="field-name-body">
            <p class="">
              <?php print $faq['node']->body['und'][0]['value']; ?>
                </p>
          </div>
        </article>

      <?php endforeach; ?>
    </div>
</div>


      <!--
      <hr>
<hr>

<?php if (isset($content['field_alteration_faq'])): ?>
  <div id="alteration_faq_content">
    <?php print render($content['field_alteration_faq']); ?>
  </div>
<?php endif; ?>
-->

</div>

  </div>
</div>