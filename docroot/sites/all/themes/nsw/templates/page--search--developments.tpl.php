<?php
/* BANSW0006-6: Only display developments that are allowed on the A-Z Listing
 * if the user lives in Western Sydney Parklands, Alpine or Siding Springs
 *
 * This template adds in a javascript block if the user lives in the aformentioned
 * areas that hides any developments that aren't allowed (which would be most of them)
 */
?>

<div id="page">
  <header class="header" id="header" role="banner">

	  	<div class="region-header-top-wrapper region-background">
	  		<?php print render($page['header_top']); ?>
	  	</div>

		<div class="region-header clearfix">

	    <?php if ($logo): ?>
	      <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" class="header__logo" id="logo"><img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" class="header__logo-image" /></a>
	    <?php endif; ?>

	    <?php if ($site_name || $site_slogan): ?>
	      <div class="header__name-and-slogan" id="name-and-slogan">
	        <?php if ($site_name): ?>
	          <h1 class="header__site-name" id="site-name">
	            <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" class="header__site-link" rel="home"><span><?php print $site_name; ?></span></a>
	          </h1>
	        <?php endif; ?>

	        <?php if ($site_slogan): ?>
	          <div class="header__site-slogan" id="site-slogan"><?php print $site_slogan; ?></div>
	        <?php endif; ?>
	      </div>
	    <?php endif; ?>

	    <?php print render($page['header']); ?>

		</div>

  </header>

  <div id="navigation" class="clearfix">
    	<?php print render($page['navigation']); ?>
  	</div>

  <div id="main">

    <?php print render($page['highlighted']); ?>

    <div id="content" class="column" role="main">

		<div class="region-content clearfix">

			<div class="region-inner">

     <a id="main-content" tabindex="-1"></a>
     <?php print render($title_prefix); ?>

     <?php if ($title): ?>
       <?php if (isset($node) && ($node->type == "alteration_object")) : ?>
         <!-- case specific for the node->title display after the 3D Canvas -->
                 <span id="see_report_in_title_btn"><a href="/reports" class="btn see-report-in-title" style="z-index:100;"><?php echo t('View reports now'); ?></a></span>
             <h1 class="page__title subtitle" id="page-subtitle-object-alteration"><?php print $title; ?></h1>
           <?php else: ?>
             <h1 class="page__title title" id="page-title"><?php print $title; ?></h1>

          <?php endif; ?>
        <?php endif; ?>

     <?php print render($title_suffix); ?>

       <?php print $messages; ?>
       <?php print render($tabs); ?>
       <?php print render($page['help']); ?>
       <?php if ($action_links): ?>
         <ul class="action-links"><?php print render($action_links); ?></ul>
       <?php endif; ?>

       <?php print render($page['content']); ?>

       <?php print $feed_icons; ?>

			</div>

		</div>

	</div>


    <?php
      // Render the sidebars to see if there's anything in them.
      $sidebar_first  = render($page['sidebar_first']);
      $sidebar_second = render($page['sidebar_second']);
    ?>

    <?php if ($sidebar_first || $sidebar_second): ?>
      <aside class="sidebars">
        <?php print $sidebar_first; ?>
        <?php print $sidebar_second; ?>
      </aside>
    <?php endif; ?>

  </div>

	<?php
		$supporting_content = $page['supporting_content'];
		$lower_content = $page['lower_content'];
	?>

	<?php if($supporting_content):?>
		<div class="region-supporting-content-wrapper clearfix">
		  	<?php print render($supporting_content); ?>
		</div>
	<?php endif; ?>

	<!-- Development type mini panel -->
	<?php if($lower_content):?>
		<div class="region-lower-content-wrapper clearfix region-background">
		  	<?php print render($lower_content); ?>
		</div>
	<?php endif; ?>
</div>



<div class="region-lower-content-wrapper-dark clearfix region-background">
  <div class="region-lower-contact">
    <?php print render($page['footer']); ?>
 </div>
</div>

<?php print render($page['bottom']); ?>

<?php
/*
 * BANSW0006-6: First seeif the property has any of the
 * Western Sydney/Alpine/Siding Springs modifiers.
 *
 * If it does, deploy the script.
 */
$wsp_modifier_list = array(
  "Western Sydney",
  "Alpine",
  "Siding Springs"
);
$modifiers = array();
if (isset($_SESSION['nsw_planning_search']['property']['Modifiers'])) {
  $modifiers = $_SESSION['nsw_planning_search']['property']['Modifiers'];
}
if (count(array_intersect($modifiers, $wsp_modifier_list)) > 0):
  ?>
    <script>
    /*
     * The script hides any item that has the "not-allowed-icon" class, and any of
     * the group-wrapper divs that does not have any "allowed-icon" classes.
     *
     * TODO: Clarify how to proceed with partially-allowed items.
     */
    (function($) {
      $(document).ready(function() {
      $(".not-allowed-icon").hide();
      var groupWrapperArray = $(".group-wrapper");
      for (var i = 0; i < groupWrapperArray.length; i++) {
          if ($(groupWrapperArray[i]).find(".allowed-icon").length <= 0) {
              $(groupWrapperArray[i]).hide();
        }
    }
  });
  })(jQuery);
  </script>
<?php endif; ?>
