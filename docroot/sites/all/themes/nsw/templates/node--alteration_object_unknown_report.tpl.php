<?php
/**
 * @file
 * Returns the HTML for a node.
 *
 * Complete documentation for this file is available online.
 * @see https://drupal.org/node/1728164
 */
$defaults = array(
    'value'  => '',
    'format' => filter_default_format(),
);
$nsw_planning_requirements_intro = variable_get(
    'nsw_planning_requirements_intro', $defaults
);
$nsw_planning_requirements_outro = variable_get(
    'nsw_planning_requirements_outro', $defaults
);
$nsw_planning_restrictions_intro = variable_get(
    'nsw_planning_not_allowed_intro', $defaults
);
$nsw_planning_restrictions_outro = variable_get(
    'nsw_planning_restrictions_outro', $defaults
);
?>

<?php if (isset($variables['additional_message'])): ?>
    <div class="messages--info messages info">
        <?php print $variables['additional_message']; ?>
    </div>
<?php endif; ?>

<!-- BEGIN : REQUIREMENTS -->
<?php if (isset($content['field_requirement_tags']['#title'])
    && $variables['display']['field_requirement_tags_title']
    && !$variables['need_approval']
): ?>
    <div class="restrictions-title">
        <h3>
          <img src="/sites/all/themes/nsw/images/info.png"/>
            <?php print $content['field_requirement_tags']['#title']; ?>
        </h3>
        </div>

      <?php if (isset($content['field_requirement_tags'])
        && $variables['display']['field_requirements_tags']
    ): ?>
        <?php
        if (!empty($content['field_intro_link_url']['#items'][0]['safe_value'])) {
            print str_replace(
                '[url_dev_type]',
                $content['field_intro_link_url']['#items'][0]['safe_value'],
                $nsw_planning_requirements_intro['value']
            );
        } else {
            print $nsw_planning_requirements_intro['value'];
        }
    ?>
            <div class="nsw-reports-pdf">
            <?php print render($content['field_requirement_tags']); ?>
            </div>
    <?php print $nsw_planning_requirements_outro['value']; ?>
    <?php endif; ?>
<?php endif; ?>
<!-- END : REQUIREMENTS -->

<!-- BEGIN RESTRICTION -->
<?php if (isset($content['field_restriction_tags']['#title'])
    && $variables['display']['field_restriction_tags_title']
): ?>
    <div class="restrictions-title">
      <h3>
          <?php if ($variables['is_allowed'] == 1
                || $variables['need_approval']
            ) :
              ?>
                    <img src="/sites/all/themes/nsw/images/not-allowed.png" alt="Not Allowed"/>
                    <?php print t('Not Allowed'); ?>
                  <?php else: ?>
                    <img src="/sites/all/themes/nsw/images/alert.png" alt="What are the restrictions?"/>
                    <?php print $content['field_restriction_tags']['#title']; ?>
            <?php endif; ?>
        </h3>
    </div>

    <?php if ($variables['need_approval']): ?>
        <?php print $variables['need_approval_intro']; ?>
        <?php print render($content['field_restriction_tags']); ?>
        <?php print $variables['need_approval_outtro']; ?>
    <?php elseif (isset($content['field_restriction_tags'])
        && $variables['display']['field_restriction_tags']
    ): ?>
        <?php print $nsw_planning_restrictions_intro['value']; ?>
        <?php print render($content['field_restriction_tags']); ?>
        <?php print $nsw_planning_restrictions_outro['value']; ?>
    <?php endif; ?>
<?php endif; ?>
<!-- END RESTRICTION -->
