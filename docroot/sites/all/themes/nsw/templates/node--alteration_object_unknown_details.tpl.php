<?php
/**
 * @file
 * Returns the HTML for a node.
 *
 * Complete documentation for this file is available online.
 * @see https://drupal.org/node/1728164
 */
$defaults = array(
  'value' => '',
  'format' => filter_default_format(),
);
$nsw_planning_requirements_intro = variable_get('nsw_planning_requirements_intro', $defaults);
$nsw_planning_requirements_outro = variable_get('nsw_planning_requirements_outro', $defaults);
$nsw_planning_restrictions_intro = variable_get('nsw_planning_restrictions_intro', $defaults);
$nsw_planning_restrictions_outro = variable_get('nsw_planning_restrictions_outro', $defaults);
?>
<!--
<div id="object_content_section" class="dev-type-node-content">

  <?php if (isset($variables['additional_message'])): ?>
    <div class="messages--info messages info">
      <?php print $variables['additional_message']; ?>
    </div>
  <?php endif; ?>

  <?php if (isset($content['field_requirement_tags'])): ?>
    <div id="alteration_object_content">
      <span
        class="info-icon"><?php print render($content['field_requirement_tags']['#title']); ?></span>
      <?php
      if (!empty($content['field_intro_link_url']['#items'][0]['safe_value'])) {
        print str_replace('[url_dev_type]', $content['field_intro_link_url']['#items'][0]['safe_value'], $nsw_planning_requirements_intro['value']);
      }
      else {
        print $nsw_planning_requirements_intro['value'];
      } ?>
      <?php print render($content['field_requirement_tags']); ?>
      <?php print $nsw_planning_requirements_outro['value']; ?>
    </div>
  <?php endif; ?>

  <?php if (isset($content['field_alteration_faq'])): ?>
    <div id="alteration_faq_content">
      <?php print render($content['field_alteration_faq']); ?>
    </div>
  <?php endif; ?>

  <?php if (isset($content['field_restriction_tags'])): ?>

    <div id="alteration_object_requirements_content">
      <span
        class="alert-icon"><?php print render($content['field_restriction_tags']['#title']); ?></span>
      <?php print $nsw_planning_restrictions_intro['value']; ?>
      <?php print render($content['field_restriction_tags']); ?>
      <?php print $nsw_planning_restrictions_outro['value']; ?>
    </div>
  <?php endif; ?>

  <?php if ($content['field_alteration_object_rest_faq']): ?>
    <div id="alteration_object_faq_requirements_content">
      <?php print render($content['field_alteration_object_rest_faq']); ?>
    </div>
  <?php endif; ?>




<div id="object_content_section">

  <div id="elements">

    <!-- begin : additional message : specific to type = unknown -->
    <?php if (isset($variables['additional_message']) && $variables['display']['field_additionnal_messages']): ?>
      <div class="messages--info messages info"><?php print $variables['additional_message']; ?></div>
    <?php endif; ?>
    <!-- end : additional message : specific to type = unknown -->


    <!-- BEGIN : REQUIREMENTS -->
    <div id="elements-1">
      <?php if (isset($content['field_requirement_tags']['#title']) && $variables['display']['field_requirement_tags_title']): ?>
        <div id="requirements-element" class="my_alteration_object">
          <h2><span class="info-icon"></span> <?php print render($content['field_requirement_tags']['#title']); ?></h2>

          <?php if (isset($content['field_requirement_tags']) && $variables['display']['field_requirements_tags']): ?>
            <div id="alteration_object_content">
              <?php
              if (!empty($content['field_intro_link_url']['#items'][0]['safe_value'])) {
                print str_replace('[url_dev_type]', $content['field_intro_link_url']['#items'][0]['safe_value'], $nsw_planning_requirements_intro['value']);
              }
              else {
                print $nsw_planning_requirements_intro['value'];
              }
              ?>
              <?php print render($content['field_requirement_tags']); ?>
    <?php print $nsw_planning_requirements_outro['value']; ?>
            </div>
          </div>
        <?php endif; ?>
<?php endif; ?>
    </div>
    <!-- END : REQUIREMENTS -->

    <!-- BEGIN RESTRICTION -->
<?php if (isset($content['field_restriction_tags']['#title']) && $variables['display']['field_restriction_tags_title']): ?>
      <br />
      <div id="restrictions-element" class="my_alteration_object">
        <h2><span class="alert-icon"></span> <?php print render($content['field_restriction_tags']['#title']); ?></h2>

          <?php if (isset($content['field_restriction_tags']) && $variables['display']['field_restriction_tags']): ?>
          <div id="alteration_object_requirements_content">
            <?php print $nsw_planning_restrictions_intro['value']; ?>
            <?php print render($content['field_restriction_tags']); ?>
    <?php print $nsw_planning_restrictions_outro['value']; ?>
          </div>
        </div>
      <?php endif; ?>
<?php endif; ?>
  </div>
  <!-- END RESTRICTION -->


  <!-- BEGIN FAQ -->
<?php if ($variables['display']['field_alteration_faq'] || $variables['display']['field_alteration_object_rest_faq']): ?>
    <br />
    <div id="alteration_faq_content" class="my_alteration_object">
      <?php if (isset($content['field_alteration_faq'])) : ?>
        <?php print render($content['field_alteration_faq']); ?> <!-- FAQ RELATED TO REQUIREMENTS -->
      <?php endif; ?>
      <?php if (isset($content['field_alteration_object_rest_faq'])) : ?>
        <?php print render($content['field_alteration_object_rest_faq']); ?> <!-- FAQ RELATED TO RESTRICTIONS -->
    <?php endif; ?>
    </div>
<?php endif; ?>
    <!-- END FAQ -->

</div>   <!-- End #elements -->