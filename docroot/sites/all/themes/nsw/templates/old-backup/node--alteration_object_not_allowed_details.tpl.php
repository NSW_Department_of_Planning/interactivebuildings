<?php
/**
 * @file
 * Returns the HTML for a node.
 *
 * Complete documentation for this file is available online.
 * @see https://drupal.org/node/1728164
 */
$defaults = array(
  'value' => '',
  'format' => filter_default_format(),
);
$nsw_planning_not_allowed_intro = variable_get('nsw_planning_not_allowed_intro', $defaults);
$nsw_planning_not_allowed_outro = variable_get('nsw_planning_not_allowed_outro', $defaults);
?>

<div id="object_content_section" class="dev-type-node-content">

  <span class="alert-icon">Not Allowed</span>

  <?php if (isset($content['field_restriction_tags'])): ?>
    <div id="alteration_object_content">
      <?php print $nsw_planning_not_allowed_intro['value']; ?>
      <?php print render($content['field_restriction_tags']); ?>
      <?php print $nsw_planning_not_allowed_outro['value']; ?>
    </div>
  <?php endif; ?>

  <?php if (isset($content['field_alteration_object_rest_faq'])): ?>
    <div id="alteration_faq_content">
      <?php print render($content['field_alteration_object_rest_faq']); ?>
    </div>
  <?php endif; ?>

</div> <!-- End #object_content_section -->
