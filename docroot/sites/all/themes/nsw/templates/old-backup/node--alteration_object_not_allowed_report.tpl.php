<?php
/**
 * @file
 * Returns the HTML for a node.
 *
 * Complete documentation for this file is available online.
 * @see https://drupal.org/node/1728164
 */
$defaults = array(
  'value' => '',
  'format' => filter_default_format(),
);
$nsw_planning_not_allowed_intro = variable_get('nsw_planning_not_allowed_intro', $defaults);
$nsw_planning_not_allowed_outro = variable_get('nsw_planning_not_allowed_outro', $defaults);
?>

<div class="restrictions-title">
  <h3>
    <img src="/sites/default/files/alert.png">
    Not Allowed
  </h3>
</div>

<?php if (isset($content['field_restriction_tags'])): ?>
  <?php print $nsw_planning_not_allowed_intro['value']; ?>
  <?php print render($content['field_restriction_tags']); ?>
  <?php print $nsw_planning_not_allowed_outro['value']; ?>
<?php endif; ?>


