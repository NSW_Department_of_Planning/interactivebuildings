<?php
/**
 * @file
 * Returns the HTML for a node.
 *
 * Complete documentation for this file is available online.
 * @see https://drupal.org/node/1728164
 */
$defaults = array(
  'value' => '',
  'format' => filter_default_format(),
);
$nsw_planning_requirements_intro = variable_get('nsw_planning_requirements_intro', $defaults);
$nsw_planning_requirements_outro = variable_get('nsw_planning_requirements_outro', $defaults);
?>

<div id="object_content_section" class="dev-type-node-content">

  <span
    class="info-icon"></span><?php print render($content['field_requirement_tags']['#title']); ?>

  <?php if (isset($content['field_requirement_tags'])): ?>
    <div id="alteration_object_content">
      <?php
      if (!empty($content['field_intro_link_url']['#items'][0]['safe_value'])) {
        print str_replace('[url_dev_type]', $content['field_intro_link_url']['#items'][0]['safe_value'], $nsw_planning_requirements_intro['value']);
      }
      else {
        print $nsw_planning_requirements_intro['value'];
      } ?>
      <?php print render($content['field_requirement_tags']); ?>
      <?php print $nsw_planning_requirements_outro['value']; ?>
    </div>
  <?php endif; ?>

  <?php if (isset($content['field_alteration_faq'])): ?>
    <div id="alteration_faq_content">
      <?php print render($content['field_alteration_faq']); ?>
    </div>
  <?php endif; ?>

</div> <!-- End #object_content_section -->
